import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/analytics'

let config = {
    apiKey: "AIzaSyBNFOKfMb1ZQ-iypSPYMFofjjmcea01ymY",
    authDomain: "amimarket-ad45f.firebaseapp.com",
    databaseURL: "https://amimarket-ad45f.firebaseio.com",
    projectId: "amimarket-ad45f",
    storageBucket: "amimarket-ad45f.appspot.com",
    messagingSenderId: "465794048798",
    appId: "1:465794048798:web:5ad0abf64d12af3d869f0f",
    measurementId: "G-SPD49MX4SP"
}

// let configQA = {
//     apiKey: "AIzaSyDPyuK4y_tSwQOeuzQRjE8zy9sxsox-Yhw",
//     authDomain: "amimarketqa.firebaseapp.com",
//     projectId: "amimarketqa",
//     storageBucket: "amimarketqa.appspot.com",
//     messagingSenderId: "1085524514705",
//     appId: "1:1085524514705:web:c8ab4103e9790c87cdec30",
//     measurementId: "G-5JCE97RNK6"
// }

const fb = firebase.initializeApp(config);

export const database = fb.firestore();
export const analytics = fb.analytics();