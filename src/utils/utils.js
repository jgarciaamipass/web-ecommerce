function rutIsValid(rut) {
  if (!rut || rut.trim().length < 3) return false;
  const rutLimpio = rut.replace(/[^0-9kK-]/g,"").toLowerCase();

  if (rutLimpio.length < 3) return false;

  const split = rutLimpio.split("-");
  if (split.length !== 2) return false;

  const num = parseInt(split[0], 10);
  const dgv = split[1];

  const dvCalc = calculateDV(num);
  return dvCalc === dgv;
}

function calculateDV(rut) {
  const cuerpo = `${rut}`;
  // Calcular Dígito Verificador
  let suma = 0;
  let multiplo = 2;

  // Para cada dígito del Cuerpo
  for (let i = 1; i <= cuerpo.length; i++) {
    // Obtener su Producto con el Múltiplo Correspondiente
    const index = multiplo * cuerpo.charAt(cuerpo.length - i);

    // Sumar al Contador General
    suma += index;

    // Consolidar Múltiplo dentro del rango [2,7]
    if (multiplo < 7) {
      multiplo += 1;
    } else {
      multiplo = 2;
    }
  }

  // Calcular Dígito Verificador en base al Módulo 11
  const dvEsperado = 11 - (suma % 11);
  if (dvEsperado === 10) return"k";
  if (dvEsperado === 11) return"0";
  return `${dvEsperado}`.toUpperCase();
}

function getImageCover(value) {
  let classification = {
    "Chilena":"Chi",
    // "Cafetería":"Caf",
    "Pastelería":"Pas",
    "Vegetariana":"Veg",
    "Sandwichería":"San",
    "Heladería":"Hel",
    "FastFood":"Fas",
    "Casera":"Cas",
    "Internacional":"Int",
    "Pizzería":"Piz",
    "Pollos":"Pol",
    "Completos":"Com",
    "Japonesa":"Jap",
    "Mexicana":"Mex",
    "Parrilladas":"Par",
    "Colombiana":"Col",
    "SupermercadosyAlmacenes":"Sup",
    "Minimarket":"Min",
    "Sushi":"Sush",
    "Árabe":"Ara",
    "Carnes":"Car",
    "Fusión":"Fus",
    "Francesa":"Fra",
    "Peruana":"Per",
    "Italiana":"Ita",
    "China":"Chn",
    "Buffet":"Buf",
    "Alemana":"Ale",
    // "Crepes-Waffles":"Cre",
    "Empanadas":"Emp",
    "Pub":"Pub",
    "Tailandesa":"Tai",
    "Norteamericana":"Nor",
    "PanaderíayPastelería":"Pan",
    "Colaciones":"Clc",
    "Rotisería":"Rot",
    "Cantonesa":"Can",
    "Cubana":"Cub",
    "Otro":"Otr",
    "PescadosyMariscos":"Pes",
    "Delivery":"Del",
    "Española":"Esp",
    "Suiza":"Sui",
    "Exclusivo":"CEx",
    // "Saludable":"Sal",
    "Hindu":"Hin",
    "ComidaCongelada":"Con",
    // "FrutosSecos":"Frs",
    "Verdulerías":"Ver",
    "Brasileña":"Bra",
    "Carnicería":"Carni",
    // "Frutasyverduras":"Fyv",
    "Pescadosymariscos":"Pym",
    "FeriasLibres":"Fer",
    "Vietnamita":"Vie",
    // "Jugosnaturales":"Jna",
    // "Verdurasyhortalizas":"Vyh",
  }
  return Object.keys(classification).find(key => key === `${value}`.replace(' ', ''))
}

const SERVER_API = process.env.REACT_APP_API_URL

export { rutIsValid, calculateDV, getImageCover, SERVER_API };