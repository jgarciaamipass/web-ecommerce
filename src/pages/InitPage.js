import React, { Component } from 'react'
import { connect } from 'react-redux'
import countItemCarReducerAction from '../redux/actions/countItemCarReducerAction';
import itemsCarReducerAction from './../redux/actions/itemsCarReducerAction';
import PageComponent from '../component/PageComponent'
import SearchLocationInput from '../component/SearchLocationInput'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch } from '@fortawesome/free-solid-svg-icons'

import bgImage from './../assets/images/home-3.jpg'

class InitPage extends Component {

    componentDidMount() {
        localStorage.removeItem('car')
        localStorage.removeItem('shopping')
        localStorage.removeItem('delivery')
        localStorage.removeItem('accountsAmipass')
        localStorage.removeItem('address')
        this.props.itemsCarReducerAction({})
        this.props.countItemCarReducerAction({
            count: 0,
            amount: 0,
            items: []
        })
        // let address = localStorage.getItem('address');
        // if(address !== null) {
        //     if(typeof address.geometry === 'undefined') {
        //         this.props.history.push(`/home`)
        //     }
        // }
    }

    onSubmit = () => {
        window.dataLayer.push({'event': 'clickButtonAddressSearch'})
        this.props.history.push('/home');
    }

    render() {
        return (
            <PageComponent>
                <div className="d-flex align-items-center h-100" 
                    style={{ 
                        backgroundImage: `url(${bgImage})`, 
                        backgroundPosition: 'top', 
                        backgroundSize: 'cover', 
                        backgroundRepeat: 'no-repeat' 
                    }} >
                    <div className='d-flex flex-column align-items-center w-100' >
                        <div className="py-2 title-search">
                            <h3 className='color-white'>Todo lo que necesitas lo encuentras aquí</h3>
                        </div>
                        <form className='col-md-4' action='#' onSubmit={this.onSubmit} >
                            <div className="py-2" >
                                <div className='form-row'>
                                    <div className='form-group col'>
                                        <SearchLocationInput/>
                                    </div>
                                    <div className='form-group'>
                                        <button className='btn btn-primary padding-button d-desktop' type='submit'><FontAwesomeIcon icon={faSearch}/> Buscar</button>
                                        <button className='btn btn-primary padding-button d-movil' type='submit'><FontAwesomeIcon icon={faSearch} /></button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </PageComponent>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        shoppingCarMenuReducer: state.shoppingCarMenuReducer,
    }
}

const mapDispatchToProps = {
    itemsCarReducerAction,
    countItemCarReducerAction
}

export default connect(mapStateToProps, mapDispatchToProps)(InitPage)