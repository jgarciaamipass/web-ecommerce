import React, { Component } from 'react'
import { Form } from 'react-bootstrap'
import CurrencyFormat from 'react-currency-format'
import PageComponent from '../component/PageComponent'
import ModalComponent from '../component/system/ModalComponent'
import { SERVER_API } from '../utils/utils'

export default class RecommenderStorePage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            phone: '',
            commune: '',
            region: '',
            email: '',
            sending: false,
            msgModal: false,
            msgTitle: '',
            msgText: '',
            validationForm: {
                name: false,
                phone: false,
                commune: false,
                region: false,
                email: false
            }
        }
    }

    onSubmit = (e) => {
        let { name, phone, commune, region, email } = this.state.validationForm

        if( !name && !phone && !commune && !region && !email ) {
            const {
                name, phone, commune, region, email
            } = this.state;

            this.setState({ isLoading: true })
            let statusResponse = 0
            
            fetch(`${SERVER_API}/recommender/store`, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    name, phone, commune, region, email
                })
            }).then(async response => {
                statusResponse = response.status
                return await response.json()
            }).then(response => {
                switch (statusResponse) {
                    case 200:
                        this.setState({
                            msgModal: true,
                            msgTitle: "Información enviada",
                            msgText: `Su información fue enviada satisfactoriamente. Nuestro equipo de comerciales entrará en contacto con tu local recomendado.`
                        })

                        this.setState({
                            name: '',
                            phone: '',
                            commune: '',
                            region: '',
                            email: '',
                        })
                        this.frmRecommender.reset()
                        break;
                    default:
                        this.setState({
                            msgModal: true,
                            msgTitle: "Error inesperado",
                            msgText: `Hubo un error inesperado registrando el usuario. COD (${statusResponse})`
                        })
                        break;
                }
            }).catch(error => {
                console.log(error)
                this.setState({
                    isLoading: false
                })
                this.setState({
                    msgModal: true,
                    msgTitle: "Error",
                    msgText: `Hubo un error registrando el usuario. COD (${statusResponse})`
                })
            })
        }
        e.preventDefault();
    }

    render() {
        return (
            <PageComponent>
                <div className="d-flex align-items-center h-100" >
                    <div className='d-flex flex-column align-items-center w-100' >
                        <div className='card col-lg-4 col-md-8 body-form-ami' >
                            <div className='card-body' >
                                <form onSubmit={this.onSubmit} action={"#"} method={'post'} ref={(el) => this.frmRecommender = el} >
                                    <div className="text-center">
                                        <h3>amiPASS</h3>
                                        <p className='text-muted' >
                                            Se preocupa de tu familia
                                        </p>
                                        <p>
                                            Por que queremos que todo lo que buscas lo encuentres en amiMARKET, recomiéndanos tu local favorito y nuestro equipo trabajará en contactarlo cuanto antes!.
                                        </p>
                                        <p>
                                            Completa los siguientes campos
                                        </p>
                                    </div>
                                    <div className='form-group' >
                                        <Form.Control
                                            required
                                            type="text"
                                            placeholder="Nombre del local"
                                            onChange={e => { this.setState({ name: e.target.value }) }}
                                            isInvalid={this.state.validationForm.name}
                                            value={this.state.name}
                                            onBlur={e => {
                                                const { value } = e.target
                                                this.setState({
                                                    validationForm: {
                                                        ...this.state.validationForm,
                                                        name: (value === null || value === '')
                                                    }
                                                })
                                            }}
                                        />
                                        <Form.Control.Feedback type="invalid">Debe indicar un nombre</Form.Control.Feedback>
                                    </div>
                                    <div className='form-group' >
                                        <CurrencyFormat format="+56 # #### ####" mask="_" 
                                            type='tel'
                                            placeholder='Teléfono del local Ej 9 1234 5678'
                                            required={true}
                                            value={this.state.phone}
                                            minLength={9}
                                            className={(this.state.validationForm.phone) ? 'form-control is-invalid' : 'form-control'}
                                            onValueChange={(values) => {
                                                const {value} = values;
                                                this.setState({ phone: value })
                                            }} 
                                            onBlur={e => {
                                                const { value } = e.target
                                                this.setState({
                                                    validationForm: {
                                                        ...this.state.validationForm,
                                                        phone: (value === null || value === '')
                                                    }
                                                })
                                            }}
                                        />
                                        <div className="invalid-feedback">
                                            Debe indicar un número de teléfono
                                        </div>
                                    </div>
                                    <div className='form-group' >
                                        <Form.Control
                                            required
                                            type="text"
                                            value={this.state.commune}
                                            placeholder="Comuna del local"
                                            onChange={e => { this.setState({ commune: e.target.value }) }}
                                            isInvalid={this.state.validationForm.commune}
                                            onBlur={e => {
                                                const { value } = e.target
                                                this.setState({
                                                    validationForm: {
                                                        ...this.state.validationForm,
                                                        commune: (value === null || value === '')
                                                    }
                                                })
                                            }}
                                        />
                                        <Form.Control.Feedback type="invalid">Debe indicar la comuna</Form.Control.Feedback>
                                    </div>
                                    <div className='form-group' >
                                        <Form.Control
                                            required
                                            type="text"
                                            value={this.state.region}
                                            placeholder="Región del local"
                                            onChange={e => { this.setState({ region: e.target.value }) }}
                                            isInvalid={this.state.validationForm.region}
                                            onBlur={e => {
                                                const { value } = e.target
                                                this.setState({
                                                    validationForm: {
                                                        ...this.state.validationForm,
                                                        region: (value === null || value === '')
                                                    }
                                                })
                                            }}
                                        />
                                        <Form.Control.Feedback type="invalid">Debe indicar la región</Form.Control.Feedback>
                                    </div>
                                    <div className='form-group' >
                                        <Form.Control
                                            required
                                            type="email"
                                            name='email'
                                            placeholder="Tu correo electrónico"
                                            onChange={e => this.setState({ email: e.target.value })}
                                            isInvalid={this.state.validationForm.email}
                                            onBlur={async e => {
                                                const { value } = e.target;
                                                let mail = value.split('@')
                                                this.setState({
                                                    validationForm: {
                                                        ...this.state.validationForm,
                                                        email: !(value.split('@').length > 1 && mail[1].indexOf(".") > 1),
                                                    }
                                                })
                                            }}
                                        />
                                        <Form.Control.Feedback type="invalid">Debe indicar un email válido</Form.Control.Feedback>
                                    </div>
                                    <div className='form-group text-center' >
                                        <button type="submit" className="btn btn-primary">Enviar</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <ModalComponent
                    show={this.state.msgModal} 
                    handleClose={() => this.setState({ msgModal: false })}
                    title={this.state.msgTitle}
                >
                    <p>{this.state.msgText}</p>
                </ModalComponent>
            </PageComponent>
        )
    }
}
