import React, { Component } from 'react'
import { Button, Card, Form } from 'react-bootstrap'
import PageComponent from '../../component/PageComponent'
import ModalComponent from '../../component/system/ModalComponent';
import { SERVER_API } from '../../utils/utils';

export default class ChangePasswordPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            token: "",
            password: "",
            passwordConfirm: "",
            msgModal: false,
            msgTitle: "",
            msgText: "",
            validationForm: {
                password: true,
                passwordConfirm: true,
                strength: {
                    number: false,
                    length: false,
                    special: false,
                    mixed: false
                }
            },
        };
    }

    componentDidMount() {
        let { token } = this.props.match.params;
        if(typeof token !== 'undefined') {
            this.setState({ token })
        }
    }

    handleCloseModal = () => {
        this.setState({ msgModal: false })
    }

    handleAcceptModal = () => {
        const { history } = this.props;
        history.push(`/login`)
    }

    hasNumber = value => {
        return new RegExp(/[0-9]/).test(value);
    }
    hasMixed = value => {
        return new RegExp(/[a-z]/).test(value) &&
            new RegExp(/[A-Z]/).test(value);
    }
    hasSpecial = value => {
        return new RegExp(/[!#@$%^&*)(+=._-]/g).test(value);
    }

    passwordStrength = (value) => {
        let strength = {
            length: value.length >= 8,
            number: this.hasNumber(value),
            mixed: this.hasMixed(value)
        }
        this.setState({
            validationForm: {
                ...this.state.validationForm,
                strength
            }
        })
    }

    sendChangePassword = (e) => {
        const { password, passwordConfirm, token, validationForm } = this.state;
        let statusResponse = 0
        if(validationForm.password && validationForm.passwordConfirm) {
            this.setState({ isLoading: true })
            fetch(`${SERVER_API}/login/reset_password`, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    token,
                    password,
                    passwordConfirm
                })
            }).then(async response => {
                statusResponse = response.status
                return await response.json();
            }).then(response => {
                console.log(response)
                switch (statusResponse) {
                    case 200:
                        this.setState({
                            msgModal: true,
                            msgTitle: "¡Cambio exitoso!",
                            msgText: "Se restableció tu contraseña con éxito"
                        })
                        break;
                    case 400:
                        this.setState({
                            msgModal: true,
                            msgTitle: "Error",
                            msgText: "Link vencido o no es válido"
                        })
                        break;
                    default:
                        this.setState({
                            msgModal: true,
                            msgTitle: "Error inesperado",
                            msgText: `Hubo un error inesperado en el cambio de contraseña. COD (${statusResponse})`
                        })
                        break;
                }
                this.setState({ 
                    isLoading: false,
                })
            }).catch(error => {
                console.log(error)
                this.setState({ 
                    isLoading: false,
                })
                this.setState({
                    msgModal: true,
                    msgTitle: "Error",
                    msgText: `Hubo un error intentando hacer el cambio de contraseña. COD (${statusResponse})`
                })
            })
        }
        e.preventDefault();
    }

    render() {
        return (
            <PageComponent>
                <div className="d-flex align-items-center h-100" >
                    <div className='d-flex flex-column align-items-center w-100' >
                        <Card className='col-lg-4 col-md-8 body-form-ami' >
                            <Card.Body >
                                <div className='text-center' >
                                    <h3>Restablecer contraseña</h3>
                                    <p>
                                        Restablece tu contraseña 
                                    </p>
                                </div>
                                <Form onSubmit={this.sendChangePassword} action={"#"} method={'POST'} >
                                    <Form.Group>
                                        <Form.Control 
                                            required 
                                            type="password" 
                                            name='password'
                                            placeholder="Contraseña"
                                            minLength={8}
                                            maxLength={12}
                                            onChange={e => {
                                                this.passwordStrength(e.target.value)
                                                this.setState({ password: e.target.value })
                                            }}
                                            isInvalid={!this.state.validationForm.password}
                                            onBlur={e => {
                                                this.passwordStrength(e.target.value)
                                                this.setState({
                                                    validationForm: {
                                                        ...this.state.validationForm,
                                                        password: (
                                                            this.state.validationForm.strength.number && 
                                                            this.state.validationForm.strength.length && 
                                                            this.state.validationForm.strength.mixed
                                                        )
                                                    }
                                                })
                                            }}
                                        />
                                        <Form.Control.Feedback type="invalid">La contraseña no es válida</Form.Control.Feedback>
                                        <small>
                                            <span>Usa de 8 a 12 caracteres con una combinación de letras, números y símbolos</span>
                                            <ul className='list-unstyled ml-3'>
                                                <li className={(this.state.validationForm.strength.length) ? 'text-success' : 'text-muted'} >8 a 12 caracteres</li>
                                                <li className={(this.state.validationForm.strength.mixed) ? 'text-success' : 'text-muted'} >1 mayúscula</li>
                                                <li className={(this.state.validationForm.strength.number) ? 'text-success' : 'text-muted'} >1 número</li>
                                            </ul>
                                        </small>
                                    </Form.Group>
                                    <Form.Group>
                                        <Form.Control
                                            required
                                            type="password"
                                            name='passwordConfirm'
                                            placeholder="Confirmar contraseña"
                                            minLength={8}
                                            maxLength={12}
                                            onChange={e => this.setState({ passwordConfirm: e.target.value })}
                                            isInvalid={!this.state.validationForm.passwordConfirm}
                                            onBlur={e => {
                                                const { value } = e.target;
                                                this.setState({
                                                    validationForm: {
                                                        ...this.state.validationForm,
                                                        passwordConfirm: (this.state.password === value)
                                                    }
                                                })
                                            }}
                                        />
                                        <Form.Control.Feedback type="invalid">La contraseña no coincide</Form.Control.Feedback>
                                    </Form.Group >
                                    <Form.Group className='text-center' >
                                        <Button variant="primary" type="submit" block disabled={this.state.isLoading}>
                                            {this.state.isLoading ? 'Enviando...' : 'Continuar'}
                                        </Button>
                                    </Form.Group>
                                </Form>
                            </Card.Body>
                        </Card>
                    </div>
                </div>
                <ModalComponent 
                    show={this.state.msgModal} 
                    title={this.state.msgTitle}
                    handleAccept={this.handleAcceptModal}
                >
                    <p>{this.state.msgText}</p>
                </ModalComponent>
            </PageComponent>
        )
    }
}
