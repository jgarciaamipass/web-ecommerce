import React, { Component } from 'react'
import PageComponent from '../../component/PageComponent'
import { rutIsValid, SERVER_API } from '../../utils/utils'
import { Form, Button, Card } from 'react-bootstrap'
import ModalComponent from '../../component/system/ModalComponent';
// import { database } from '../../utils/firebase'

export default class RegisterPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            firstName: "",
            lastName: "",
            numDocument: "",
            email: "",
            emailConfirm: "",
            password: "",
            passwordConfirm: "",
            showModal: false,
            isLoading: false,
            validated: false,
            validationForm: {
                firstName: true,
                lastName: true,
                rut: true,
                documentExist: false,
                password: true,
                passwordConfirm: true,
                email: true,
                emailExist: false,
                emailConfirm: true,
                strength: {
                    number: false,
                    length: false,
                    special: false,
                    mixed: false
                }
            },
            msgModal: false,
            msgTitle: "",
            msgText: ""
        };
    }

    componentDidMount() {
        const { history } = this.props;
        if (localStorage.getItem('jwt') !== null) {
            history.push('/home')
        }
    }

    register = (e) => {
        let statusResponse = 0;
        const { firstName, lastName, rut, email, emailConfirm, password, passwordConfirm } = this.state.validationForm
        this.setState({
            validated: (
                firstName && lastName && rut && email && emailConfirm && password && passwordConfirm
            )
        })
        if ((
            firstName && lastName && rut && email && emailConfirm && password && passwordConfirm
        )) {
            const {
                firstName,
                lastName,
                numDocument,
                email,
                emailConfirm,
                password,
                passwordConfirm,
            } = this.state;
            this.setState({ isLoading: true })
            fetch(`${SERVER_API}/login/register`, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    firstName,
                    lastName,
                    numDocument,
                    email,
                    emailConfirm,
                    password,
                    passwordConfirm
                })
            }).then(async response => {
                statusResponse = response.status
                return await response.json()
            }).then(async response => {
                switch (statusResponse) {
                    case 200:
                        this.setState({ showModal: true })
                        // let itemsCar = JSON.parse(localStorage.getItem('car'));

                        // const cart = await database
                        // .collection(`${iduser}`)
                        // .doc(`cart-${this.state.commerce.commerceID}${this.state.commerce.storeID}`)
                        // .set({
                        //     commerce: this.state.commerce.commerceID,
                        //     store: this.state.commerce.storeID,
                        //     commerceAlias: this.state.commerce.ecommerceName,
                        //     items: itemsCar
                        // })
                        break;
                    default:
                        this.setState({
                            msgModal: true,
                            msgTitle: "Error inesperado",
                            msgText: `Hubo un error inesperado registrando el usuario. COD (${statusResponse})`
                        })
                        break;
                }
            }).catch(error => {
                console.log(error)
                this.setState({
                    isLoading: false
                })
                this.setState({
                    msgModal: true,
                    msgTitle: "Error",
                    msgText: `Hubo un error registrando el usuario. COD (${statusResponse})`
                })
            })
        }
        this.setState({ validated: true })
        e.preventDefault();
    }

    onRedirect = () => {
        const { history } = this.props;
        history.push('/')
    }

    hasNumber = value => {
        return new RegExp(/[0-9]/).test(value);
    }
    hasMixed = value => {
        return new RegExp(/[a-z]/).test(value) &&
            new RegExp(/[A-Z]/).test(value);
    }
    hasSpecial = value => {
        return new RegExp(/[!#@$%^&*)(+=._-]/g).test(value);
    }

    passwordStrength = (value) => {
        let strength = {
            length: value.length >= 8,
            number: this.hasNumber(value),
            mixed: this.hasMixed(value)
        }
        this.setState({
            validationForm: {
                ...this.state.validationForm,
                strength
            }
        })
    }

    documentExist = (numDocument) => {
        let statusResponse = 0;
        return fetch(`${SERVER_API}/login/document_exist`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                numDocument
            })
        }).then(async response => {
            statusResponse = response.status
            console.log(statusResponse)
            return await response.json()
        }).then(response => {
            switch (statusResponse) {
                case 200:
                    return false
                case 400:
                    return true
                default:
                    this.setState({
                        msgModal: true,
                        msgTitle: "Error inesperado",
                        msgText: `Hubo un error inesperado validando si existe el RUT. COD (${statusResponse})`
                    })
                    break;
            }
        }).catch(error => {
            console.log(error)
            this.setState({
                msgModal: true,
                msgTitle: "Error",
                msgText: `Hubo un error inesperado consultando si existe el RUT. COD (${statusResponse})`
            })
        })
    }

    emailExist = (email) => {
        let statusResponse = 0;
        return fetch(`${SERVER_API}/login/email_exist`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                email
            })
        }).then(async response => {
            statusResponse = response.status
            return await response.json()
        }).then(response => {
            switch (statusResponse) {
                case 200:
                    return false
                case 400:
                    return true
                default:
                    this.setState({
                        msgModal: true,
                        msgTitle: "Error inesperado",
                        msgText: `Hubo un error al validar si existe el correo electrónico. COD (${statusResponse})`
                    })
                    break;
            }
        }).catch(error => {
            console.log(error)
            this.setState({
                msgModal: true,
                msgTitle: "Error",
                msgText: `Hubo un error al consultar si existe el correo electrónico. COD (${statusResponse})`
            })
        })
    }

    render() {
        return (
            <PageComponent>
                <Card className='col-lg-4 offset-md-4 my-4 body-form-ami' >
                    <Card.Body >
                        <div className="text-center">
                            <h3>Ingresa tus datos</h3>
                            <p className='text-muted' >
                                Donde estés, estamos para ti, siempre podrás comprar con tu amiPASS
                            </p>
                        </div>
                        <Form onSubmit={this.register} action={"#"} method={'post'} >
                            <Form.Group>
                                <Form.Control
                                    required
                                    type="text"
                                    name='firstName'
                                    placeholder="Nombre"
                                    onChange={e => this.setState({ firstName: e.target.value })}
                                    isInvalid={!this.state.validationForm.firstName}
                                    onBlur={e => {
                                        const { value } = e.target;
                                        this.setState({
                                            validationForm: {
                                                ...this.state.validationForm,
                                                firstName: (value.length > 2)
                                            }
                                        })
                                    }}
                                />
                                <Form.Control.Feedback type="invalid">Indique su nombre aquí</Form.Control.Feedback>
                            </Form.Group>
                            <Form.Group>
                                <Form.Control
                                    required
                                    type="text"
                                    name='lastName'
                                    placeholder="Apellidos"
                                    onChange={e => this.setState({ lastName: e.target.value })}
                                    isInvalid={!this.state.validationForm.lastName}
                                    onBlur={e => {
                                        const { value } = e.target;
                                        this.setState({
                                            validationForm: {
                                                ...this.state.validationForm,
                                                lastName: (value.length > 2)
                                            }
                                        })
                                    }}
                                />
                                <Form.Control.Feedback type="invalid">Debe escribir sus apellidos</Form.Control.Feedback>
                            </Form.Group>
                            <Form.Group>
                                <Form.Control
                                    required
                                    type="text"
                                    name='rut'
                                    placeholder="RUT Ej. 12345678-9"
                                    onChange={e => {
                                        const { value } = e.target
                                        this.setState({ numDocument: value.toUpperCase().replace(/\./g, '').replace(/-/g, '') })
                                    }}
                                    isInvalid={!this.state.validationForm.rut}
                                    onBlur={async e => {
                                        const { value } = e.target
                                        if(await this.documentExist(value.toUpperCase().replace(/\./g, '').replace(/-/g, ''))) {
                                            this.setState({
                                                validationForm: {
                                                    ...this.state.validationForm,
                                                    rut: false,
                                                    documentExist: true,
                                                }
                                            })
                                        } else {
                                            this.setState({
                                                validationForm: {
                                                    ...this.state.validationForm,
                                                    rut: rutIsValid(value),
                                                    documentExist: false,
                                                }
                                            })
                                        }
                                    }}
                                />
                                {
                                    this.state.validationForm.documentExist ?
                                    <Form.Control.Feedback type="invalid">El RUT ya existe</Form.Control.Feedback>
                                    :
                                    <Form.Control.Feedback type="invalid">Debe indicar un RUT válido</Form.Control.Feedback>
                                }
                            </Form.Group>
                            <Form.Group>
                                <Form.Control
                                    required
                                    type="email"
                                    name='email'
                                    placeholder="Correo electrónico"
                                    onPaste={e => { 
                                        e.preventDefault() 
                                        return false 
                                    }}
                                    onChange={e => this.setState({ email: e.target.value })}
                                    isInvalid={!this.state.validationForm.email}
                                    onBlur={async e => {
                                        const { value } = e.target;
                                        if(await this.emailExist(value)) {
                                            this.setState({
                                                validationForm: {
                                                    ...this.state.validationForm,
                                                    email: false,
                                                    emailExist: true
                                                }
                                            })
                                        } else {
                                            let mail = value.split('@')
                                            this.setState({
                                                validationForm: {
                                                    ...this.state.validationForm,
                                                    email: (value.split('@').length > 1 && mail[1].indexOf(".") > 1),
                                                    emailExist: false
                                                }
                                            })
                                        }
                                    }}
                                />
                                {
                                    this.state.validationForm.emailExist ? 
                                    <Form.Control.Feedback type="invalid">El email ya existe</Form.Control.Feedback>
                                    :
                                    <Form.Control.Feedback type="invalid">Escriba un correo electrónico</Form.Control.Feedback>
                                }
                            </Form.Group>
                            <Form.Group>
                                <Form.Control required
                                    type="email"
                                    name='emailConfirm'
                                    placeholder="Confirma correo electrónico"
                                    onPaste={e => { 
                                        e.preventDefault() 
                                        return false 
                                    }}
                                    onChange={e => {
                                        this.setState({ emailConfirm: e.target.value })
                                    }}
                                    isInvalid={!this.state.validationForm.emailConfirm}
                                    onBlur={e => {
                                        const { value } = e.target;

                                        let arroba = (value.split('@').length > 1)
                                        let match = (this.state.email === this.state.emailConfirm)
                                        let length = value.length > 1

                                        this.setState({
                                            validationForm: {
                                                ...this.state.validationForm,
                                                emailConfirm: (arroba && match && length)
                                            }
                                        })
                                    }}
                                />
                                <Form.Control.Feedback type="invalid">La confirmación del correo no coincide</Form.Control.Feedback>
                            </Form.Group>
                            <Form.Group>
                                <Form.Control 
                                    required 
                                    type="password" 
                                    name='password'
                                    placeholder="Contraseña"
                                    minLength={8}
                                    maxLength={12}
                                    onChange={e => {
                                        this.passwordStrength(e.target.value)
                                        this.setState({ password: e.target.value })
                                    }}
                                    isInvalid={!this.state.validationForm.password}
                                    onBlur={e => {
                                        this.passwordStrength(e.target.value)
                                        this.setState({
                                            validationForm: {
                                                ...this.state.validationForm,
                                                password: (
                                                    this.state.validationForm.strength.number && 
                                                    this.state.validationForm.strength.length && 
                                                    this.state.validationForm.strength.mixed
                                                )
                                            }
                                        })
                                    }}
                                />
                                <Form.Control.Feedback type="invalid">La contraseña no es válida</Form.Control.Feedback>
                                <small>
                                    <span>Usa de 8 a 12 caracteres con una combinación de letras, números y símbolos</span>
                                    <ul className='list-unstyled ml-3'>
                                        <li className={(this.state.validationForm.strength.length) ? 'text-success' : 'text-muted'} >8 a 12 caracteres</li>
                                        <li className={(this.state.validationForm.strength.mixed) ? 'text-success' : 'text-muted'} >1 mayúscula</li>
                                        <li className={(this.state.validationForm.strength.number) ? 'text-success' : 'text-muted'} >1 número</li>
                                    </ul>
                                </small>
                            </Form.Group>
                            <Form.Group>
                                <Form.Control
                                    required
                                    type="password"
                                    name='passwordConfirm'
                                    placeholder="Confirmar contraseña"
                                    minLength={7}
                                    maxLength={12}
                                    onChange={e => this.setState({ passwordConfirm: e.target.value })}
                                    isInvalid={!this.state.validationForm.passwordConfirm}
                                    onBlur={e => {
                                        const { value } = e.target;
                                        this.setState({
                                            validationForm: {
                                                ...this.state.validationForm,
                                                passwordConfirm: (this.state.password === value)
                                            }
                                        })
                                    }}
                                />
                                <Form.Control.Feedback type="invalid">La contraseña no coincide</Form.Control.Feedback>
                            </Form.Group>
                            <Form.Group className='text-center' >
                                <Button variant="primary" type="submit" disabled={this.state.isLoading}>
                                    {this.state.isLoading ? 'Registrando...' : 'Registrar'}
                                </Button>
                            </Form.Group>
                        </Form>
                    </Card.Body>
                </Card>
                <ModalComponent 
                    show={this.state.showModal}
                    title="Cuenta creada con éxito"
                    handleClose={this.onRedirect}
                >
                    <p>Te hemos enviado un mail a tu correo electrónico <strong>{this.state.email}</strong> para que actives tu cuenta.</p>
                </ModalComponent>
                <ModalComponent
                    show={this.state.msgModal} 
                    handleClose={() => this.setState({ msgModal: false })}
                    title={this.state.msgTitle}
                >
                    <p>{this.state.msgText}</p>
                </ModalComponent>
            </PageComponent>
        )
    }
}
