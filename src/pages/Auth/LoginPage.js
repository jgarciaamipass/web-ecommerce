import React, { Component } from 'react'
import { connect } from 'react-redux';
import PageComponent from '../../component/PageComponent'
import menuAuthReducerAction from '../../redux/actions/menuAuthReducerAction'
import { Form, Button, Card } from 'react-bootstrap'
import { Link } from 'react-router-dom';
import ModalComponent from '../../component/system/ModalComponent';
import { SERVER_API } from '../../utils/utils';
import { database } from '../../utils/firebase'

// import firebaseConfig from '../../utils/firebaseConfig'
// import firebaseApp from 'firebase/app'


// const baseURL = '/api/v1'

class LoginPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
          email: "",
          password: "",
          remember: false,
          isLoading: false,
          msgModal: false,
          msgText: '',
          msgTitle: '',
          userGoogle: {}
        };
    }

    componentDidMount = () => {
        
    }

    componentWillMount() {
        const { history } = this.props;
        if(localStorage.getItem('jwt') !== null) {
            history.push('/home')
        }
    }

    handleCloseModal = () => {
        this.setState({ showModal: false })
    }

    login = (e) => {
        const { email, password, remember } = this.state;
        let statusResponse = 0;
        this.setState({ isLoading: true })
        fetch(`${SERVER_API}/login`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                email,
                password,
                remember
            })
        }).then(async response => {
            statusResponse = response.status;
            return await response.json();
        }).then(async response => {
            switch (statusResponse) {
                case 200:
                    const { jwt, iduser, fullname, rememberToken } = response
                    if(typeof jwt == 'undefined') return

                    localStorage.setItem('jwt', jwt)
                    localStorage.setItem('tkey', rememberToken)
                    localStorage.setItem('user', JSON.stringify({ 
                        codeUser: iduser,
                        username: iduser, 
                        email: this.state.email,
                        fullname
                    }))
                    this.props.menuAuthReducerAction({ isAuth: true })

                    if(localStorage.getItem('userTemp')) {
                        const userTemp = await database.collection(`${localStorage.getItem('userTemp')}`)
                        const carts = await userTemp.get();
                        if(carts.docs.length > 0) {
                            if(window.confirm('¿Quieres seguir con tu compra?')) {
                                carts.forEach(async item => {
                                    let data = item.data();
                                    let cart = await database
                                    .collection(`${iduser}`)
                                    .doc(`cart-${data.commerce}${data.store}`)
                                    
                                    cart.set(data)
    
                                    userTemp
                                    .doc(item.id)
                                    .delete()
                                })
                            } else {
                                carts.forEach(async item => {
                                    userTemp
                                    .doc(item.id)
                                    .delete()
                                })
                            }
                        }
                    }

                    localStorage.removeItem('userTemp')

                    this.props.history.push(`/home`)
                    break;
                case 401:
                    this.setState({
                        msgModal: true,
                        msgTitle: "Error",
                        msgText: "Usuario o clave inválidos"
                    })
                    break;
                case 403:
                    this.setState({
                        msgModal: true,
                        msgTitle: "¡Atención!",
                        msgText: "El usuario se encuentra bloqueado. Debe activar su cuenta para iniciar sesión"
                    })
                    break;
                case 404:
                    this.setState({
                        msgModal: true,
                        msgTitle: "Error ingresando",
                        msgText: `El usuario ${this.state.email} no existe, por favor registrese o ingrese un email registrado`
                    })
                    break;
                default:
                    this.setState({
                        msgModal: true,
                        msgTitle: "Error inesperado",
                        msgText: `Hubo un error inesperado iniciando sesión. COD (${statusResponse})`
                    })
                    break;
            }
            this.setState({ isLoading: false })
        }).catch(error => {
            console.log(error)
            this.setState({
                msgModal: true,
                msgTitle: "Error",
                msgText: `Hubo un error intentando de iniciando sesión. COD (${statusResponse})`
            })
        })
        e.preventDefault();
    }

    loginGoogle = async () => {
        // if(typeof this.state.userGoogle.credential === 'undefined') {
        //     const provider = new firebaseApp.auth.GoogleAuthProvider()
        //     await firebaseApp.auth().signInWithPopup(provider).then(response => {
        //         this.setState({ userGoogle: response })
        //         console.log(this.state.userGoogle)
        //     }).catch(error => {
        //         console.log(error)
        //     })
        // } else {
        //     await firebaseApp.auth().signOut().then(response => {
        //         this.setState({ userGoogle: {} })
        //         console.log(this.state.userGoogle)
        //     }).catch(error => {
        //         console.log(error)
        //     })
        // }
    }

    render() {
        return (
            <PageComponent>
                <div className="d-flex align-items-center h-100" >
                    <div className='d-flex flex-column align-items-center w-100' >
                        <Card className='col-lg-4 col-md-8 body-form-ami' >
                            <Card.Body>
                                <div className='text-center' >
                                    <h3>Ingresa tus datos</h3>
                                    <p>
                                        Donde estés, estamos para ti, siempre podrás comprar con tu amiPASS
                                    </p>
                                </div>
                                <Form onSubmit={this.login} action={"#"} method={'POST'} >
                                    <Form.Group>
                                        <Form.Control required type="email" placeholder="Correo electrónico" onChange={ e => this.setState({ email: e.target.value }) } />
                                    </Form.Group>
                                    <Form.Group>
                                        <Form.Control required type="password" placeholder="Contraseña" onChange={ e => this.setState({ password: e.target.value })} />
                                    </Form.Group>
                                    <Form.Group controlId="formBasicCheckbox">
                                        <Form.Check value={this.state.remember} onChange={() => this.setState({ remember: !this.state.remember })} type="checkbox" label="Recordar sesión" />
                                    </Form.Group>
                                    <Button variant="primary" type="submit" block disabled={this.state.isLoading}>
                                        {this.state.isLoading ? 'Iniciando sesión...' : 'Ingresar'}
                                    </Button>
                                    {/* <Button variant="primary" block
                                        onClick={this.loginGoogle}
                                    >
                                        Iniciar con Google
                                    </Button> */}
                                    <div className="mt-3 text-center " >
                                        <Link href="#" to={'/forget_password'} >¿Olvidaste tu contraseña?</Link>
                                    </div>
                                </Form>
                            </Card.Body>
                        </Card>
                    </div>
                </div>
                <ModalComponent
                    show={this.state.msgModal} 
                    handleClose={() => this.setState({ msgModal:false })}
                    title={this.state.msgTitle}
                >
                    <p>{this.state.msgText}</p>
                </ModalComponent>
            </PageComponent>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        menuAuthReducer: state.menuAuthReducer
    }
}

const mapDispatchToProps = {
    menuAuthReducerAction
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage)