import React, { Component } from 'react'
import { Button, Card, Form } from 'react-bootstrap'
import PageComponent from '../../component/PageComponent'
import ModalComponent from '../../component/system/ModalComponent'
import { SERVER_API } from '../../utils/utils'

export default class ForgetPasswordPage extends Component {
    constructor(props) {
        super(props)
        this.state= {
            email: "",
            isLoading: false,
            sended: false,
            already: false,
            msgModal: false,
            msgText: '',
            msgTitle: '',
            emailValid: true
        }
    }

    componentDidMount() {
        const { history } = this.props;
        let { token } = this.props.match.params;
        if(typeof token !== 'undefined') {
            history.push(`/change_password/${token}`)
        }
    }

    sendForgetPassword = (e) => {
        if(this.state.emailValid) {
            const { email } = this.state;
            let statusResponse = 0
            this.setState({ isLoading: true })
            fetch(`${SERVER_API}/login/forgot_password`, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    email
                })
            }).then(async response => {
                statusResponse = response.status
                console.log(response)
                return await response.json();
            }).then(response => {
                console.log(response)
                switch (statusResponse) {
                    case 200:
                        this.setState({ sended: true })
                        break;
                    case 208:
                        this.setState({ already: true, sended: true })
                        break;
                    case 401:
                        this.setState({
                            msgModal: true,
                            msgTitle: "¡Atención!",
                            msgText: `El usuario no está activo. Revise su correo en la bandeja de entrada o spam "Activa tu cuenta amiMARKET". COD (${statusResponse})`
                        })
                        break;
                    case 404:
                        this.setState({
                            msgModal: true,
                            msgTitle: "¡Atención!",
                            msgText: `Usuario no encontrado, por favor registrese o ingrese un email registrado. COD (${statusResponse})`
                        })
                        break;
                    default:
                        this.setState({
                            msgModal: true,
                            msgTitle: "Error inesperado",
                            msgText: `Hubo un error inesperado en la solicitud de cambio de contraseña. COD (${statusResponse})`
                        })
                        break;
                }
                this.setState({ isLoading: false })
            }).catch(error => {
                console.log(error)
                this.setState({ 
                    isLoading: false,
                })
                this.setState({
                    msgModal: true,
                    msgTitle: "Error",
                    msgText: `Hubo un error intentando de solicitar el cambio de contraseña. COD (${statusResponse})`
                })
            })
        }
        e.preventDefault();
    }

    render() {
        return (
            <PageComponent>
                <div className="d-flex align-items-center h-100" >
                    <div className='d-flex flex-column align-items-center w-100' >
                        {
                            !this.state.sended ? 
                            <Card className='col-lg-4 col-md-8 body-form-ami' >
                                <Card.Body className="text-center" >
                                    <h3>Recuperar contraseña</h3>
                                    <p>
                                    Ingresa tu correo electrónico para recuperar la contraseña 
                                    </p>
                                    <Form onSubmit={this.sendForgetPassword} action={"#"} method={'POST'} >
                                        <Form.Group>
                                            <Form.Control 
                                                required 
                                                type="email" 
                                                placeholder="Correo electrónico" 
                                                isInvalid={!this.state.emailValid}
                                                onChange={ e => {
                                                    this.setState({ email: e.target.value })
                                                    } 
                                                }
                                                onBlur={ e => {
                                                    const { value } = e.target;
                                                    let mail = value.split('@')
                                                    this.setState({ 
                                                        emailValid: (value.split('@').length > 1 && mail[1].indexOf(".") > 1),
                                                    })
                                                    console.log(this.state.emailValid)
                                                }}
                                            />
                                            <Form.Control.Feedback type="invalid">Escriba un correo electrónico válido</Form.Control.Feedback>
                                        </Form.Group>
                                        <Button variant="primary" type="submit" block disabled={this.state.isLoading}>
                                            {this.state.isLoading ? 'Enviando...' : 'Continuar'}
                                        </Button>
                                    </Form>
                                </Card.Body>
                            </Card> :
                            <Card className='col-lg-6 col-md-8 body-form-ami card' >
                                <Card.Body className="text-center" >
                                    <h3>Recuperar contraseña</h3>
                                    {
                                        !this.state.already ?
                                        <>
                                        <p>
                                            Te hemos enviado un email para que puedas cambiar la contraseña
                                        </p>
                                        </> :
                                        <>
                                        <p>
                                            Ya hemos enviado el email para que puedas cambiar la contraseña al correo <strong>{`${this.state.email.charAt(0)}****${this.state.email.slice(this.state.email.indexOf("@") )}`}</strong>. 
                                            <br/>Verifica tu bandeja de SPAM en caso de que no lo hayas recibido en tu bandeja de entrada
                                        </p>
                                        </>
                                    }
                                </Card.Body>
                            </Card>
                        }
                    </div>
                </div>
                <ModalComponent
                    show={this.state.msgModal} 
                    handleClose={() => this.setState({ msgModal:false })}
                    title={this.state.msgTitle}
                >
                    <p>{this.state.msgText}</p>
                </ModalComponent>
            </PageComponent>
        )
    }
}
