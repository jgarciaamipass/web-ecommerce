import React, { Component } from 'react'
import { connect } from 'react-redux';
import menuAuthReducerAction from '../../redux/actions/menuAuthReducerAction'

class LogoutPage extends Component {

    componentDidMount() {
        localStorage.clear();
        this.props.menuAuthReducerAction({ isAuth: false })
        this.props.history.push('/')
    }

    render() {
        return (
            <div>
                
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        menuAuthReducer: state.menuAuthReducer
    }
}

const mapDispatchToProps = {
    menuAuthReducerAction
}

export default connect(mapStateToProps, mapDispatchToProps)(LogoutPage)