import React, { Component } from 'react'
import PageComponent from '../../component/PageComponent'
import PreloadComponent from '../../component/PreloadComponent'
import ModalComponent from '../../component/system/ModalComponent'
import { SERVER_API } from '../../utils/utils'
import { database } from '../../utils/firebase'

export default class ActivateAccountPage extends Component {
    constructor(props) {
        super(props)
        this.state= {
            isLoading: false,
            msgModal: false,
            msgText: '',
            msgTitle: ''
        }
    }
    componentDidMount() {
        let { token } = this.props.match.params;
        
        this.setState({ isLoading: true })
        let statusResponse = 0;
        fetch(`${SERVER_API}/login/activate_account/${token}`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            }
        }).then(async response => {
            statusResponse = response.status;
            return await response.json();
        }).then(async response => {
            this.setState({ isLoading: false })
            switch (statusResponse) {
                case 200:
                    window.dataLayer.push({'event': 'accountCreationFailed'})
                    const { id } = response
                    console.log(response)
                    if(localStorage.getItem('userTemp')) {
                        const userTemp = await database.collection(`${localStorage.getItem('userTemp')}`)
                        const carts = await userTemp.get();
                        if(carts.docs.length > 0) {
                            if(window.confirm('¿Quieres seguir con tu compra?')) {
                                carts.forEach(async item => {
                                    let data = item.data();
                                    let cart = await database
                                    .collection(`${id}`)
                                    .doc(`cart-${data.commerce}${data.store}`)
                                    
                                    cart.set(data)
    
                                    userTemp
                                    .doc(item.id)
                                    .delete()
                                })
                            } else {
                                carts.forEach(async item => {
                                    userTemp
                                    .doc(item.id)
                                    .delete()
                                })
                            }
                        }
                    }
                    this.setState({
                        msgModal: true,
                        msgTitle: "Cuenta activada",
                        msgText: "Activaste tu cuenta, inicia sesión y disfruta de los productos, servicios y promociones de amiMARKET."
                    })
                    break;
                case 404:
                    this.setState({
                        msgModal: true,
                        msgTitle: "¡Atención!",
                        msgText: "Esta cuenta ya se encuentra activa"
                    })
                    break;
                default:
                    window.dataLayer.push({'event': 'accountCreationSuccessful'})
                    this.setState({
                        msgModal: true,
                        msgTitle: "Error inesperado",
                        msgText: `Hubo un error inesperado activando la cuenta. COD (${statusResponse})`
                    })
                    break;
            }
        }).catch(error => {
            console.log(error)
            this.setState({ 
                isLoading: false,
            })
            this.setState({
                msgModal: true,
                msgTitle: "Error",
                msgText: `Hubo un error procesando la activación de la cuenta. COD (${statusResponse})`
            })
        })
    }
    render() {
        return (
            <PageComponent>
                {
                    this.state.isLoading ? <PreloadComponent/> :
                    <ModalComponent
                        show={this.state.msgModal} 
                        handleClose={() => {
                            this.props.history.push('/home')
                        }}
                        title={this.state.msgTitle}
                    >
                        <p>{this.state.msgText}</p>
                    </ModalComponent>
                }
            </PageComponent>
        )
    }
}
