import React, { Component } from 'react'
import PageComponent from '../../component/PageComponent'

export default class Error404Page extends Component {
    render() {
        return (
            <PageComponent>
                <div className="d-flex align-items-center h-100" >
                    <div className='d-flex flex-column align-items-center w-100' >
                        <div className='col-6 text-center' >
                            <h1 className='display-1 text-primary' >404</h1>
                            <h3>¡Ups! No se encontró nada</h3>
                            <p>
                                Es posible que la página que esté buscando se haya eliminado 
                                si se cambió el nombre o no está disponible temporalmente. Regresar a la <a href='/' >página principal</a>
                            </p>
                        </div>
                    </div>
                </div>
            </PageComponent>
        )
    }
}
