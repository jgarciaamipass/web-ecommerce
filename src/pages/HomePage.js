import React, { Component } from 'react'
import { connect } from 'react-redux'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faMapMarkerAlt } from '@fortawesome/free-solid-svg-icons'
import { faSearch } from '@fortawesome/free-solid-svg-icons'
import { faFilter } from '@fortawesome/free-solid-svg-icons'

import PageComponent from '../component/PageComponent'
import PreloadComponent from '../component/PreloadComponent'
import CommerceFromListComponent from '../component/CommerceFromListComponent'
import ModalComponent from '../component/system/ModalComponent'
import CoverComponent from '../component/CoverComponent'
import SearchLocationInput from '../component/SearchLocationInput'

import countItemCarReducerAction from '../redux/actions/countItemCarReducerAction';
import itemsCarReducerAction from './../redux/actions/itemsCarReducerAction';

import bgImage from './../assets/images/slider-1920x350-1.jpg'

import { SERVER_API } from '../utils/utils'

class HomePage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            coords: {},
            msgShow: false,
            msgText: "",
            msgTitle: "",
            address: {},
            showModalAddress: false,
            categories: [],
            communes: [],
            commerces: [],
            commerceSearched: [],
            commercesByCategories: [],
            commercesByCommune: [],
            searchText: "",
            typeFilter: "lc",
            loadingCommerces: true,
            msgModalFinderEmpty: false,
            modalFilters: false,
            delivery: false,
            withdrawal: false
        }
    }

    componentWillMount() {
        localStorage.removeItem('car')
        localStorage.removeItem('shopping')
        localStorage.removeItem('delivery')
        localStorage.removeItem('accountsAmipass')
        if(localStorage.getItem('tkey') === "null") {
            localStorage.removeItem('tkey')
        }
        this.props.itemsCarReducerAction({})
        this.props.countItemCarReducerAction({
            count: 0,
            amount: 0,
            items: []
        })
        if(localStorage.getItem('address') === null) {
            this.props.history.push(`/`)
            return false
        }
        this.setState({
            address: JSON.parse(localStorage.getItem('address'))
        })
        
        Promise.all([
            this.onSearch()
        ]).catch(error => {
            console.log(error)
            localStorage.removeItem('jwt')
            localStorage.removeItem('user')
            this.setState({
                msgShow: true,
                msgTitle: "Error",
                msgText: 'Tuvimos un error al intentar de cargar los comercios. Por favor intente más tarde.'
            })
        })
        if(!localStorage.getItem('user') & !localStorage.getItem('userTemp')) {
            localStorage.setItem('userTemp', this.makeId(20))
        }
    }

    makeId = (length) => {
        var result = '';
        var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for ( var i = 0; i < length; i++ ) {
           result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }

    onSearch = () => {
        let { geometry, address_components } = JSON.parse(localStorage.getItem('address'))
        let commune = address_components.filter(item => {
            let level3 = item.types.filter(type => {
                return (type === 'administrative_area_level_3')
            })

            return (level3.length > 0) ? item : false
        })[0].long_name
        this.setState({
            commerceSearched: [],
            commerces: [],
            loadingCommerces: true
        })
        let statusResponse = 0;
        fetch(`${SERVER_API}/commerce`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                page: 0,
                chunk: 99999,
                latitude: geometry.location.lat,
                longitude: geometry.location.lng,
                radius: 20,
                commune,
                key: this.state.searchText,
                delivery: this.state.delivery,
                withdrawal: this.state.withdrawal
            })
        }).then(async response => {
            statusResponse = response.status
            return await response.json();
        }).then(async response => {
            switch (statusResponse) {
                case 200:
                    let commerces = response;
                    if(commerces.length === 0) {
                        this.setState({ msgModalFinderEmpty: true })
                    }
                    commerces = commerces.filter(item => item.storeCategory !== "")
                    this.setState({ 
                        commerces: commerces.sort((a,b) => (a.distance > b.distance) ? 1 : (b.distance > a.distance) ? -1 : 0) 
                    });
                    let commercesByCategories = commerces.reduce((r, item) => {
                        r[item.storeCategory] = [...r[item.storeCategory] || [], item];
                        return r;
                    }, {})

                    let commercesByCommune = commerces.reduce((r, item) => {
                        r[item.commune] = [...r[item.commune] || [], item];
                        return r;
                    }, {})

                    this.setState({ commercesByCategories, commercesByCommune })
                    let categories = Object.keys(commercesByCategories)
                    let communes = Object.keys(commercesByCommune)
                    this.setState({ 
                        categories: categories.sort((a,b) => { return a.localeCompare(b) }), 
                        communes, 
                        loadingCommerces: false 
                    });
                    break;
                default:
                    this.setState({
                        msgModal: true,
                        msgTitle: "Error inesperado",
                        msgText: `Hubo un error inesperado al intentar de mostrar los comercios. Por favor intente más tarde. COD (${statusResponse})`
                    })
                    break;
            }

        }).catch(error => { throw Error(error) })
    }

    titleCase = (string) => {
        let str = string.toLowerCase().split(" ");
        for(let i = 0; i< str.length; i++){
           str[i] = str[i].charAt(0).toUpperCase() + str[i].slice(1) + " ";
        }
        return String(str).replace(',', '').trim()
    }

    filterByCategories = (text) => {
        const newData = this.state.commerces.filter(item => {
            const itemData = `${item.storeCategory}`.toUpperCase()
            const textComparer = text.toUpperCase()
            return itemData.indexOf(textComparer) > -1
        })
        return newData;
    }

    render() {
        return (
            <PageComponent>
                <CoverComponent
                    cover={bgImage}
                >
                    <h3 className='text-center-m d-desktop color-white'>¿Qué buscas hoy?</h3>
                    {
                        this.state.address && <span className='text-center-m color-white'>{this.state.address.formatted_address}</span>
                    }
                    <button 
                        type='button' 
                        className='btn btn-primary btn-sm btn-other-address' 
                        onClick={() => {
                            window.dataLayer.push({'event': 'buttonOtherAddressCommerce'})
                            this.setState({ showModalAddress: true })
                        }}
                        >
                        {
                            this.state.address ? <span><FontAwesomeIcon icon={faMapMarkerAlt} /> Otra dirección</span> : <span><FontAwesomeIcon icon={faMapMarkerAlt} /> Una dirección</span>
                        }
                    </button>
                </CoverComponent>
                <div className='container py-2' >
                    <div className='row' >
                        <div className='card search-component col'>
                            <div className='card-body col text-center card-search-body'>
                                <div className="" >
                                    <h4 className='text-center-m d-movil text-bold'>¿Qué buscas hoy?</h4>
                                    <h4 className='font-semibold description-search-m d-movil d-desktop'>Puedes realizar búsquedas por nombre de local o producto</h4>
                                </div>
                                <form className="d-flex justify-content-center padding-movil-search" action='#' onSubmit={(e) => { e.preventDefault(); this.onSearch() }} >
                                    <div className="form-group mr-3 col-md-6 field-search-m">
                                        <input
                                            className='form-control'
                                            type='text'
                                            placeholder="Busca por nombre de local o producto"
                                            onChange={(e) => {
                                                this.setState({
                                                    searchText: e.target.value
                                                })
                                            }}
                                        />
                                    </div>
                                    <div className='form-group mr-3' >
                                        <button type='submit' 
                                            className='btn btn-primary padding-button d-desktop'
                                            onClick={() => {
                                                window.dataLayer.push({'event': 'clickButtonCommerceSearch'})
                                                this.onSearch()
                                            }}
                                        ><FontAwesomeIcon icon={faSearch} /> Buscar</button>
                                        <button type='submit' className='btn btn-primary padding-button-m d-movil'><FontAwesomeIcon icon={faSearch} /></button>
                                    </div>
                                </form>
                                <div className="d-movil">
                                    <button type='button' className='btn-amipass-gray font-size-14' onClick={() => { 
                                        this.setState({ modalFilters: true }) 
                                        console.log('pasa')
                                    }} >Filtrar por<FontAwesomeIcon className="icon-filter" icon={faFilter} /></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='row py-4 py-4-movil'>
                        <div className='col-md-3 categories-div d-desktop' >
                            <div>
                                <div className=''><h6>Tipo de entrega</h6></div>
                                <div className='py-2'>
                                    <div className='list-categories'>
                                        <ul className='list-unstyled list-ul' >                                            
                                            <li className='m-l-20 switch-li'>
                                                <div className="switch-div d-flex">
                                                    <span className='mr-2 switch-text'>Delivery</span>
                                                    <label className='switch' >
                                                        <input 
                                                            type="checkbox" 
                                                            value={(this.state.delivery) ? '' : 'on'}
                                                            checked={this.state.delivery}
                                                            onChange={(e) => {
                                                                window.dataLayer.push({'event': 'clickFilterDelivery'})
                                                                this.setState({ 
                                                                    delivery: !this.state.delivery, 
                                                                    withdrawal: false
                                                                }, () => {
                                                                    this.onSearch()
                                                                })
                                                            }}
                                                        />
                                                        <span className="switch-slider switch-color-delivery round"></span>
                                                    </label>
                                                </div>
                                                
                                            </li>
                                            <li className='m-l-20 switch-li'>
                                                <div className="switch-div d-flex">
                                                    <span className='mr-2 switch-text'>Retiro en local</span>
                                                    <label className='switch' >
                                                    <input type="checkbox"
                                                        value={(this.state.withdrawal) ? '' : 'on'} 
                                                        checked={this.state.withdrawal}
                                                        onChange={(e) => {
                                                            window.dataLayer.push({'event': 'clickFilterPickUp'})
                                                            this.setState({ 
                                                                withdrawal: !this.state.withdrawal,
                                                                delivery: false
                                                            }, () => {
                                                                this.onSearch()
                                                            })
                                                        }}
                                                    />
                                                        <span className="switch-slider switch-color-withdrawal round"></span>
                                                    </label>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div className=''><h6>Categorías</h6></div>
                                {
                                    this.state.loadingCommerces ?
                                        <PreloadComponent/>
                                    :
                                    this.state.commerces.length === 0 ? 
                                        <div className='text-center py-2'>
                                            <h5>Sin categorías</h5>
                                        </div>
                                    :
                                    <>
                                        <div className='py-2'>
                                            <div className='list-categories'>
                                                <ul className='list-unstyled list-ul' >
                                                    {
                                                        this.state.categories.map((item, index) => {
                                                            let count = (typeof this.state.commercesByCategories[item] !== 'undefined') ? this.state.commercesByCategories[item].length : 0
                                                            return (
                                                                <li className='text-muted m-l-20' key={index} >
                                                                    <button className='btn btn-link btn-sm text-categories text-filter' 
                                                                        value={`${item}`.trim()} 
                                                                        onClick={(e) => {
                                                                            window.dataLayer.push({
                                                                                event: 'clickFilterCategories',
                                                                                labelName: `${item}`.trim()
                                                                            })
                                                                            let newData = this.filterByCategories(e.target.value);
                                                                            this.setState({
                                                                                commerceSearched: newData
                                                                            })
                                                                        }}
                                                                    >{item}</button>
                                                                    <span className="badge badge-pill badge-primary badge-primary-filter tag-categories m-l-4">{count}</span>
                                                                </li>
                                                            )
                                                        })
                                                    }
                                                    <li className='text-muted m-l-20'>
                                                        <button className='btn btn-link btn-sm text-categories text-filter' 
                                                            value="*" 
                                                            onClick={(e) => {
                                                                let newData = this.filterByCategories(e.target.value);
                                                                this.setState({
                                                                    commerceSearched: newData
                                                                })
                                                            }}
                                                        >Todas</button>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </>
                                }
                            </div>
                        </div>
                        <div className='col-md-9' >
                            <div className=' d-desktop d-movil' >
                                <h6>¿Qué estás buscando? <br /> <small>Selecciona el tipo de local que necesitas</small></h6>
                            </div>
                            <div className='py-2' >
                                {   
                                    this.state.loadingCommerces ?
                                        <PreloadComponent/>
                                    :
                                    (this.state.commerceSearched.length > 0) ?
                                        this.state.commerceSearched.map(item => (
                                            <CommerceFromListComponent key={item.internalID} data={item} />
                                        )) :
                                    this.state.commerces.length === 0 ?
                                        <div className='text-center py-2'>
                                            <h3>Sin comercios en tu dirección</h3>
                                        </div>
                                        :
                                        this.state.commerces.map(item => (
                                            <CommerceFromListComponent key={item.internalID} data={item} />
                                        ))
                                }
                            </div>
                        </div>
                    </div>
                    <ModalComponent
                        show={this.state.showModalAddress}
                        title='Ingresa tu dirección'
                        // headerClose={() => this.setState({ showModalAddress: false })}
                        handleAccept={() => {
                            this.setState({
                                showModalAddress: false,
                                address: JSON.parse(localStorage.getItem('address'))
                            })
                            this.onSearch()
                        }}
                    >
                        <SearchLocationInput />
                    </ModalComponent>
                    <ModalComponent
                        show={this.state.msgModal}
                        handleClose={() => this.setState({ msgModal: false })}
                        title={this.state.msgTitle}
                    >
                        <p>{this.state.msgText}</p>
                    </ModalComponent>
                    <ModalComponent
                        show={this.state.msgModalFinderEmpty}
                        handleClose={() => this.setState({ msgModalFinderEmpty: false })}
                        handleAccept={(this.state.typeFilter === 'lc' ? () => {
                            this.props.history.push(`/recommender`)
                        } : null)}
                        textClose='Volver'
                        textAccept='Quiero sugerir este local'
                        title='¡Lo sentimos!'
                    >
                        <p className='text-center' >No encontramos los resultados para tu busqueda de <strong>{this.state.searchText}</strong></p>
                    </ModalComponent>
                    <ModalComponent
                        show={this.state.modalFilters}
                        handleClose={() => this.setState({ modalFilters: false })}
                        // handleAccept={() => this.setState({ modalFilters: false })}
                        textClose='Salir'
                        textAccept='Aplicar'
                        title='Filtrar por'
                    >
                        <div>
                            <div className=''><h6>Tipo de entrega</h6></div>
                            <div className='d-flex justify-content-center pt-2' >
                                <ul className='list-unstyled' >                                            
                                    <li className='m-l-20 switch-li'>
                                        <div className="switch-div d-flex">
                                            <span className='mr-2 switch-text'>Delivery</span>
                                            <label className='switch' >
                                                <input 
                                                    type="checkbox" 
                                                    value={(this.state.delivery) ? '' : 'on'}
                                                    checked={this.state.delivery}
                                                    onChange={(e) => {
                                                        this.setState({ 
                                                            delivery: !this.state.delivery, 
                                                            withdrawal: false
                                                        }, () => {
                                                            this.onSearch()
                                                        })
                                                    }}
                                                />
                                                <span className="switch-slider switch-color-delivery round"></span>
                                            </label>
                                        </div>
                                        
                                    </li>
                                    <li className='m-l-20 switch-li'>
                                        <div className="switch-div d-flex">
                                            <span className='mr-2 switch-text'>Retiro en local</span>
                                            <label className='switch' >
                                            <input type="checkbox"
                                                value={(this.state.withdrawal) ? '' : 'on'} 
                                                checked={this.state.withdrawal}
                                                onChange={(e) => {
                                                    this.setState({ 
                                                        withdrawal: !this.state.withdrawal,
                                                        delivery: false
                                                    }, () => {
                                                        this.onSearch()
                                                    })
                                                }}
                                            />
                                                <span className="switch-slider switch-color-withdrawal round"></span>
                                            </label>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div>
                            <div className=''><h6>Categorías</h6></div>
                            <div className='d-flex justify-content-center pt-2' >
                                {
                                    this.state.commerces.length === 0 ? 
                                        <div className='text-center py-2'>
                                            <h5>Sin categorías</h5>
                                        </div>
                                    :
                                    <>
                                        <div className='py-2'>
                                            <div className='list-categories'>
                                                <ul className='list-unstyled list-ul' >
                                                    {
                                                        this.state.categories.map((item, index) => {
                                                            let count = (typeof this.state.commercesByCategories[item] !== 'undefined') ? this.state.commercesByCategories[item].length : 0
                                                            return (
                                                                <li className='text-muted m-l-20' key={index} >
                                                                    <button 
                                                                        className='btn btn-link btn-sm text-categories text-filter' 
                                                                        value={`${item}`.trim()} 
                                                                        onClick={(e) => {
                                                                            window.dataLayer.push({
                                                                                event: 'clickFilterCategories',
                                                                                labelName: `${item}`.trim()
                                                                            })
                                                                            let newData = this.filterByCategories(e.target.value);
                                                                            this.setState({
                                                                                commerceSearched: newData
                                                                            })
                                                                        }}
                                                                    >{item}</button>
                                                                    <span className="badge badge-pill badge-primary badge-primary-filter tag-categories m-l-4">{count}</span>
                                                                </li>
                                                            )
                                                        })
                                                    }
                                                </ul>
                                            </div>
                                        </div>
                                    </>
                                }
                            </div>
                        </div>
                    </ModalComponent>
                </div>
            </PageComponent>
        )
    }
}

const mapStateToProps = (state) => {
    return {

    }
}

const mapDispatchToProps = {
    itemsCarReducerAction,
    countItemCarReducerAction
}

export default connect(mapStateToProps, mapDispatchToProps)(HomePage)