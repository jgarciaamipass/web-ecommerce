import React, { Component } from 'react'
import CurrencyFormat from 'react-currency-format'
import ReactDatePicker from 'react-datepicker'
import "react-datepicker/dist/react-datepicker.css";
import es from 'date-fns/locale/es';
import CoverComponent from '../../component/CoverComponent'
import PageComponent from '../../component/PageComponent'
import ModalComponent from '../../component/system/ModalComponent'
import { SERVER_API } from '../../utils/utils'
// import SearchLocationInput from '../../component/SearchLocationInput';

export default class ProfilePage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            msgModal: false,
            msgTitle: "",
            msgText: "",
            actionModalAddress: false,
            actionModalAmipass: false,
            actionModalTransbank: false,
            actionTitle: "",
            actionText: "",
            info: {
                codeUser: 0,
                firstName: '',
                lastName: '',
                numDocument: '',
                email: '',
                phone: '',
                birthday: ''
            },
            address: [],
            accountsAmipass: [],
            accountsTransbank: [],
            security: {
                oldPassword: '',
                newPassword: '',
                confirmPassword: ''
            },
            validationForm: {
                firstName: true,
                lastName: true,
                numDocument: true,
                email: true,
                phone: true,
                birthday: true,
                security: {
                    oldPassword: '',
                    newPassword: '',
                    confirmPassword: ''
                }
            },
            addressSelected: {},
            accountsAmipassSelected: {},
            accountsTransbankSelected: {},
            showModalAddress: false
        }
    }

    componentDidMount = () => {
        let user = JSON.parse(localStorage.getItem('user'));
        Promise.all([
            this.getInfo(user),
            // this.getAddress(user),
            // this.getAccountAmipass(user),
            // this.getAccountTransbank(user)
        ])
    }

    getInfo = (user) => {
        let statusResponse = 0
        return fetch(`${SERVER_API}/profile`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('jwt')}`,
                'RememberToken': `${localStorage.getItem('tkey')}`,
            },
            body: JSON.stringify({
                email: user.email
            })
        }).then(async response => {
            statusResponse = response.status
            return await response.json();
        }).then(response => {
            switch (statusResponse) {
                case 200:
                    let { id, firstName, lastName, numDocument, email, phone, birthday } = response
                    let formatBirthday = (birthday) ? Date.parse(birthday) : ''
                    this.setState({
                        info: {
                            ...this.state.info,
                            codeUser: id,
                            firstName,
                            lastName,
                            numDocument,
                            email,
                            phone: (phone) ? phone : '',
                            birthday: formatBirthday
                        }
                    })
                    break;
                default:
                    this.setState({
                        msgModal: true,
                        msgTitle: "Error inesperado",
                        msgText: `Hubo un error inesperado. COD (${statusResponse})`
                    })
                    break;
            }
        }).catch(error => {
            console.log(error)
            this.setState({
                msgModal: true,
                msgTitle: "Error",
                msgText: `Hubo un error registrando el usuario. ${error}. COD (${statusResponse})`
            })
        })
    }

    validateInfo = () => {
        // let { firstName, lastName, numDocument, phone, birthday, email } = this.state.validationForm
        let { phone } = this.state.info
        if(phone.length !== 9) {
            this.setState({
                validationForm: {
                    ...this.state.validationForm,
                    phone: false
                }
            })
            return false
        }
        return true
    }

    updateInfo = (e) => {
        e.preventDefault();
        let statusResponse = 0
        let { codeUser, firstName, lastName, numDocument, phone, email, birthday } = this.state.info
        
        if( this.validateInfo()) {
            fetch(`${SERVER_API}/profile/update/${codeUser}`, {
                method: 'PUT',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('jwt')}`,
                    'RememberToken': `${localStorage.getItem('tkey')}`
                },
                body: JSON.stringify({
                    firstName,
                    lastName,
                    email,
                    numDocument,
                    phone, 
                    birthday
                })
            }).then(async response => {
                statusResponse = response.status
                return await response.json();
            }).then(response => {
                switch (statusResponse) {
                    case 200:
                        localStorage.setItem('user', JSON.stringify({ 
                            codeUser,
                            username: codeUser, 
                            email,
                            fullname: `${firstName} ${lastName}`
                        }))
                        this.setState({
                            msgModal: true,
                            msgTitle: "Datos personales",
                            msgText: `Datos personales actualizados correctamente`
                        })
                        break;
                    default:
                        this.setState({
                            msgModal: true,
                            msgTitle: "Error inesperado",
                            msgText: `Hubo un error inesperado consultando los datos del usuario. COD (${statusResponse})`
                        })
                        break;
                }
            })
        }
    }

    // getAddress = (user) => {
    //     let statusResponse = 0
    //     fetch(`${SERVER_API}/profile/address/find/${user.codeUser}`, {
    //         method: 'GET',
    //         headers: {
    //             'Accept': 'application/json',
    //             'Content-Type': 'application/json',
    //             'Authorization': `Bearer ${localStorage.getItem('jwt')}`
    //         },
    //     }).then(async response => {
    //         statusResponse = response.status
    //         return await response.json();
    //     }).then(response => {
    //         switch (statusResponse) {
    //             case 200:
    //                 this.setState({
    //                     address: response
    //                 })
    //                 break;
    //             default:
    //                 this.setState({
    //                     msgModal: true,
    //                     msgTitle: "Error inesperado",
    //                     msgText: `Hubo un error inesperado consultando las direcciones. COD (${statusResponse})`
    //                 })
    //                 break;
    //         }
    //     })
    // }

    // getAccountAmipass = (user) => {
    //     let statusResponse = 0
    //     fetch(`${SERVER_API}/profile/account/amipass/show/${user.codeUser}`, {
    //         method: 'GET',
    //         headers: {
    //             'Accept': 'application/json',
    //             'Content-Type': 'application/json',
    //             'Authorization': `Bearer ${localStorage.getItem('jwt')}`
    //         },
    //     }).then(async response => {
    //         statusResponse = response.status
    //         return await response.json()
    //     }).then(response => {
    //         switch (statusResponse) {
    //             case 200:
    //                 this.setState({ accountsAmipass: response })
    //                 break;
    //             default:
    //                 this.setState({
    //                     msgModal: true,
    //                     msgTitle: "Error inesperado",
    //                     msgText: `Hubo un error inesperado . COD (${statusResponse})`
    //                 })
    //                 break;
    //         }
    //     }).catch(error => {
    //         console.log(error)
    //         this.setState({
    //             msgModal: true,
    //             msgTitle: "Error",
    //             msgText: `Hubo un error. COD (${statusResponse})`
    //         })
    //     })
    // }

    // getAccountTransbank = (user) => {
    //     let statusResponse = 0
    //     fetch(`${SERVER_API}/pay/transbank/card`, {
    //         method: 'POST',
    //         headers: {
    //             'Accept': 'application/json',
    //             'Content-Type': 'application/json',
    //             'Authorization': `Bearer ${localStorage.getItem('jwt')}`
    //         },
    //         body: JSON.stringify({
    //             username: user.username,
    //             email: user.email
    //         })
    //     }).then(async response => {
    //         statusResponse = response.status
    //         return await response.json()
    //     }).then(response => {
    //         switch (statusResponse) {
    //             case 200:
    //                 this.setState({
    //                     accountsTransbank: response
    //                 })
    //                 break;
    //             default:
    //                 this.setState({
    //                     msgModal: true,
    //                     msgTitle: "Error inesperado",
    //                     msgText: `Hubo un error inesperado . COD (${statusResponse})`
    //                 })
    //                 break;
    //         }
    //     }).catch(error => {
    //         console.log(error)
    //         this.setState({
    //             msgModal: true,
    //             msgTitle: "Error",
    //             msgText: `Hubo un error. COD (${statusResponse})`
    //         })
    //     })
    // }

    // addressSelected = (item) => {
    //     this.setState({
    //         addressSelected: item,
    //         actionModalAddress: true,
    //         actionTitle: "Libreta de direcciones",
    //         actionText: `¿Desea eliminar la dirección "${item.address}"?`
    //     })
    // }

    // accountsAmipassSelected = (item) => {
    //     this.setState({
    //         accountsAmipassSelected: item,
    //         actionModalAddress: true,
    //         actionTitle: "Cuenta amipass",
    //         actionText: `¿Desea la cuenta amipass "${item.employeeName} (XXXXX${item.cardCode.slice(6)})"?`
    //     })
    // }

    // accountsTransbankSelected = (item) => {
    //     this.setState({
    //         accountsTransbankSelected: item,
    //         actionModalTransbank: true,
    //         actionTitle: "Tarjeta transbank",
    //         actionText: `¿Desea eliminar la tarjeta "${item.last_four_card_digits}"?`
    //     })
    // }

    // handleAcceptAddress = () => {
    //     let statusResponse = 0
    //     this.setState({actionModalAddress: false})
    //     fetch(`${SERVER_API}/profile/address/delete/${this.state.addressSelected.idusersAddress}`, {
    //         method: 'DELETE',
    //         headers: {
    //             'Accept': 'application/json',
    //             'Content-Type': 'application/json',
    //             'Authorization': `Bearer ${localStorage.getItem('jwt')}`
    //         },
    //     }).then(async response => {
    //         statusResponse = response.status
    //         return await response.json();
    //     }).then(response => {
    //         switch (statusResponse) {
    //             case 200:
    //                 let newAddress = this.state.address.filter(item => {
    //                     return item.idusersAddress !== this.state.addressSelected.idusersAddress
    //                 })
    //                 this.setState({
    //                     address: newAddress
    //                 })
    //                 this.setState({
    //                     msgModal: true,
    //                     msgTitle: "Libreta de direcciones",
    //                     msgText: `Se eliminó la dirección satisfactoriamente`
    //                 })
    //                 break;
    //             default:
    //                 this.setState({
    //                     msgModal: true,
    //                     msgTitle: "Error inesperado",
    //                     msgText: `Hubo un error inesperado consultando las direcciones. COD (${statusResponse})`
    //                 })
    //                 break;
    //         }
    //     })
    // }

    // handleAcceptAccountAmipass = () => {

    // }
    
    // handleAcceptAccountTransbank = () => {
    //     let statusResponse = 0
    //     fetch(`${SERVER_API}/pay/transbank/card/unsubscribe`, {
    //         method: 'DELETE',
    //         headers: {
    //             'Accept': 'application/json',
    //             'Content-Type': 'application/json',
    //             'Authorization': `Bearer ${localStorage.getItem('jwt')}`
    //         },
    //         body: JSON.stringify({
    //             username: this.state.info.codeUser,
    //             id: this.state.info.email,
    //         })
    //     }).then(async response => {
    //         statusResponse = response.status
    //         return await response.text()
    //     }).then(response => {
    //         switch (statusResponse) {
    //             case 200:
    //                 console.log(response)
    //                 break;
    //             default:
    //                 this.setState({
    //                     msgModal: true,
    //                     msgTitle: "Error inesperado",
    //                     msgText: `Hubo un error inesperado . COD (${statusResponse})`
    //                 })
    //                 break;
    //         }
    //     }).catch(error => {
    //         console.log(error)
    //         this.setState({
    //             msgModal: true,
    //             msgTitle: "Error",
    //             msgText: `Hubo un error. COD (${statusResponse})`
    //         })
    //     })
    // }

    // handleModalAddress = () => {
    //     this.setState({ showModalAddress: !this.state.showModalAddress });
    // }

    // addAddress = () => {
    //     let newAddressUser = this.state.address;
    //     let address = JSON.parse(localStorage.getItem('address'))
    //     let user = JSON.parse(localStorage.getItem('user'));
    //     let statusResponse = 0
    //     fetch(`${SERVER_API}/profile/address/store`, {
    //         method: 'POST',
    //         headers: {
    //             'Accept': 'application/json',
    //             'Content-Type': 'application/json',
    //             'Authorization': `Bearer ${localStorage.getItem('jwt')}`
    //         },
    //         body: JSON.stringify({
    //             idusers: user.codeUser,
    //             address: address.formatted_address,
    //             latitude: address.geometry.location.lat,
    //             longitude: address.geometry.location.lng,
    //             streetNumber: address.address_components[0].long_name,
    //             route: address.address_components[1].long_name,
    //             locality: address.address_components[2].long_name,
    //             areaLevel3: address.address_components[3].long_name,
    //             areaLevel2: address.address_components[4].long_name,
    //             areaLevel1: address.address_components[5].long_name,
    //             country: address.address_components[6].long_name,
    //         })
    //     }).then(async response => {
    //         statusResponse = response.status
    //         return await response.json();
    //     }).then(response => {
    //         switch (statusResponse) {
    //             case 200:
    //                 newAddressUser.push(response)
    //                 this.setState({ address: newAddressUser, showModalAddress: false })
    //                 break;
    //             default:
    //                 this.setState({
    //                     msgModal: true,
    //                     msgTitle: "Error inesperado",
    //                     msgText: `Hubo un error inesperado consultando las direcciones para delivery. COD (${statusResponse})`,
    //                     showModalAddress: false
    //                 })
    //                 break;
    //         }
    //     })
    // }

    render() {
        return (
            <PageComponent>
                <CoverComponent>
                    <h3>Mi cuenta</h3>
                </CoverComponent>
                <div className='container py-2' >
                    <div className='card col-lg-6 offset-md-3 my-4 body-form-ami'>
                        <div className='card-body'>
                            <div className="p-2 text-center" >
                                <h4 className='font-semibold'>Datos personales</h4>
                            </div>
                            <form className='needs-validation' 
                                onSubmit={this.updateInfo} method='PUT' action='#' 
                                noValidate
                                ref={form => this.form = form} >
                                <div className='form-row' >
                                    <div className='form-group col-md-6' >
                                        <label htmlFor='firstName' >Nombre</label>
                                        <input className={`form-control`} 
                                            id='firstName'
                                            type='text' 
                                            required
                                            value={this.state.info.firstName}
                                            onChange={ e => {
                                                const { value } = e.target;
                                                var letters = /^[A-Za-z\s]+$/;
                                                if(value.match(letters)) {
                                                    this.setState({
                                                        info: {
                                                            ...this.state.info,
                                                            firstName: value
                                                        }
                                                    })
                                                }
                                            }}
                                            onBlur={e => {
                                                const { value } = e.target;
                                                this.setState({
                                                    validationForm: {
                                                        ...this.state.validationForm,
                                                        firstName: (value.length > 2)
                                                    }
                                                })
                                            }}
                                        />
                                    </div>
                                    <div className='form-group col-md-6' >
                                        <label htmlFor='lastName' >Apellidos</label>
                                        <input className='form-control' 
                                            id='lastName'
                                            type='text'
                                            required
                                            value={this.state.info.lastName}
                                            onChange={ e => {
                                                const { value } = e.target;
                                                var letters = /^[a-zA-Z\s]*$/;
                                                if(value.match(letters)) {
                                                    this.setState({
                                                        info: {
                                                            ...this.state.info,
                                                            lastName: value
                                                        }
                                                    })
                                                }
                                            }}
                                            onBlur={e => {
                                                const { value } = e.target;
                                                this.setState({
                                                    validationForm: {
                                                        ...this.state.validationForm,
                                                        lastName: (value.length > 2)
                                                    }
                                                })
                                            }}
                                        />
                                    </div>
                                </div>
                                <div className='form-row' >
                                    <div className='form-group col-md-6' >
                                        <label htmlFor='numDocument' >RUT</label>
                                        <input className='form-control' 
                                            readOnly
                                            id='numDocument'
                                            type='text' 
                                            required
                                            value={this.state.info.numDocument}
                                        />
                                    </div>
                                    <div className='form-group col-md-6' >
                                        <label htmlFor='phone'>Teléfono</label>
                                        <CurrencyFormat format="+56 ###-###-###" mask="_" 
                                            id='phone'
                                            type='tel'
                                            placeholder='912-344-321'
                                            required={true}
                                            value={this.state.info.phone}
                                            minLength={9}
                                            className={`form-control ${(this.state.validationForm.phone) ? '' : 'is-invalid'}`} 
                                            onValueChange={(values) => {
                                                let {value} = values;
                                                value = value.replace(/[&\\#,+_\-()$~%.'":*?<>{}\s]/g,'')
                                                this.setState({
                                                    info: {
                                                        ...this.state.info,
                                                        phone: value
                                                    }
                                                })
                                            }}
                                            onBlur={e => {
                                                let { value } = e.target;
                                                value = value.replace(/[&\\#,+_\-()$~%.'":*?<>{}\s]/g,'')
                                                this.setState({
                                                    validationForm: {
                                                        ...this.state.validationForm,
                                                        phone: (value.length === 11)
                                                    }
                                                })
                                            }}
                                        />
                                        <div className="invalid-feedback">
                                            Se requiere un número válido.
                                        </div>
                                    </div>
                                </div>
                                <div className='form-row' >
                                    <div className='form-group col-md-6' >
                                        <label htmlFor='birthday' >Fecha de nacimiento</label>
                                        <ReactDatePicker
                                            className='form-control'
                                            id='birthday'
                                            placeholderText='Año/Mes/Día'
                                            selected={this.state.info.birthday}
                                            maxDate={new Date()}
                                            dateFormat="yyyy/MM/dd"
                                            locale={es}
                                            showMonthDropdown
                                            showYearDropdown
                                            onChange={ value => {
                                                this.setState({
                                                    info: {
                                                        ...this.state.info,
                                                        birthday: value
                                                    }
                                                })
                                            }}
                                            onBlur={value => {
                                                this.setState({
                                                    validationForm: {
                                                        ...this.state.validationForm,
                                                        birthday: (value.length > 2)
                                                    }
                                                })
                                            }}
                                        />
                                    </div>
                                    <div className='form-group col-md-6' >
                                        <label htmlFor='email'>Correo electrónico</label>
                                        <input className='form-control' 
                                            id='email'
                                            type='text' 
                                            readOnly
                                            value={this.state.info.email}
                                        />
                                    </div>
                                </div>
                                <div className='text-center' >
                                    <button className='btn btn-primary' type='submit' >Actualizar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    {/* <div className='card col-lg-6 offset-md-3 my-4 body-form-ami'>
                        <div className='card-body'>
                            <div className="p-2 text-center" >
                                <h4 className='font-semibold'>Libreta de direcciones <br /> <small className='text-muted' >Administra tus direcciones</small></h4>
                            </div>
                            {
                                this.state.address.map(item => (
                                    <div key={item.idusersAddress} className='card my-4'>
                                        <div className='card-body'>
                                            <div className='row' >
                                                <div className='col-md-9' >
                                                    <small>{item.address}</small>
                                                </div>
                                                <div className='col-md-3' >
                                                    <div>
                                                        <button 
                                                            className='btn btn-link' 
                                                            onClick={() => this.addressSelected(item)}
                                                        >Eliminar</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                ))
                            }
                            <div className='text-center' >
                                <button className='btn btn-primary' type='button'
                                    onClick={() => this.setState({ showModalAddress: true })}
                                >Agregar nueva</button>
                            </div>
                        </div>
                    </div>
                    <div className='card col-lg-6 offset-md-3 my-4 body-form-ami'>
                        <div className='card-body'>
                            <div className="p-2 text-center" >
                                <h4 className='font-semibold'>Medio de pago <br /> <small className='text-muted' >Administra tus medios de pagos</small></h4>
                            </div>
                            <h5>Amipass</h5>
                            {
                                this.state.accountsAmipass.length === 0 && <div className='my-4 text-center' ><small className='text-muted' >No hay cuentas asociadas</small></div>
                            }
                            {
                                this.state.accountsAmipass.map(item => (
                                    <div key={item.cardCode} className='card my-4'>
                                        <div className='card-body'>
                                            <div className='row' >
                                                <div className='col-md-9' >
                                                    <span><strong>{item.employeeName}</strong></span><br/>
                                                    <small className='text-muted' >{item.employerName}</small><br/>
                                                    <small className='text-muted' >XXXXX{item.cardCode.slice(6)}</small>
                                                </div>
                                                <div className='col-md-3' >
                                                    <div>
                                                        <button className='btn btn-link' 
                                                            onClick={() => this.accountsAmipassSelected(item)}
                                                        >Eliminar</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                ))
                            }
                            <div className='text-center' >
                                <button className='btn btn-primary' type='submit' >Agregar nueva</button>
                            </div>
                            <hr/>
                            <h5>Transbank</h5>
                            {
                                this.state.accountsTransbank.length === 0 && <div className='my-4 text-center' ><small className='text-muted' >No hay cuentas asociadas</small></div>
                            }
                            {
                                this.state.accountsTransbank.map(item => (
                                    <div key={item.id} className='card my-4'>
                                        <div className='card-body'>
                                            <div className='row' >
                                                <div className='col-md-9' >
                                                    <span>
                                                        <small>Tarjeta {item.last_four_card_digits}</small>
                                                    </span>
                                                    <span className='float-right' ><small>{item.credit_card_type}</small></span>
                                                </div>
                                                <div className='col-md-3' >
                                                    <button className='btn btn-link' 
                                                        onClick={() => this.accountsTransbankSelected(item)}
                                                    >Eliminar</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                ))
                            }
                            <div className='text-center' >
                                <button className='btn btn-primary' type='submit' >Agregar nueva</button>
                            </div>
                        </div>
                    </div>
                    <div className='card col-lg-6 offset-md-3 my-4 body-form-ami'>
                        <div className='card-body'>
                            <div className="p-2 text-center" >
                                <h4 className='font-semibold'>Seguridad <br /> <small className='text-muted' >Puedes actualizar tu contraseña de acceso</small></h4>
                            </div>
                            <div className='form-row' >
                                <div className='form-group col-md-6' >
                                    <label htmlFor='oldPassword' >Contraseña actual</label>
                                    <input className='form-control' 
                                        id='oldPassword'
                                        type='password' 
                                        required
                                        value={this.state.security.oldPassword}
                                        onChange={ e => {
                                            const { value } = e.target;
                                            this.setState({
                                                security: {
                                                    ...this.state.security,
                                                    oldPassword: value
                                                }
                                            })
                                        }}
                                        onBlur={e => {
                                            const { value } = e.target;
                                            this.setState({
                                                validationForm: {
                                                    security: {
                                                        ...this.state.validationForm.security,
                                                        oldPassword: (value.length > 2)
                                                    }
                                                }
                                            })
                                        }}
                                    />
                                </div>
                            </div>
                            <div className='form-row' >
                                <div className='form-group col-md-6' >
                                    <label htmlFor='newPassword' >Contraseña nueva</label>
                                    <input className='form-control' 
                                        id='newPassword'
                                        type='password' 
                                        required
                                        value={this.state.security.newPassword}
                                        onChange={ e => {
                                            const { value } = e.target;
                                            this.setState({
                                                security: {
                                                    ...this.state.security,
                                                    newPassword: value
                                                }
                                            })
                                        }}
                                        onBlur={e => {
                                            const { value } = e.target;
                                            this.setState({
                                                validationForm: {
                                                    security: {
                                                        ...this.state.validationForm.security,
                                                        newPassword: (value.length > 2)
                                                    }
                                                }
                                            })
                                        }}
                                    />
                                </div>
                                <div className='form-group col-md-6' >
                                    <label htmlFor='confirmPassword' >Confirmar contraseña nueva</label>
                                    <input className='form-control' 
                                        id='confirmPassword'
                                        type='password'
                                        required
                                        value={this.state.security.confirmPassword}
                                        onChange={ e => {
                                            const { value } = e.target;
                                            this.setState({
                                                security: {
                                                    ...this.state.security,
                                                    confirmPassword: value
                                                }
                                            })
                                        }}
                                        onBlur={e => {
                                            const { value } = e.target;
                                            this.setState({
                                                validationForm: {
                                                    security: {
                                                        ...this.state.validationForm.security,
                                                        lastName: (value.length > 2)
                                                    }
                                                }
                                            })
                                        }}
                                    />
                                </div>
                            </div>
                            <div className='text-center' >
                                <button className='btn btn-primary' type='submit' >Actualizar</button>
                            </div>
                        </div>
                    </div> */}
                </div>
                <ModalComponent
                    show={this.state.msgModal} 
                    handleClose={() => this.setState({ msgModal: false })}
                    title={this.state.msgTitle}
                >
                    <p>{this.state.msgText}</p>
                </ModalComponent>
                {/* <ModalComponent
                    show={this.state.actionModalAddress} 
                    handleClose={() => this.setState({ actionModalAddress: false })}
                    handleAccept={this.handleAcceptAddress}
                    title={this.state.actionTitle}
                    textClose='No'
                    textAccept='Si'
                >
                    <p>{this.state.actionText}</p>
                </ModalComponent>
                <ModalComponent
                    show={this.state.actionModalAmipass} 
                    handleClose={() => this.setState({ actionModalAmipass: false })}
                    handleAccept={this.handleAcceptAccountAmipass}
                    title={this.state.actionTitle}
                    textClose='No'
                    textAccept='Si'
                >
                    <p>{this.state.actionText}</p>
                </ModalComponent>
                <ModalComponent
                    show={this.state.actionModalTransbank} 
                    handleClose={() => this.setState({ actionModalTransbank: false })}
                    handleAccept={this.handleAcceptAccountTransbank}
                    title={this.state.actionTitle}
                    textClose='No'
                    textAccept='Si'
                >
                    <p>{this.state.actionText}</p>
                </ModalComponent>
                <ModalComponent
                    show={this.state.showModalAddress} 
                    handleClose={this.handleModalAddress}
                    handleAccept={this.addAddress}
                    textClose='Salir'
                    textAccept='Agregar'
                    title={this.state.msgTitle}
                >
                    <SearchLocationInput />
                </ModalComponent> */}
            </PageComponent>
        )
    }
}
