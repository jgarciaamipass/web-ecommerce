import React, { Component } from 'react'
import { Container } from 'react-bootstrap'
import CoverComponent from '../../../component/CoverComponent'
import PageComponent from '../../../component/PageComponent'
import PreloadComponent from '../../../component/PreloadComponent'
import ShoppingCard from '../../../component/ShoppingCard'
import { SERVER_API } from '../../../utils/utils'

export default class ShoppingHistoryPage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            user: {},
            shoppings: []
        }
    }

    componentDidMount() {
        let user = JSON.parse(localStorage.getItem('user'))
        this.setState({ user })
        let statusResponse = 0;
        fetch(`${SERVER_API}/shopping/my_orders/${user.codeUser}`, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('jwt')}`,
                'RememberToken': `${localStorage.getItem('tkey')}`
            },
        }).then(async response => {
            statusResponse = response.status
            return await response.json();
        }).then(response => {
            switch (statusResponse) {
                case 200:
                    let shoppings = response.map(item => {
                        let { orderHeader, orderDetails, orderPayments, consumer } = item;
                        return {
                            ...consumer,
                            ...orderHeader,
                            typePayment: orderPayments[0].paymentTypeID,
                            details: orderDetails
                        }
                    }).sort((a, b) => {
                        if (a["id"] < b["id"]) {    
                            return 1;    
                        } else if (a["id"] > b["id"]) {    
                            return -1;    
                        }
                        return 0
                    })
                    this.setState({ shoppings })
                    break;
                default:
                    this.setState({
                        msgModal: true,
                        msgTitle: "Error inesperado",
                        msgText: `Hubo un error inesperado consultando el historico de compras. COD (${statusResponse})`
                    })
                    break;
            }
        })
    }

    GetSortOrder(prop) {    
        return function(a, b) {    
            if (a[prop] > b[prop]) {    
                return 1;    
            } else if (a[prop] < b[prop]) {    
                return -1;    
            }    
            return 0;    
        }    
    }  

    cancelOrder = (e, item) => {
        if(window.confirm(`Desea cancelar la orden #${item.id}`)) {
            let statusResponse = 0;
            e.target.disabled = true
            e.target.innerText = 'Cancelando orden...'
            fetch(`${SERVER_API}/shopping/order/status/${item.id}`, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('jwt')}`,
                    'RememberToken': `${localStorage.getItem('tkey')}`
                },
                body: JSON.stringify({
                    status: -2
                })
            }).then(async response => {
                statusResponse = response.status
                return await response.json();
            }).then(response => {
                switch (statusResponse) {
                    case 200:
                        let shoppings = this.state.shoppings.map(shop => {
                            if(shop.id === item.id) {
                                shop.statusID = -2
                            }
                            return shop
                        })
                        this.setState({ shoppings })
                        break;
                    default:
                        this.setState({
                            msgModal: true,
                            msgTitle: "Error inesperado",
                            msgText: `Hubo un error inesperado consultando el historico de compras. COD (${statusResponse})`
                        })
                        e.target.disabled = false
                        e.target.innerText = 'Cancelar orden'
                        break;
                }
            })
        }
    }

    render() {
        return (
            <PageComponent>
                <CoverComponent>
                    <h3>Mis compras</h3>
                </CoverComponent>
                <Container className='mt-3' >
                    {
                        this.state.shoppings.length === 0 ? 
                            <PreloadComponent/>
                        :
                        <>
                            <div className="d-flex align-items-center h-100" >
                                <div className='d-flex flex-column align-items-center w-100' >
                                    <div className='col-md-10' >
                                        {
                                            this.state.shoppings.map(item => (
                                                <div className='my-3' >
                                                    <ShoppingCard 
                                                        key={item.id} 
                                                        data={item} 
                                                        handleCancel={(e) => this.cancelOrder(e, item)}
                                                        handleShow={() => {
                                                            this.props.history.push(`/profile/history/show/${item.id}`)
                                                        }}
                                                    />
                                                </div>
                                            ))
                                        }
                                    </div>
                                </div>
                            </div>
                        </>
                    }
                </Container>
            </PageComponent>
        )
    }
}
