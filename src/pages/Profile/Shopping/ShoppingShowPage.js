import React, { Component } from 'react'
import CurrencyFormat from 'react-currency-format'
import CoverComponent from '../../../component/CoverComponent'
import PageComponent from '../../../component/PageComponent'
import { SERVER_API } from '../../../utils/utils'

export default class ShoppingShowPage extends Component {
    constructor(props) {
        super(props)
        this.state = {
        }
    }

    componentDidMount = () => {
        let { number } = this.props.match.params;
        let statusResponse = 0
        fetch(`${SERVER_API}/shopping/order/show/${number}`, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('jwt')}`,
                'RememberToken': `${localStorage.getItem('tkey')}`
            },
        }).then(async response => {
            statusResponse = response.status
            return await response.json()
        }).then(response => {
            switch (statusResponse) {
                case 200:
                    const { orderHeader, orderDetails, orderPayments, urlInvoice } = response
                    this.setState({ 
                        ...orderHeader,
                        invoice: urlInvoice,
                        typePayment: orderPayments[0].paymentTypeID,
                        details: orderDetails })
                    console.log(this.state)
                    break;
                default:
                    this.setState({
                        msgModal: true,
                        msgTitle: "Error inesperado",
                        msgText: `Hubo un error inesperado . COD (${statusResponse})`
                    })
                    break;
            }
        }).catch(error => {
            console.log(error)
            this.setState({
                msgModal: true,
                msgTitle: "Error",
                msgText: `Hubo un error. COD (${statusResponse})`
            })
        })
    }


    render() {
        return (
            <PageComponent>
                <CoverComponent>
                    <h3>Resumen de compra</h3>
                </CoverComponent>
                <div className='container' >
                    <div className='card d-flex p-2 bd-highlight mb-5' >
                        <div className='card card-body'>
                            <div className='d-flex bd-highlight py-2'>
                                <div className='p-2 flex-grow-1 bd-highlight' >
                                    <span>Compra <strong>#{this.state.id}</strong></span>
                                </div>
                                <div className='p-2 bd-highlight' >
                                    {
                                        this.state.statusID === -1 && <strong className='text-danger'>Cancelado</strong>
                                    }
                                    {
                                        this.state.statusID === -2 && <strong className='text-danger'>Cancelado</strong>
                                    }
                                    {
                                        this.state.statusID === 1 && <span>Por confirmar</span>
                                    }
                                    {
                                        this.state.statusID === 2 && <span>Confirmado</span>
                                    }
                                    {
                                        this.state.statusID === 3 && <span>En camino</span>
                                    }
                                    {
                                        this.state.statusID === 4 && <span>Entregado</span>
                                    }
                                </div>
                            </div>
                            <div className='row py-2' >
                                <div className='col-md-4'>
                                    <strong>Comercio</strong> <br />
                                    <span>{this.state.commerce}</span>
                                </div>
                                <div className='col-md-4'>
                                    <strong>Fecha de compra</strong> <br />
                                    <span>{this.state.creationDate}</span>
                                </div>
                                {
                                    this.state.statusID === -1 && 
                                    <div className='col-md-4' >
                                        <strong>Motivo de cancelación</strong> <br />
                                        <span>Cancelado por el comercio</span>
                                    </div>
                                }
                            </div>
                            <div className='row py-2' >
                                <div className='col-md-4'>
                                    <strong>Tipo de entrega</strong> <br />
                                    {
                                        this.state.orderTypeID === 1 ? <span>Retiro en local</span> :
                                        <span>Delivery</span>
                                    }
                                </div>
                                <div className='col-md-4'>
                                    <strong>Medio de pago</strong> <br />
                                    {
                                        this.state.typePayment === 1 ? <span>Amipass</span>
                                        : <span>Transbank</span>
                                    }
                                </div>
                            </div>
                            {
                                this.state.orderTypeID === 2 &&
                                <div className='row py-1' >
                                    <div className='col' >
                                        <strong>Dirección de entrega</strong> <br />
                                        <span>{this.state.address}</span>
                                    </div>
                                </div>
                            }
                            <hr/>
                            <div className='row py-1' >
                                <div className='col' >
                                    <h6>Detalle de la compra</h6>
                                </div>
                            </div>
                            <div className='row py-4' >
                                {
                                    (this.state.details) && this.state.details.map(itemDetail => (
                                        <div className='col-12 mb-2' key={itemDetail.id} >
                                            <div className='card'>
                                                <div className='card card-body'>
                                                    <div className='row py-2' >
                                                        <div className='col'>
                                                            <strong>Producto</strong> <br />
                                                            <span>{itemDetail.name}</span><br />
                                                            <small className='text-muted' >{itemDetail.description}</small>
                                                        </div>
                                                        <div className='col text-center'>
                                                            <strong>Cantidad</strong> <br />
                                                            <span>{itemDetail.totalItems}</span>
                                                        </div>
                                                        <div className='col text-center'>
                                                            <strong>Monto</strong> <br />
                                                            <div className='text-right' >
                                                                <CurrencyFormat 
                                                                    value={itemDetail.finalAmount}
                                                                    displayType={'text'} 
                                                                    thousandSeparator={true} 
                                                                    prefix={'$'}
                                                                    renderText={value => (
                                                                    <span>{value.replace(/,/g, '.')}</span>
                                                                    )}
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    ))
                                }
                            </div>
                            <div className='row' >
                                <div className="col">
                                    {
                                        this.state.orderTypeID === 2 &&
                                        <div className='d-flex bd-highlight'>
                                            <div className='p-2 flex-grow-1 bd-highlight text-right'>
                                                <span>Delivery</span> <br />
                                            </div>
                                            <div className='p-2 bd-highlight'>
                                                <CurrencyFormat 
                                                    value={this.state.deliveryAmount}
                                                    displayType={'text'} 
                                                    thousandSeparator={true} 
                                                    prefix={'$'}
                                                    renderText={value => (
                                                    <span>{value.replace(/,/g, '.')}</span>
                                                    )}
                                                />
                                            </div>
                                        </div>
                                    }
                                    <div className='d-flex bd-highlight'>
                                        <div className='p-2 flex-grow-1 bd-highlight text-right'>
                                            <span>Sub total</span> <br />
                                        </div>
                                        <div className='p-2 bd-highlight'>
                                            <CurrencyFormat 
                                                value={this.state.finalAmount}
                                                displayType={'text'} 
                                                thousandSeparator={true} 
                                                prefix={'$'}
                                                renderText={value => (
                                                <span>{value.replace(/,/g, '.')}</span>
                                                )}
                                            />
                                        </div>
                                    </div>
                                    <div className='d-flex bd-highlight'>
                                        <div className='p-2 flex-grow-1 bd-highlight text-right'>
                                            <strong>Total</strong> <br />
                                        </div>
                                        <div className='p-2 bd-highlight'>
                                            <CurrencyFormat 
                                                value={(this.state.orderTypeID === 2) ? this.state.deliveryAmount + this.state.finalAmount : this.state.finalAmount}
                                                displayType={'text'} 
                                                thousandSeparator={true} 
                                                prefix={'$'}
                                                renderText={value => (
                                                <span>{value.replace(/,/g, '.')}</span>
                                                )}
                                            />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className='row py-2' >
                                <div className='col text-center' >
                                    {
                                        this.state.statusID === 1 && 
                                        <span className='mx-2' >
                                            <button 
                                                className='btn btn-primary'
                                                onClick={(e) => {}}
                                            >
                                                Cancelar orden
                                            </button>
                                        </span>
                                    }
                                    <button 
                                        className='btn btn-primary'
                                        onClick={(e) => {
                                            this.props.history.push(`/profile/history`)
                                        }}
                                    >
                                        Volver
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </PageComponent>
        )
    }
}
