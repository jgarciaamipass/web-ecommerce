import React, { Component } from 'react'
import { connect } from 'react-redux';
import commerceInfoReducerAction from '../../redux/actions/commerceInfoReducerAction';
import countItemCarReducerAction from '../../redux/actions/countItemCarReducerAction';
import addressDeliveryReducerAction from '../../redux/actions/addressDeliveryReducerAction';
import itemsCarReducerAction from '../../redux/actions/itemsCarReducerAction';
import PageComponent from '../../component/PageComponent'
import { Form, Container, Button, } from 'react-bootstrap';
import CurrencyFormat from 'react-currency-format';
import SearchLocationInput from '../../component/SearchLocationInput';
import ModalComponent from '../../component/system/ModalComponent';
import { SERVER_API } from '../../utils/utils';
import CoverComponent from '../../component/CoverComponent';
import { database } from '../../utils/firebase'

import { login } from '../../api/index'
import CarMenuComponent from '../../component/CarMenuComponent';
import ProductFromListComponent from '../../component/ProductFromListComponent';

class DeliveryPage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            products: [],
            commerce: {},
            infoUser: {
                fullName: "",
                phone: ""
            },
            addressUser: [],
            delivery: false,
            address: "",
            addressPlace: "",
            addressSelected: 0,
            my: 0,
            fullName: "",
            phone: "",
            date: "",
            totalAmount: 0,
            deliveryAmount: 0,
            addressValid: false,
            showModalAddress: false,
            showModalValidation: false,
            msgModal: false,
            msgTitle: "",
            msgText: "",
            msgCloseCommerce: false,
            msgWithoutDelivery: false,
            productsWithoutStock: [],
            productsSoldOut: [],
            showModalProductsWithoutStock: false,
            modalProductStockAcceptDisable: false
        }
    }

    componentDidMount() {
        let user = JSON.parse(localStorage.getItem('user'));
        let { commerce } = this.props.match.params;
        let statusResponseCommerce = 0;
        let statusResponseInfo = 0
        let statusResponseAddress = 0;

        fetch(`${SERVER_API}/commerce/show/name/${commerce}`, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        }).then(async response => {
            statusResponseCommerce = response.status
            return await response.json();
        }).then(async response => {
            switch (statusResponseCommerce) {
                case 200:
                    let { commerce, commerceConfig } = response
                    let { ecommerceConfig, communesDeliveries } = commerceConfig;
                    let car = [];

                    if(!commerce.hasDelivery && !commerce.hasWithdrawal) {
                        this.setState({
                            msgWithoutDelivery: true,
                        })
                    }

                    fetch(`${SERVER_API}/commerce/state/${commerce.internalID}`, {
                        method: 'GET',
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json',
                        },
                    }).then(async response => {
                        return await response.json()
                    }).then(response => {
                        let { isOpen } = response
                        if(!isOpen) {
                            this.setState({
                                msgCloseCommerce: true,
                            })
                        }
                    })

                    this.setState({ 
                        commerce,
                        communesDeliveries,
                        deliveryAmount: commerceConfig.ecommerceConfig.deliveryPrice,
                        ecommerceName: ecommerceConfig.ecommerceName
                    });
                    this.props.commerceInfoReducerAction({ 
                        ...commerce, 
                        config: commerceConfig,
                        ecommerceName: ecommerceConfig.ecommerceName
                    })

                    let cart = await database
                        .collection(`${user.username}`)
                        .doc(`cart-${this.state.commerce.commerceID}${this.state.commerce.storeID}`)
                        .get()

                    if(cart.exists) {
                        if(cart.data().items.length > 0) {
                            let count = 0
                            let amount = 0
                            let productsByVerify = []
                            car = cart.data().items.map(item => {
                                productsByVerify.push({
                                    id: item.id,
                                    stock: item.quantity
                                })
                                count += item.quantity
                                amount += item.saleAmount * item.quantity
                                return item
                            })

                            this.props.countItemCarReducerAction({ 
                                count: count, 
                                amount: amount, 
                                items: car 
                            })
                            this.calculateTotalAmount(car)
                            this.setState({ products: car })
                            this.validateStock(`${this.state.commerce.commerceID}${this.state.commerce.storeID}`,productsByVerify)
                        } else {
                            await database
                            .collection(`${user.username}`)
                            .doc(`cart-${this.state.commerce.commerceID}${this.state.commerce.storeID}`)
                            .delete()
                        }
                    } else {
                        this.props.history.push(`/local/${this.state.ecommerceName}`)
                    }
                    break;
                default:
                    this.setState({
                        msgModal: true,
                        msgTitle: "Error inesperado",
                        msgText: `Hubo un error inesperado consultando la información del comercio. COD (${statusResponseCommerce})`
                    })
                    break;
            }
        })

        login.refreshToken()
        .then(() => {
            Promise.all([
                fetch(`${SERVER_API}/profile/address/find/${user.codeUser}`, {
                    method: 'GET',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${localStorage.getItem('jwt')}`,
                        'RememberToken': `${localStorage.getItem('tkey')}`
                    },
                }).then(async response => {
                    statusResponseAddress = response.status
                    return await response.json();
                }).then(response => {
                    switch (statusResponseAddress) {
                        case 200:
                            this.setState({
                                addressUser: response
                            })
                            break;
                        default:
                            this.setState({
                                msgModal: true,
                                msgTitle: "Error inesperado",
                                msgText: `Hubo un error inesperado consultando las direcciones. COD (${statusResponseAddress})`
                            })
                            break;
                    }
                }),
                fetch(`${SERVER_API}/profile/show/info_basic/${user.codeUser}`, {
                    method: 'GET',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${localStorage.getItem('jwt')}`,
                        'RememberToken': `${localStorage.getItem('tkey')}`
                    },
                }).then(async response => {
                    statusResponseInfo = response.status
                    return await response.json();
                }).then(response => {
                    switch (statusResponseInfo) {
                        case 200:
                            let { firstName, lastName, phone } = response
                            this.setState({
                                infoUser: {
                                    fullName: `${firstName} ${lastName}`,
                                    phone
                                }
                            });
                            break;
                        default:
                            this.setState({
                                msgModal: true,
                                msgTitle: "Error inesperado",
                                msgText: `Hubo un error inesperado consultando los datos personales. COD (${statusResponseInfo})`
                            })
                            break;
                    }
                })
            ]).catch(error => {
                console.log(error)
                this.setState({
                    msgModal: true,
                    msgTitle: "Error",
                    msgText: `Hubo un error cargando las direcciones, datos del usuario o del comercio. COD (${statusResponseCommerce}:${statusResponseAddress}:${statusResponseInfo})`
                })
            })
        }).catch(error => {
            this.setState({
                msgModal: true,
                msgTitle: "Error inesperado",
                msgText: `Debe iniciar sesión para continuar con la compra`
            })
        })
    }

    validateStock = async (commerce, productsByVerify) => {
        await fetch(`${SERVER_API}/commerce/product/validate/${commerce}`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(productsByVerify)
        }).then(async response => {
            return await response.json();
        }).then(async response => {
            let { productStocks } = response
            if(productStocks.length > 0) {
                let productsWithoutStock = []
                let productsSoldOut = []
                this.state.products.map(item => {
                    let prod = productStocks.filter(a => item.id === a.id)
                    if(prod.length > 0) {
                        if(prod[0].stock === 0) {
                            item.stk = prod[0].stock
                            productsSoldOut.push(item)
                        } else {
                            productsWithoutStock.push(item)
                        }
                    }
                    return prod
                })

                if(productsSoldOut.length > 0) {
                    let newDataCart = []
                    let newCart = [];
                    this.state.products.map(item => {
                        let productInStock = productsSoldOut.filter(a => a.id === item.id)
                        if(productInStock.length === 0) {
                            return newDataCart.push(item)
                        }
                        return false
                    })

                    let count = 0
                    let amount = 0
                    newDataCart.map(item => {
                        count += item.quantity
                        amount += item.saleAmount * item.quantity
                        return newCart.push(item)
                    })
                    this.props.countItemCarReducerAction({ 
                        count: count, 
                        amount: amount, 
                        items: newCart 
                    })
                    this.setState({ products: newCart })
                }
                
                this.setState({ 
                    productsWithoutStock,
                    productsSoldOut,
                    showModalProductsWithoutStock: true, 
                    modalProductStockAcceptDisable: (productsWithoutStock.length > 0 && productsSoldOut.length > 0) || !(productsWithoutStock.length === 0 && productsSoldOut.length > 0)
                })
            }
        })
    }

    calculateTotalAmount = (products) => {
        let totalAmount = 0;
        products.map(item => {
            return totalAmount += item.saleAmount * item.quantity
        })
        this.setState({totalAmount})
    }

    next = (e) => {
        window.dataLayer.push({event: 'paymentMethodPage'})
        e.preventDefault();
        localStorage.setItem('delivery', JSON.stringify({
            delivery: this.state.delivery,
            address: this.state.address,
            deliveryAmount: this.state.deliveryAmount,
            id: this.state.addressSelected,
            my: this.state.my,
            fullName: this.state.fullName,
            phone: this.state.phone,
            date: this.state.date,
        }))
        if(this.state.delivery && this.state.addressValid) {
            this.props.history.push(`/local/${this.state.ecommerceName}/pay/end`)
        }
        if(!this.state.delivery) {
            this.props.history.push(`/local/${this.state.ecommerceName}/pay/end`)
        }
        if(!this.state.delivery && !this.state.addressValid) {
            this.setState({
                msgModal: true,
                msgTitle: "¡Atención!",
                msgText: `El local no tiene despacho disponible en la dirección seleccionada.`
            })
        }
    }

    handleModalAddress = () => {
        this.setState({ showModalAddress: !this.state.showModalAddress });
    }
    
    handleModalValidation = () => {
        this.setState({ showModalValidation: !this.state.showModalValidation })
    }

    addAddress = () => {
        if(this.state.addressPlace === "") {
            this.setState({
                msgModal: true,
                msgTitle: "¡Atención!",
                msgText: `La dirección requiere que especifique un número de casa, oficina o departamento.`,
                addressSelected: 0,
                address: "",
                addressValid: false
            })
            return false
        }
        let newAddressUser = this.state.addressUser;
        let { geometry, address_components, formatted_address } = JSON.parse(localStorage.getItem('address'))
        let user = JSON.parse(localStorage.getItem('user'));
        let statusResponse = 0
        
        let streetNumber = ''
        let route = ''
        let locality = ''
        let areaLevel3 = ''
        let areaLevel2 = ''
        let areaLevel1 = ''
        let country = ''

        address_components.map(item => {
            return item.types.filter(type => {
                switch (type) {
                    case 'street_number':
                        streetNumber = item.long_name
                        break;
                    case 'route':
                        route = item.long_name
                        break;
                    case 'locality':
                        locality = item.long_name
                        break;
                    case 'administrative_area_level_3':
                        areaLevel3 = item.long_name
                        break;
                    case 'administrative_area_level_2':
                        areaLevel2 = item.long_name
                        break;
                    case 'administrative_area_level_1':
                        areaLevel1 = item.long_name
                        break;
                    case 'country':
                        country = item.long_name
                        break;
                    default:
                        return false
                }
            })
        })
        fetch(`${SERVER_API}/profile/address/store`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('jwt')}`,
                'RememberToken': `${localStorage.getItem('tkey')}`
            },
            body: JSON.stringify({
                userId: user.codeUser,
                address: formatted_address,
                latitude: geometry.location.lat,
                longitude: geometry.location.lng,
                streetNumber,
                route,
                locality,
                areaLevel3,
                areaLevel2,
                areaLevel1,
                country,
                place: this.state.addressPlace
            })
        }).then(async response => {
            statusResponse = response.status
            return await response.json();
        }).then(response => {
            switch (statusResponse) {
                case 200:
                    newAddressUser.push(response)
                    this.setState({ addressUser: newAddressUser, showModalAddress: false })
                    break;
                default:
                    this.setState({
                        msgModal: true,
                        msgTitle: "Error inesperado",
                        msgText: `Hubo un error inesperado consultando las direcciones para delivery. COD (${statusResponse})`,
                        showModalAddress: false
                    })
                    break;
            }
        })
    }

    onChangeAddress = (e) => {
        var index = e.target.selectedIndex;
        var optionElement = e.target.childNodes[index]
        var commune =  optionElement.getAttribute('data-commune');
        console.log(commune)
        let addressValid = this.state.communesDeliveries
        .filter(item => `${item.communeName}`.toUpperCase() === `${commune}`.toUpperCase())
        if(addressValid.length === 0) {
            this.setState({
                msgModal: true,
                msgTitle: "¡Atención!",
                msgText: `El local no tiene despacho disponible en la dirección seleccionada.`,
                addressSelected: 0,
                address: "",
                addressValid: false
            })
            return false
        }
        this.setState({ 
            addressValid: true,
            addressSelected: e.target.value,
            deliveryAmount: addressValid[0].communeDeliveryPrice
        })
        this.calculateTotalAmount(this.state.products)
    }

    addProduct = async (item) => {
        let user = JSON.parse(localStorage.getItem('user'));
        let newData = [];
        let newProductsWithoutStock = []
        let countItem = this.props.shoppingCarMenuReducer.count
        let oldAmount = this.props.shoppingCarMenuReducer.amount
        let newAmount = 0
        this.state.products.map(product => {
            if(product.id === item.id) {
                if(item.quantity === item.stk) return newData.push(product)
                let quantityOld = product.quantity
                product.quantity++
                countItem = countItem + 1
                newAmount = (product.quantity === 1) ? (product.saleAmount * product.quantity) : ((product.saleAmount * product.quantity) - (product.saleAmount * quantityOld))
                return newData.push(product)
            }
            return newData.push(product)
        })

        let itemsCar = newData.filter(item => item.quantity > 0)

        this.props.countItemCarReducerAction({ 
            count: countItem,
            amount: (oldAmount + newAmount), 
            items: itemsCar 
        })

        this.props.itemsCarReducerAction(itemsCar)

        const cart = await database
        .collection(`${user.username}`)
        .doc(`cart-${this.state.commerce.commerceID}${this.state.commerce.storeID}`)

        cart.set({
            commerce: this.state.commerce.commerceID,
            store: this.state.commerce.storeID,
            commerceAlias: this.state.ecommerceName,
            items: itemsCar
        })

        this.state.productsWithoutStock.map(item => {
            let data = newData.filter(a => a.id === item.id)[0]
            if(typeof data !== 'undefined') {
                return newProductsWithoutStock.push(data)
            }
            return newProductsWithoutStock.push(item)
        })
        this.setState({
            productsWithoutStock: newProductsWithoutStock,
            modalProductStockAcceptDisable: !(typeof itemsCar.filter(a => a.quantity > a.stk)[0] === 'undefined')
        })
    }

    removeProduct = async (item) => {
        let user = JSON.parse(localStorage.getItem('user'));
        let newData = [];
        let newProductsWithoutStock = []
        let countItem = this.props.shoppingCarMenuReducer.count
        let oldAmount = this.props.shoppingCarMenuReducer.amount
        let newAmount = 0
        this.state.products.map(product => {
            if(product.id === item.id) {
                if(item.quantity === 0) {
                    newAmount -= product.saleAmount
                    return newData.push(product)
                }
                let quantityOld = product.quantity
                product.quantity--
                countItem = countItem - 1
                newAmount = (product.saleAmount * quantityOld) - (product.saleAmount * product.quantity)
                return newData.push(product)
            }
            return newData.push(product)
        })

        let itemsCar = newData.filter(item => item.quantity > 0)

        this.props.countItemCarReducerAction({ 
            count: countItem,
            amount: (oldAmount - newAmount), 
            items: itemsCar 
        })

        this.props.itemsCarReducerAction(itemsCar)

        const cart = await database
        .collection(`${user.username}`)
        .doc(`cart-${this.state.commerce.commerceID}${this.state.commerce.storeID}`)

        cart.set({
            commerce: this.state.commerce.commerceID,
            store: this.state.commerce.storeID,
            commerceAlias: this.state.ecommerceName,
            items: itemsCar
        })

        this.state.productsWithoutStock.map(item => {
            let data = newData.filter(a => a.id === item.id)[0]
            if(typeof data !== 'undefined') {
                return newProductsWithoutStock.push(data)
            }
            return newProductsWithoutStock.push(item)
        })
        this.setState({
            productsWithoutStock: newProductsWithoutStock,
            modalProductStockAcceptDisable: !(typeof itemsCar.filter(a => a.quantity > a.stk)[0] === 'undefined')
        })
    }

    render() {
        return (
            <PageComponent>
                <CoverComponent>
                    <h3>Finalizando compra</h3>
                    <span>Puedes seleccionar entre retiro y delivery</span>
                </CoverComponent>
                <Container className='my-5 h-100' >
                    <form onSubmit={this.next} action={'#'} method={'POST'} >
                        <div className='row'>
                            <div className='col-md-8' >
                                {
                                    this.state.commerce.hasWithdrawal && 
                                    <div className='row mb-5' >
                                        <div className='col'>
                                            <div className='card'>
                                                <div className='card-body'>
                                                    <div className='float-left' ><Form.Check required type="radio" radioGroup='delivery' name='delivery' value={0} onChange={(e) => this.setState({ delivery: false }) } /></div>
                                                    <div className='text-center' >
                                                        <h6>Retiro en tienda <br/>
                                                        <small className='text-muted' >Sin costo adicional</small></h6>
                                                        <p>{this.props.commerceReducer.localAddress}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                }
                                {
                                    this.state.commerce.hasDelivery && 
                                    <div className='row mb-5' >
                                        <div className='col'>
                                            <div className='card'>
                                                <div className='card-body'>
                                                    <div className='float-left' ><Form.Check required type="radio" radioGroup='delivery' name='delivery' value={1} onChange={(e) => this.setState({ delivery: true }) } /></div>
                                                    <div className='text-center' >
                                                        <h6>Despacho a domicilio <br/> 
                                                        <small className='text-muted'>Ingresa tu dirección y recibe tu pedido donde quieras</small></h6>
                                                    </div>
                                                    <select 
                                                        className='form-control' 
                                                        disabled={!this.state.delivery} 
                                                        value={this.state.addressSelected} 
                                                        required={this.state.delivery} 
                                                        onChange={this.onChangeAddress} >
                                                        <option value='' >Direcciones</option>
                                                        {
                                                            this.state.addressUser.map(item => (
                                                                <option key={item.id} value={item.id} data-commune={item.areaLevel3} >{item.address} ({item.place})</option>
                                                            ))
                                                        }
                                                    </select>
                                                    <div className='mt-2' >
                                                        <Button disabled={!this.state.delivery} size='sm' 
                                                        onClick={() => {
                                                            window.dataLayer.push({event: 'buttonOtherAddressDeliveryType'})
                                                            this.setState({ showModalAddress: true })
                                                        }}
                                                        >Agregar otra dirección</Button>
                                                    </div>
                                                    {
                                                        this.state.delivery &&
                                                        <div className="accordion" id='accordionDeliveryZone' >
                                                            {
                                                                this.state.communesDeliveries.map((item,i) => (
                                                                    <div className='card' key={item.id} >
                                                                        <div className="card-header" id={`heading${item.id}`}>
                                                                            <div className="btn btn-block" data-toggle="collapse" data-target={`#collapse${item.id}`} aria-expanded={(i===0)?'true':'false'} aria-controls={`collapse-${item.id}`}>
                                                                                <div className="d-flex bd-highlight">
                                                                                    <div className="p-2 flex-grow-1 bd-highlight text-left">
                                                                                        <div>
                                                                                            <h5>{(item.regionName) ? item.regionName : 'Sin especificar'}</h5>
                                                                                        </div>
                                                                                        <div>
                                                                                            <span>{(item.communeName) ? item.communeName : 'Sin especificar'}</span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div className="p-2 bd-highlight text-center">
                                                                                        <div>
                                                                                            <h5>Delivery</h5>
                                                                                        </div>
                                                                                        {
                                                                                            item.communeDeliveryPrice > 0 ?
                                                                                            <CurrencyFormat 
                                                                                                value={item.communeDeliveryPrice}
                                                                                                displayType={'text'} 
                                                                                                thousandSeparator={true} 
                                                                                                prefix={'$'}
                                                                                                renderText={value => (
                                                                                                    <span>{value.replace(/,/g, '.')}</span>
                                                                                                )}
                                                                                            /> :
                                                                                            <span>Gratis</span>
                                                                                        }
                                                                                    </div>
                                                                                </div>
                                                                                <div className='text-center'>
                                                                                    <small>Ver más</small>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div id={`collapse${item.id}`} className={`collapse ${(i===0)?'show':''}`} aria-labelledby={`heading${item.id}`} data-parent="#accordionDeliveryZone">
                                                                            <div className="card-body">
                                                                                <div className="row text-center">
                                                                                    <div className='col-xl-2'>
                                                                                        <h5>Región</h5>
                                                                                        <span>{(item.regionName) ? item.regionName : 'Sin especificar'}</span>
                                                                                    </div>
                                                                                    <div className='col-xl-2'>
                                                                                        <h5>Comuna</h5>
                                                                                        <span>{(item.communeName) ? item.communeName : 'Sin especificar'}</span>
                                                                                    </div>
                                                                                    <div className='col-xl-2'>
                                                                                        <h5>Tiempo de entrega</h5>
                                                                                        <span>{(item.communeDeliveryTime) ? item.communeDeliveryTime : 'Sin especificar'}</span>
                                                                                    </div>
                                                                                    <div className='col-xl-4'>
                                                                                        <h5>Referencia</h5>
                                                                                        <span>{(item.communeReference) ? item.communeReference : 'Sin especificar'}</span>
                                                                                    </div>
                                                                                    <div className='col-xl-2'>
                                                                                        <h5>Costo Delivery</h5>
                                                                                        <div>
                                                                                            {
                                                                                                item.communeDeliveryPrice > 0 ?
                                                                                                <CurrencyFormat 
                                                                                                    value={item.communeDeliveryPrice}
                                                                                                    displayType={'text'} 
                                                                                                    thousandSeparator={true} 
                                                                                                    prefix={'$'}
                                                                                                    renderText={value => (
                                                                                                        <span>{value.replace(/,/g, '.')}</span>
                                                                                                    )}
                                                                                                /> :
                                                                                                <span>Gratis</span>
                                                                                            }
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                ))
                                                            }
                                                        </div>
                                                    }
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                }
                                <div className='row mb-5' >
                                    <div className='col'>
                                        <div className='card'>
                                            <div className='card-body'>
                                                <div className='text-center' >
                                                    <h6>¿Quién recibe o retira tu compra?</h6>
                                                </div>
                                                <Form.Group >                                       
                                                    <div className='d-flex flex-row' >
                                                        <Form.Check required className='pr-3' type="radio" name='my' value={1} label='Yo' onChange={(e) => {
                                                            this.setState({ 
                                                                my: true,
                                                                fullName: this.state.infoUser.fullName,
                                                                phone: this.state.infoUser.phone
                                                            })
                                                        }} />
                                                        <Form.Check required className='pr-3' type="radio" name='my' value={0} label='Otro' onChange={(e) => {
                                                            this.setState({ 
                                                                my: false,
                                                                fullName: "",
                                                                phone: ""
                                                            })
                                                        }}/>
                                                    </div>
                                                </Form.Group>
                                                <div className='row'>
                                                    <div className='col-md-4'>
                                                        <Form.Label>Nombre completo</Form.Label>
                                                        <Form.Control  
                                                            type="text" 
                                                            value={this.state.fullName} 
                                                            placeholder="ej. Pedro Pérez" 
                                                            onChange={(e) => {
                                                                let value = e.target.value
                                                                if(value.match(/^[a-zA-Z\s]*$/)) {
                                                                    this.setState({ fullName: e.target.value })
                                                                    return true
                                                                } else {
                                                                    return false
                                                                }
                                                            }} />
                                                    </div>
                                                    <div className='col-md-4'>
                                                        <Form.Label>Teléfono</Form.Label>
                                                        <CurrencyFormat format="+56 ###-###-###" mask="_" 
                                                            type='tel'
                                                            placeholder='912-344-321'
                                                            required={true}
                                                            value={this.state.phone}
                                                            minLength={9}
                                                            className='form-control' onValueChange={(values) => {
                                                            const {value} = values;
                                                            this.setState({ phone: value })
                                                        }} />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className='row py-5' >
                                    <div className='col text-center' >
                                        <div>
                                            <button className='btn btn-primary' type='submit' >Siguiente</button>
                                        </div>
                                        <div>
                                            <button className='btn btn-link' type='button' onClick={() => {
                                                window.dataLayer.push({event: 'continueShopping'})
                                                window.history.pushState({},'',`/local/${this.state.ecommerceName}`);
                                            window.history.go()
                                            }} >Seguir comprando</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className='col'>
                                <div className='card'>
                                    <div className='card-body'>
                                        <div className='text-center border-bottom' >
                                            <h6>Resumen de compra</h6>
                                        </div>
                                        <div className='py-4' >
                                            <CarMenuComponent data={this.props.shoppingCarMenuReducer.items} comment={false} />
                                        </div>
                                        <div className='py-2' >
                                            <small className='text-muted'>Sub total: <CurrencyFormat 
                                                value={this.props.shoppingCarMenuReducer.amount}
                                                displayType={'text'} 
                                                thousandSeparator={true} 
                                                prefix={'$'}
                                                renderText={value => (
                                                <span>{value.replace(/,/g, '.')}</span>
                                                )}
                                            /></small><br/>
                                            {
                                                this.state.delivery && 
                                                <>
                                                    <small className='text-muted'>Delivery: {
                                                        this.state.deliveryAmount > 0 ?
                                                        <CurrencyFormat 
                                                            value={this.state.deliveryAmount}
                                                            displayType={'text'} 
                                                            thousandSeparator={true} 
                                                            prefix={'$'}
                                                            renderText= { value => (
                                                            <span> {value.replace(/,/g, '.')}</span>
                                                            )}
                                                        /> :
                                                        <span className='text-success' >Gratis</span>
                                                    }
                                                    </small><br/>
                                                </>
                                            }
                                            {
                                                this.state.delivery ? 
                                                <>
                                                    <strong>
                                                        <span>Total a pagar: </span><CurrencyFormat 
                                                            value={this.props.shoppingCarMenuReducer.amount + this.state.deliveryAmount}
                                                            displayType={'text'} 
                                                            thousandSeparator={true} 
                                                            prefix={'$'}
                                                            renderText={value => (
                                                            <span>{value.replace(/,/g, '.')}</span>
                                                            )}
                                                        />
                                                    </strong>
                                                </> :
                                                <strong>
                                                    <span>Total a pagar: </span><CurrencyFormat 
                                                        value={this.props.shoppingCarMenuReducer.amount}
                                                        displayType={'text'} 
                                                        thousandSeparator={true} 
                                                        prefix={'$'}
                                                        renderText={value => (
                                                        <span>{value.replace(/,/g, '.')}</span>
                                                        )}
                                                    />
                                                </strong>
                                            }
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </Container>
                <ModalComponent
                    show={this.state.showModalAddress} 
                    handleClose={this.handleModalAddress}
                    handleAccept={this.addAddress}
                    title={'Dirección de entrega'}
                    textClose={'Cerrar'}
                    textAccept={'Agregar'}
                >
                    <div className='row' >
                        <div className='col'>
                            <div className='form-group' >
                                <SearchLocationInput />
                            </div>
                            <div className='form-group' >
                                <input 
                                    type='text' 
                                    className='form-control' 
                                    maxLength='120' 
                                    placeholder='Depto 204'
                                    onChange={(e) => { this.setState({ addressPlace: e.target.value }) }}
                                />
                            </div>
                        </div>
                    </div>
                </ModalComponent>
                <ModalComponent
                    show={this.state.showModalValidation} 
                    handleClose={this.handleModalValidation}
                    title={this.state.msgTitle}
                    textClose='Salir'
                >
                    <div className='row' >
                            <div className='col'>
                                <p>
                                    {this.state.messageValidation}
                                </p>
                            </div>
                        </div>
                </ModalComponent>
                <ModalComponent
                    show={this.state.msgModal} 
                    handleClose={() => this.setState({ msgModal: false })}
                    title={this.state.msgTitle}
                >
                    <p>{this.state.msgText}</p>
                </ModalComponent>
                <ModalComponent
                    show={this.state.msgCloseCommerce} 
                    handleAccept={() => {
                        this.props.history.push(`/home`)
                    }}
                    title={'Comercio cerrado'}
                >
                    <p>Este comercio se encuentra cerrado</p>
                </ModalComponent>
                <ModalComponent
                    show={this.state.msgWithoutDelivery} 
                    handleAccept={() => {
                        this.props.history.push(`/home`)
                    }}
                    title={'Medio de entrega'}
                >
                    <p>Este comercio no tiene un medio de entrega delivery o retiro en el local</p>
                </ModalComponent>
                <ModalComponent
                    show={this.state.showModalProductsWithoutStock} 
                    handleAccept={() => {
                        this.setState({
                            showModalProductsWithoutStock: false
                        })
                    }}
                    acceptDisabled={this.state.modalProductStockAcceptDisable}
                    title={'Cantidad no disponible'}
                >
                    {
                        this.state.productsWithoutStock.map(item => 
                            <>
                            <p>Los siguientes productos superan la cantidad disponible para efectuar tu compra. Cambie la cantidad para continuar.</p>
                            <div>
                                <ProductFromListComponent 
                                    controls={true}
                                    key={item.id} 
                                    item={item} 
                                    addProduct={() => this.addProduct(item)}
                                    removeProduct={() => this.removeProduct(item)}
                                />
                            </div>
                            </>
                        )
                    }
                    {
                        this.state.productsSoldOut.length > 0 &&
                        <>
                            <p>Estos productos se encuentran agotados</p>
                            <div>
                                {
                                    this.state.productsSoldOut.map(item => {
                                        return <ProductFromListComponent 
                                            controls={true}
                                            key={item.id} 
                                            item={item} 
                                            addProduct={() => this.addProduct(item)}
                                            removeProduct={() => this.removeProduct(item)}
                                        />
                                    })
                                }
                            </div>
                        </>
                    }
                </ModalComponent>
            </PageComponent>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        shoppingCarMenuReducer: state.shoppingCarMenuReducer,
        commerceReducer: state.commerceReducer
    }
}

const mapDispatchToProps = {
    commerceInfoReducerAction,
    countItemCarReducerAction,
    itemsCarReducerAction,
    addressDeliveryReducerAction,
}

export default connect(mapStateToProps, mapDispatchToProps)(DeliveryPage)