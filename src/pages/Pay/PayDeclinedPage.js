import React, { Component } from 'react'
import PageComponent from '../../component/PageComponent'
import { Jumbotron, Card, Button } from 'react-bootstrap'

export default class PayDeclinedPage extends Component {
    render() {
        return (
            <PageComponent>
                <Jumbotron>
                    <div className='align-self-center' >
                        <h4>Su pago fue rechazado</h4>
                    </div>
                </Jumbotron>
                <div className="d-flex align-items-center h-25" >
                    <div className='d-flex flex-column align-items-center w-100' >
                        <Card>
                            <Card.Body className='text-center' >
                                <p>
                                    Consulte con su empleador o entidad bancaria condiciones de consumo
                                </p>
                                <Button size='sm' >Volver a intentar</Button>
                            </Card.Body>
                        </Card>
                    </div>
                </div>
            </PageComponent>
        )
    }
}
