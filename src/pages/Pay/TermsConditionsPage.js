import React, { Component } from 'react'
import CoverComponent from '../../component/CoverComponent'
import PageComponent from '../../component/PageComponent'
import TermsConditions from '../../component/TermsConditions'

export default class TermsConditionsPage extends Component {
    render() {
        return (
            <PageComponent>
                <CoverComponent>
                    <h3>Términos y condiciones</h3>
                </CoverComponent>
                <div className='container' >
                    <TermsConditions />
                </div>
            </PageComponent>
        )
    }
}
