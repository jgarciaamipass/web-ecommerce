import React, { Component } from 'react'
import { connect } from 'react-redux'
import PageComponent from '../../component/PageComponent'
import { Jumbotron, Card, Container } from 'react-bootstrap'
import PreloadComponent from '../../component/PreloadComponent'
import ModalComponent from '../../component/system/ModalComponent'
import countItemCarReducerAction from '../../redux/actions/countItemCarReducerAction';
import itemsCarReducerAction from '../../redux/actions/itemsCarReducerAction';
import CurrencyFormat from 'react-currency-format'
import { SERVER_API } from '../../utils/utils'

class PaySuccessfullyPage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            msgShow: false,
            msgText: "",
            msgTitle: "",
            shopping: {},
            shoppingDetails: [],
        }
    }

    componentDidMount() {
        localStorage.removeItem('car')
        localStorage.removeItem('shopping')
        localStorage.removeItem('delivery')
        localStorage.removeItem('accountsAmipass')
        this.props.itemsCarReducerAction({})
        this.props.countItemCarReducerAction({
            count: 0,
            amount: 0,
            items: []
        })
        let { shoppingCode, commerce } = this.props.match.params;
        let statusResponse = 0
        fetch(`${SERVER_API}/shopping/order/show/${shoppingCode}`, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('jwt')}`,
                'RememberToken': `${localStorage.getItem('tkey')}`
            },
        }).then(async response => {
            statusResponse = response.status
            return await response.json()
        }).then(response => {
            switch (statusResponse) {
                case 200:
                    const { orderHeader, orderDetails, orderPayments, urlInvoice } = response
                    this.setState({ shopping: {
                        ...orderHeader,
                        invoice: urlInvoice,
                        typePayment: orderPayments[0].paymentTypeID
                    }, shoppingDetails: orderDetails })
                    setTimeout(() => {
                        window.history.pushState(
                            {},
                            '',
                            `/local/${commerce}`
                        );
                        window.history.go()
                    }, 10000)
                    break;
                default:
                    this.setState({
                        msgModal: true,
                        msgTitle: "Error inesperado",
                        msgText: `Hubo un error inesperado . COD (${statusResponse})`
                    })
                    break;
            }
        }).catch(error => {
            console.log(error)
            this.setState({
                msgModal: true,
                msgTitle: "Error",
                msgText: `Hubo un error. COD (${statusResponse})`
            })
        })
    }

    render() {
        let invoice = this.state.shopping.invoice
        return (
            <PageComponent>
                <Jumbotron>
                    <div className='align-self-center' >
                        <h4>Su compra fue realizada</h4>
                    </div>
                </Jumbotron>
                <Container className='d-flex justify-content-center' >
                    {
                        (typeof this.state.shopping.id == 'undefined') ?
                        <PreloadComponent/> :
                        <Card className='col-md-8 mb-5' >
                            <Card.Body>
                                <div className='row py-2'>
                                    <div className='col' >
                                        <span>Compra <strong>{this.state.shopping.id}</strong></span>
                                    </div>
                                </div>
                                <div className='row py-2' >
                                    <div className='col-md-4'>
                                        <strong>Comercio</strong> <br />
                                        <span>{this.state.shopping.commerce}</span>
                                    </div>
                                    <div className='col-md-4'>
                                        <strong>Total</strong> <br />
                                        <CurrencyFormat 
                                            value={this.state.shopping.finalAmount}
                                            displayType={'text'} 
                                            thousandSeparator={true} 
                                            prefix={'$'}
                                            renderText={value => (
                                            <span>{value.replace(/,/g, '.')}</span>
                                            )}
                                        />
                                    </div>
                                    <div className='col-md-4'>
                                        <strong>Medio de pago</strong> <br />
                                        {
                                            this.state.shopping.typePayment === 1 ? <span>Amipass</span>
                                            : <span>Transbank</span>
                                        }
                                    </div>
                                </div>
                                <div className='row py-2' >
                                    <div className='col-md-4'>
                                        <strong>Estado</strong> <br />
                                        {/* <span>{this.state.shopping.statusID}</span> */}
                                        <span>Creado</span>
                                    </div>
                                    <div className='col-md-4'>
                                        <strong>Tipo de entrega</strong> <br />
                                        {
                                            this.state.shopping.orderTypeID === 2 ? <span>Delivery</span> :
                                            <span>Retiro en local</span>
                                        }
                                    </div>
                                    <div className='col-md-4'>
                                        <strong>Fecha de compra</strong> <br />
                                        <span>{this.state.shopping.creationDate}</span>
                                    </div>
                                </div>
                                <div className='row py-1' >
                                    <div className='col' >
                                        <h6>Detalle de la compra</h6>
                                    </div>
                                </div>
                                <div className='row py-4' >
                                    {
                                        this.state.shoppingDetails.map(item => (
                                            <div className='col-12 mb-2' key={item.id} >
                                                <Card>
                                                    <Card.Body>
                                                        <div className='row py-2' >
                                                            <div className='col'>
                                                                <strong>Producto</strong> <br />
                                                                <span>{item.name}</span>
                                                            </div>
                                                            <div className='col'>
                                                                <strong>Monto</strong> <br />
                                                                <CurrencyFormat 
                                                                    value={item.finalAmount}
                                                                    displayType={'text'} 
                                                                    thousandSeparator={true} 
                                                                    prefix={'$'}
                                                                    renderText={value => (
                                                                    <span>{value.replace(/,/g, '.')}</span>
                                                                    )}
                                                                />
                                                            </div>
                                                        </div>
                                                        <div className='row py-2' >
                                                            <div className='col'>
                                                                <strong>Cantidad</strong> <br />
                                                                <span>{item.totalItems}</span>
                                                            </div>
                                                            <div className='col'>
                                                                <strong>Descripción</strong> <br />
                                                                <span>{item.description}</span>
                                                            </div>
                                                        </div>
                                                    </Card.Body>
                                                </Card>
                                            </div>
                                        ))
                                    }
                                </div>
                                {
                                    this.state.shopping.typePayment !== 1 &&
                                    <div className='row py-2' >
                                        <div className='col text-center' >
                                            <a href={invoice} className="btn btn-primary" role="button" aria-pressed="true" >Descargar comprobante</a>
                                        </div>
                                    </div>
                                }
                            </Card.Body>
                        </Card>
                    }
                </Container>
                <ModalComponent
                    show={this.state.msgModal} 
                    handleClose={() => this.setState({ msgModal: false })}
                    title={this.state.msgTitle}
                >
                    <p>{this.state.msgText}</p>
                </ModalComponent>
            </PageComponent>
        )
    }
}


const mapStateToProps = (state) => {
    return {
        shoppingCarMenuReducer: state.shoppingCarMenuReducer,
    }
}

const mapDispatchToProps = {
    itemsCarReducerAction,
    countItemCarReducerAction
}

export default connect(mapStateToProps, mapDispatchToProps)(PaySuccessfullyPage)