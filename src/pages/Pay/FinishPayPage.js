import React, { Component } from 'react'
import queryString from 'querystring';
import { connect } from 'react-redux';
import commerceInfoReducerAction from '../../redux/actions/commerceInfoReducerAction';
import countItemCarReducerAction from '../../redux/actions/countItemCarReducerAction';
import itemsCarReducerAction from '../../redux/actions/itemsCarReducerAction';
import PageComponent from '../../component/PageComponent'
import { Form, Button, Card, Image, Modal } from 'react-bootstrap';

import logoAmipass from './../../assets/images/medio-de-pago-amipass.svg'
import logoWebpay from './../../assets/images/medio-de-pago-webpay.svg'
import CurrencyFormat from 'react-currency-format';
import ModalComponent from '../../component/system/ModalComponent';
import { SERVER_API } from '../../utils/utils';
import { database } from '../../utils/firebase'
import CoverComponent from '../../component/CoverComponent';
import TermsConditions from '../../component/TermsConditions';

import { login } from '../../api/index'
import CarMenuComponent from '../../component/CarMenuComponent';
import ProductFromListComponent from '../../component/ProductFromListComponent';

class FinishPayPage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            msgShow: false,
            msgText: "",
            msgTitle: "",
            user: {},
            products: [],
            commerce: {},
            delivery: {},
            modalCardsTransbank: false,
            modalLoginAmipass: false,
            modalAccountsAmipass: false,
            modalSusbcribeTransbank: false,
            modalTermsConditions: false,
            acceptTerms: false,
            typePay: 0,
            cardsTranskbank: [],
            cardTranskbank: null,
            accountsAmipass: [],
            accountAmipass: null,
            amount: 0,
            deliveryAmount: 0,
            order: 0,
            shopping: {},
            urlSusbcribeTransbank: '',
            paying: false,
            goPay: false,
            secureKey: false,
            payingAmipass: false,
            msgCloseCommerce: false,
            msgNonFood: false,
            productsWithoutStock: [],
            productsSoldOut: [],
            showModalProductsWithoutStock: false,
            modalProductStockAcceptDisable: false
        }
        this.storeShoppingCar = this.storeShoppingCar.bind(this)
    }

    componentDidMount() {
        let delivery = JSON.parse(localStorage.getItem('delivery'));
        this.setState({ user: JSON.parse(localStorage.getItem('user')) })
        this.setState({ delivery })

        let { commerce } = this.props.match.params;
        let statusResponseCommerce = 0;

        fetch(`${SERVER_API}/commerce/show/name/${commerce}`, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        }).then(async response => {
            statusResponseCommerce = response.status
            return await response.json();
        }).then(async response => {
            switch (statusResponseCommerce) {
                case 200:
                    let { commerce, commerceConfig } = response
                    let { ecommerceConfig } = commerceConfig;
                    let car = [];

                    fetch(`${SERVER_API}/commerce/state/${commerce.internalID}`, {
                        method: 'GET',
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json',
                        },
                    }).then(async response => {
                        return await response.json()
                    }).then(response => {
                        let { isOpen } = response
                        if(!isOpen) {
                            this.setState({
                                msgCloseCommerce: true,
                            })
                        }
                    })
                    
                    this.setState({ 
                        commerce, 
                        deliveryAmount: commerceConfig.ecommerceConfig.deliveryPrice,
                        ecommerceName: ecommerceConfig.ecommerceName
                    });
                    this.props.commerceInfoReducerAction({ 
                        ...commerce, 
                        config: commerceConfig,
                        ecommerceName: ecommerceConfig.ecommerceName
                    })

                    let cart = await database
                        .collection(`${this.state.user.username}`)
                        .doc(`cart-${this.state.commerce.commerceID}${this.state.commerce.storeID}`)
                        .get()

                    if(cart.data()) {
                        if(cart.data().items.length > 0) {
                            let count = 0
                            let amount = 0
                            let productsByVerify = []
                            car = cart.data().items.map(item => {
                                productsByVerify.push({
                                    id: item.id,
                                    stock: item.quantity
                                })
                                count += item.quantity
                                amount += item.saleAmount * item.quantity
                                return item
                            })

                            this.props.countItemCarReducerAction({ 
                                count: count, 
                                amount: amount, 
                                items: car
                            })
                            this.calculateTotalAmount(car)
                            this.setState({ products: car })
                            this.validateStock(`${this.state.commerce.commerceID}${this.state.commerce.storeID}`,productsByVerify)
                        } else {
                            await database
                            .collection(`${this.state.user.username}`)
                            .doc(`cart-${this.state.commerce.commerceID}${this.state.commerce.storeID}`)
                            .delete()
                        }
                    }
                    break;
                default:
                    this.setState({
                        msgModal: true,
                        msgTitle: "Error inesperado",
                        msgText: `Hubo un error inesperado consultando la información del comercio. COD (${statusResponseCommerce})`
                    })
                    break;
            }
            let search = queryString.parse(this.props.location.search.replace("?", ""))
            if((typeof search.typePay !== 'undefined' && typeof search.acceptTerms !== 'undefined')) {
                this.setState({
                    typePay: Number.parseInt(search.typePay),
                    acceptTerms: (search.acceptTerms === "true"),
                    shopping: JSON.parse(localStorage.getItem('shopping'))
                })
                this.loadRoadToPay()
            }
        }).catch(error => {
            console.log(error)
            this.setState({
                msgModal: true,
                msgTitle: "Error",
                msgText: `Hubo un error cargando los datos del comercio. COD (${statusResponseCommerce})`
            })
        })

        login.refreshToken().catch(error => {
            this.setState({
                msgModal: true,
                msgTitle: "Error inesperado",
                msgText: `Debe iniciar sesión para continuar con la compra.`
            })
        })
    }

    calculateTotalAmount = (products) => {
        let totalAmount = 0;
        products.map(item => {
            return totalAmount += item.saleAmount * item.quantity
        })
        this.setState({totalAmount})
    }
    
    validateStock = async (commerce, productsByVerify) => {
        await fetch(`${SERVER_API}/commerce/product/validate/${commerce}`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(productsByVerify)
        }).then(async response => {
            return await response.json();
        }).then(async response => {
            let { productStocks } = response
            if(productStocks.length > 0) {
                let productsWithoutStock = []
                let productsSoldOut = []
                this.state.products.map(item => {
                    let prod = productStocks.filter(a => item.id === a.id)
                    if(prod.length > 0) {
                        if(prod[0].stock === 0) {
                            item.stk = prod[0].stock
                            productsSoldOut.push(item)
                        } else {
                            productsWithoutStock.push(item)
                        }
                    }
                    return prod
                })

                if(productsSoldOut.length > 0) {
                    let newDataCart = []
                    let newCart = [];
                    this.state.products.map(item => {
                        let productInStock = productsSoldOut.filter(a => a.id === item.id)
                        if(productInStock.length === 0) {
                            return newDataCart.push(item)
                        }
                        return false
                    })

                    let count = 0
                    let amount = 0
                    newDataCart.map(item => {
                        count += item.quantity
                        amount += item.saleAmount * item.quantity
                        return newCart.push(item)
                    })
                    this.props.countItemCarReducerAction({ 
                        count: count, 
                        amount: amount, 
                        items: newCart 
                    })
                    this.setState({ products: newCart })
                }
                
                this.setState({ 
                    productsWithoutStock,
                    productsSoldOut,
                    showModalProductsWithoutStock: true, 
                    modalProductStockAcceptDisable: (productsWithoutStock.length > 0 && productsSoldOut.length > 0) || !(productsWithoutStock.length === 0 && productsSoldOut.length > 0)
                })
            }
        })
    }

    addProduct = async (item) => {
        let user = JSON.parse(localStorage.getItem('user'));
        let newData = [];
        let newProductsWithoutStock = []
        let countItem = this.props.shoppingCarMenuReducer.count
        let oldAmount = this.props.shoppingCarMenuReducer.amount
        let newAmount = 0
        this.state.products.map(product => {
            if(product.id === item.id) {
                if(item.quantity === item.stk) return newData.push(product)
                let quantityOld = product.quantity
                product.quantity++
                countItem = countItem + 1
                newAmount = (product.quantity === 1) ? (product.saleAmount * product.quantity) : ((product.saleAmount * product.quantity) - (product.saleAmount * quantityOld))
                return newData.push(product)
            }
            return newData.push(product)
        })

        let itemsCar = newData.filter(item => item.quantity > 0)

        this.props.countItemCarReducerAction({ 
            count: countItem,
            amount: (oldAmount + newAmount), 
            items: itemsCar 
        })

        this.props.itemsCarReducerAction(itemsCar)

        const cart = await database
        .collection(`${user.username}`)
        .doc(`cart-${this.state.commerce.commerceID}${this.state.commerce.storeID}`)

        cart.set({
            commerce: this.state.commerce.commerceID,
            store: this.state.commerce.storeID,
            commerceAlias: this.state.ecommerceName,
            items: itemsCar
        })

        this.state.productsWithoutStock.map(item => {
            let data = newData.filter(a => a.id === item.id)[0]
            if(typeof data !== 'undefined') {
                return newProductsWithoutStock.push(data)
            }
            return newProductsWithoutStock.push(item)
        })
        this.setState({
            productsWithoutStock: newProductsWithoutStock,
            modalProductStockAcceptDisable: !(typeof itemsCar.filter(a => a.quantity > a.stk)[0] === 'undefined')
        })
    }

    removeProduct = async (item) => {
        let user = JSON.parse(localStorage.getItem('user'));
        let newData = [];
        let newProductsWithoutStock = []
        let countItem = this.props.shoppingCarMenuReducer.count
        let oldAmount = this.props.shoppingCarMenuReducer.amount
        let newAmount = 0
        this.state.products.map(product => {
            if(product.id === item.id) {
                if(item.quantity === 0) {
                    newAmount -= product.saleAmount
                    return newData.push(product)
                }
                let quantityOld = product.quantity
                product.quantity--
                countItem = countItem - 1
                newAmount = (product.saleAmount * quantityOld) - (product.saleAmount * product.quantity)
                return newData.push(product)
            }
            return newData.push(product)
        })

        let itemsCar = newData.filter(item => item.quantity > 0)

        this.props.countItemCarReducerAction({ 
            count: countItem,
            amount: (oldAmount - newAmount), 
            items: itemsCar 
        })

        this.props.itemsCarReducerAction(itemsCar)

        const cart = await database
        .collection(`${user.username}`)
        .doc(`cart-${this.state.commerce.commerceID}${this.state.commerce.storeID}`)

        cart.set({
            commerce: this.state.commerce.commerceID,
            store: this.state.commerce.storeID,
            commerceAlias: this.state.ecommerceName,
            items: itemsCar
        })

        this.state.productsWithoutStock.map(item => {
            let data = newData.filter(a => a.id === item.id)[0]
            if(typeof data !== 'undefined') {
                return newProductsWithoutStock.push(data)
            }
            return newProductsWithoutStock.push(item)
        })
        this.setState({
            productsWithoutStock: newProductsWithoutStock,
            modalProductStockAcceptDisable: !(typeof itemsCar.filter(a => a.quantity > a.stk)[0] === 'undefined')
        })
    }

    storeShoppingCar = (e) => {
        e.preventDefault()
        this.setState({ goPay: true })
        let productsByVerify = []
        this.state.products.map(item => {
            return productsByVerify.push({
                id: item.id,
                stock: item.quantity
            })
        })
        this.validateStock(`${this.state.commerce.commerceID}${this.state.commerce.storeID}`, productsByVerify)
        .then(() => {
            if(this.state.showModalProductsWithoutStock) return;
            fetch(`${SERVER_API}/commerce/state/${this.state.commerce.commerceID}${this.state.commerce.storeID}`, {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
            }).then(async response => {
                return await response.json()
            }).then(response => {
                let { isOpen } = response
                if(!isOpen) {
                    this.setState({
                        msgCloseCommerce: true,
                    })
                } else {
                    
                    let products = []
                    this.setState({ goPay: true })
                    this.state.products.map(item => {
                        return products.push({
                            productIdMac: item.id,
                            name: item.productName,
                            quantity: item.quantity,
                            unitPrice: item.saleAmount,
                            discountAmount: 0,
                            total: item.saleAmount * item.quantity,
                        })
                    });
                    let statusResponse = 0
                    if(localStorage.getItem('shopping')) {
                        this.setState({
                            shopping: JSON.parse(localStorage.getItem('shopping'))
                        })
                        this.loadRoadToPay()
                    } else {
                        fetch(`${SERVER_API}/shopping/store`, {
                            method: 'POST',
                            headers: {
                                'Accept': 'application/json',
                                'Content-Type': 'application/json',
                                'Authorization': `Bearer ${localStorage.getItem('jwt')}`,
                                'RememberToken': `${localStorage.getItem('tkey')}`
                            },
                            body: JSON.stringify({
                                userId: this.state.user.codeUser,
                                commerce: this.state.commerce.commerceID,
                                store: this.state.commerce.storeID,
                                channelShoppingId: 1,
                                paymentTypeId: this.state.typePay,
                                userAddressId: this.state.delivery.id,
                                status: 1,
                                totalAmount: this.props.shoppingCarMenuReducer.amount,
                                deliveryAmount: this.state.delivery.deliveryAmount,
                                discountAmount: 0,
                                delivery: this.state.delivery.delivery,
                                comment: `Contacto: ${this.state.delivery.fullName} - ${this.state.delivery.phone} - ${this.state.delivery.date}`,
                                fullName: this.state.delivery.fullName,
                                phone: this.state.delivery.phone,
                                shoppingCarItemsByShoppingCarId: products,
                            })
                        }).then(async response => {
                            statusResponse = response.status
                            return await response.json()
                        }).then(response => {
                            switch (statusResponse) {
                                case 200:
                                    this.setState({ shopping: response})
                                    localStorage.setItem('shopping', JSON.stringify(response))
                                    this.loadRoadToPay()
                                    break;
                                default:
                                    this.setState({
                                        goPay: false,
                                        msgModal: true,
                                        msgTitle: "Error inesperado",
                                        msgText: `Hubo un error inesperado . COD (${statusResponse})`
                                    })
                                    break;
                            }
                        }).catch(error => {
                            console.log(error)
                            this.setState({ goPay: false })
                            this.setState({
                                msgModal: true,
                                msgTitle: "Error",
                                msgText: `Hubo un error. COD (${statusResponse})`
                            })
                        })
                    }
                }
            })
        })
    }

    loadRoadToPay = () => {
        let statusResponse = 0
        if(this.state.typePay === 1) {
            let nonFoodExist = this.state.products.filter(item => item.food === false)
            if(nonFoodExist.length === 0) {
                fetch(`${SERVER_API}/profile/account/amipass/show/${this.state.user.codeUser}`, {
                    method: 'GET',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${localStorage.getItem('jwt')}`,
                        'RememberToken': `${localStorage.getItem('tkey')}`
                    },
                }).then(async response => {
                    statusResponse = response.status
                    return await response.json()
                }).then(response => {
                    switch (statusResponse) {
                        case 200:
                            if(response === null || response.length === 0) {
                                this.setState({ modalLoginAmipass: true })
                            } else {
                                this.setState({ accountsAmipass: response })
                                this.setState({ modalAccountsAmipass: true })
                            }
                            break;
                        default:
                            this.setState({
                                msgModal: true,
                                msgTitle: "Error inesperado",
                                msgText: `Hubo un error inesperado. COD (${statusResponse})`
                            })
                            break;
                    }
                }).catch(error => {
                    console.log(error)
                    this.setState({ goPay: false })
                    this.setState({
                        msgModal: true,
                        msgTitle: "Error",
                        msgText: `Hubo un error. COD (${statusResponse})`
                    })
                })
            } else {
                this.setState({ goPay: false, msgNonFood: true })
            }
        } 
        if(this.state.typePay === 2) {
            fetch(`${SERVER_API}/pay/transbank/card`, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('jwt')}`,
                    'RememberToken': `${localStorage.getItem('tkey')}`
                },
                body: JSON.stringify({
                    username: this.state.user.username,
                    email: this.state.user.email
                })
            }).then(async response => {
                statusResponse = response.status
                return await response.json()
            }).then(response => {
                switch (statusResponse) {
                    case 200:
                        if(response.length === 0) {
                            this.subscribeCardTransbank()
                        } else {
                            this.setState({ cardsTranskbank: response })
                            this.setState({ modalCardsTransbank: true })
                        }
                        break;
                    default:
                        this.setState({
                            msgModal: true,
                            msgTitle: "Error inesperado",
                            msgText: `Hubo un error inesperado . COD (${statusResponse})`
                        })
                        break;
                }
            }).catch(error => {
                console.log(error)
                this.setState({ goPay: false })
                this.setState({
                    msgModal: true,
                    msgTitle: "Error",
                    msgText: `Hubo un error. COD (${statusResponse})`
                })
            })
        }
    }

    subscribeCardTransbank = () => {
        //4051885600446623
        let statusResponse = 0
        fetch(`${SERVER_API}/pay/transbank/card/subscribe`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('jwt')}`,
                'RememberToken': `${localStorage.getItem('tkey')}`
            },
            body: JSON.stringify({
                email: this.state.user.email,
                username: this.state.user.username,
                responseUrl: `${window.location.protocol}//${window.location.host}/local/${this.state.ecommerceName}/pay/end/?typePay=${this.state.typePay}&acceptTerms=${this.state.acceptTerms}`
            })
        }).then(async response => {
            statusResponse = response.status
            return await response.text()
        }).then(response => {
            switch (statusResponse) {
                case 200:
                    this.setState({ goPay: false })
                    // this.setState({ urlSusbcribeTransbank: response, modalSusbcribeTransbank: true })
                    window.open(response)
                    break;
                default:
                    this.setState({
                        msgModal: true,
                        msgTitle: "Error inesperado",
                        msgText: `Hubo un error inesperado . COD (${statusResponse})`
                    })
                    break;
            }
        }).catch(error => {
            console.log(error)
            this.setState({
                msgModal: true,
                msgTitle: "Error",
                msgText: `Hubo un error. COD (${statusResponse})`
            })
        })
    }

    setAccountAmipass = (item) => {
        this.setState({ accountAmipass: item })
    }

    setCardTransbank = (card) => {
        this.setState({ cardTranskbank: card });
    }

    goPayAmipass = (e) => {
        window.dataLayer.push({'event': 'amipassPayment'})
        e.preventDefault();
        this.setState({ payingAmipass: true })
        let statusResponse = 0
        fetch(`${SERVER_API}/pay/amipass`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('jwt')}`,
                'RememberToken': `${localStorage.getItem('tkey')}`
            },
            body: JSON.stringify({
                "sEmpleado": this.state.accountAmipass.employee,
                "sEmpresa": this.state.accountAmipass.employer,
                "sClave": this.state.key,
                "sEstablecimiento": this.state.commerce.commerceID,
                "sLocal": this.state.commerce.storeID,
                "nMonto": (this.state.delivery.delivery) ? this.state.shopping.totalAmount + this.state.delivery.deliveryAmount : this.state.shopping.totalAmount,
                "buyOrder": this.state.shopping.id
            })
        }).then(async response => {
            statusResponse = response.status
            return await response.json();
        }).then(response => {
            switch (statusResponse) {
                case 200:
                    let { cDatosTX, sMensaje } = response
                    const { history } = this.props;
                    let statusResponseOrder = 0
                    if(cDatosTX.sCodAutorizador === "1") {
                        fetch(`${SERVER_API}/shopping/order/store/${this.state.shopping.id}`, {
                            method: 'POST',
                            headers: {
                                'Accept': 'application/json',
                                'Content-Type': 'application/json',
                                'Authorization': `Bearer ${localStorage.getItem('jwt')}`,
                                'RememberToken': `${localStorage.getItem('tkey')}`
                            }
                        }).then(async response => {
                            statusResponseOrder = response.status
                            return await response.json()
                        }).then(async response => {
                            switch (statusResponseOrder) {
                                case 200:
                                    localStorage.removeItem('shopping')
                                    localStorage.removeItem('car')
                                    localStorage.removeItem('delivery')
                                    localStorage.removeItem('accountsAmipass')
                                    await database
                                    .collection(`${this.state.user.username}`)
                                    .doc(`cart-${this.state.commerce.commerceID}${this.state.commerce.storeID}`)
                                    .delete()
                                    history.push(`/local/${this.state.ecommerceName}/pay/successfully/${response.id}`)
                                    break;
                                default:
                                    this.setState({
                                        msgModal: true,
                                        msgTitle: "Error inesperado",
                                        msgText: `Hubo un error inesperado . COD (${statusResponseOrder})`
                                    })
                                    break;
                            }
                        }).catch(error => {
                            console.log(error);
                            this.setState({
                                msgModal: true,
                                msgTitle: "Error",
                                msgText: `Hubo un error. COD (${statusResponseOrder})`
                            })
                        })
                    } else {
                        this.setState({
                            msgModal: true,
                            msgTitle: "!Atención¡",
                            msgText: sMensaje
                        })
                    }
                    break;
                case 403:
                    this.setState({ paying: false })
                    this.setState({
                        msgModal: true,
                        msgTitle: "Productos no disponibles",
                        msgText: `Existen productos en tu carro que superan el limite para la venta del comercio. Vuelva a los productos para actualizar tu carro de compras. COD (${statusResponse})`
                    })
                    break;
                case 404:
                    this.setState({ paying: false })
                    this.setState({
                        msgModal: true,
                        msgTitle: "Comercio no disponible",
                        msgText: `Este comercio no está disponible para vender ya que se encuentra cerrado. COD (${statusResponse})`
                    })
                    break;
                default:
                    this.setState({
                        msgModal: true,
                        msgTitle: "Error inesperado",
                        msgText: `Hubo un error inesperado . COD (${statusResponse})`
                    })
                    break;
            }
        }).catch(error => { 
            console.log(error) 
            this.setState({ payingAmipass: false })
            this.setState({
                msgModal: true,
                msgTitle: "Error",
                msgText: `Hubo un error. COD (${statusResponse})`
            })
        })
    }

    goPayTransbank = () => {
        window.dataLayer.push({'event': 'transbankPayment'})
        if(this.state.cardTranskbank == null) {
            this.setState({
                msgModal: true,
                msgTitle: "¡Atención!",
                msgText: `Debe seleccionar una tarjeta para pagar`
            })
            return false
        }
        this.setState({ paying: true })
        let statusResponse = 0;
        fetch(`${SERVER_API}/pay/transbank`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('jwt')}`,
                'RememberToken': `${localStorage.getItem('tkey')}`
            },
            body: JSON.stringify({
                username: this.state.cardTranskbank.username,
                card: this.state.cardTranskbank.id,
                amount: (this.state.delivery.delivery) ? this.state.shopping.totalAmount + this.state.delivery.deliveryAmount : this.state.shopping.totalAmount,
                shopping: this.state.shopping.id,
                commerce: this.state.shopping.commerce,
                store: this.state.shopping.store,
                installments: 1,
                eticket: true,
            })
        }).then(async response => {
            statusResponse = response.status
            return await response.json()
        }).then(response => {
            switch (statusResponse) {
                case 200:
                    let statusStoreOrder = 0
                    const { history } = this.props;
                    if(response.code_response === "0") {
                        fetch(`${SERVER_API}/shopping/order/store/${this.state.shopping.id}`, {
                            method: 'POST',
                            headers: {
                                'Accept': 'application/json',
                                'Content-Type': 'application/json',
                                'Authorization': `Bearer ${localStorage.getItem('jwt')}`,
                                'RememberToken': `${localStorage.getItem('tkey')}`
                            }
                        }).then(async response => {
                            statusStoreOrder = response.status
                            return await response.json()
                        }).then(async response => {
                            switch (statusStoreOrder) {
                                case 200:
                                    localStorage.removeItem('shopping')
                                    localStorage.removeItem('car')
                                    localStorage.removeItem('delivery')
                                    localStorage.removeItem('accountsAmipass')
                                    await database
                                    .collection(`${this.state.user.username}`)
                                    .doc(`cart-${this.state.commerce.commerceID}${this.state.commerce.storeID}`)
                                    .delete()
                                    history.push(`/local/${this.state.ecommerceName}/pay/successfully/${response.id}`)
                                    break;
                                default:
                                    this.setState({ paying: false })
                                    this.setState({
                                        msgModal: true,
                                        msgTitle: "Error inesperado",
                                        msgText: `Hubo un error inesperado . COD (${statusStoreOrder})`
                                    })
                                    break;
                            }
                        }).catch(error => {
                            console.log(error)
                            this.setState({ paying: false })
                            this.setState({
                                msgModal: true,
                                msgTitle: "Error",
                                msgText: `Hubo un error. COD (${statusStoreOrder})`
                            })
                        })
                    } else {
                        history.push(`/local/${this.state.ecommerceName}/pay/declined`)
                    }
                    break;
                case 403:
                    this.setState({ paying: false })
                    this.setState({
                        msgModal: true,
                        msgTitle: "Productos no disponibles",
                        msgText: `Existen productos en tu carro que superan el limite para la venta del comercio. Vuelva a los productos para actualizar tu carro de compras. COD (${statusStoreOrder})`
                    })
                    break;
                case 404:
                    this.setState({ paying: false })
                    this.setState({
                        msgModal: true,
                        msgTitle: "Comercio no disponible",
                        msgText: `Este comercio no está disponible para vender ya que se encuentra cerrado. COD (${statusStoreOrder})`
                    })
                    break;
                default:
                    this.setState({ paying: false })
                    this.setState({
                        msgModal: true,
                        msgTitle: "Error inesperado",
                        msgText: `Hubo un error inesperado . COD (${statusResponse})`
                    })
                    break;
            }
        }).catch(error => {
            console.log(error)
            this.setState({ paying: false })
            this.setState({
                msgModal: true,
                msgTitle: "Error",
                msgText: `Hubo un error. COD (${statusResponse})`
            })
        })
    }

    handleCloseTransbank = () => {
        this.setState({ 
            modalCardsTransbank: false,
            goPay: false
        })
    }
    handleCloseModalAccountsAmipass = () => {
        this.setState({ 
            modalAccountsAmipass: false,
            goPay: false
        })
    }

    handleCloseSusbcribeTransbank = () => {
        this.setState({ 
            modalSusbcribeTransbank: false,
            goPay: false
        })
    }

    handleCloseLoginAmipass = () => {
        this.setState({ 
            modalLoginAmipass: false,
            goPay: false
        })
    }

    handleCloseTermsConditions = () => {
        this.setState({ 
            modalTermsConditions: false
        })
    }


    render() {
        return (
            <PageComponent>
                <CoverComponent>
                    <h3>Selecciona tu medio de pago</h3>
                    <span>Recuerda utilizar tu saldo amiPASS para la compra de alimentos</span>
                </CoverComponent>
                <div className='container my-5 h-100' >
                    <form onSubmit={this.storeShoppingCar} action={'#'} method={'POST'} ref={el => this.formShopping = el} >
                        <div className='row'>
                            <div className='col-md-8' >
                                <div className='row mb-5' >
                                    <div className='col'>
                                        <div className='card'>
                                            <div className='card-body'>
                                                <h6>Términos y condiciones</h6>
                                                <div>
                                                    <div className='float-left' >
                                                        <Form.Check required type="checkbox" checked={this.state.acceptTerms} name='terms' onChange={() => this.setState({ acceptTerms: !this.state.acceptTerms })} />
                                                    </div>
                                                    <div>
                                                        <small className='text-muted' >Acepto términos y condiciones según contrato con el emisor</small>
                                                        <br/> <Button variant='link' onClick={() => this.setState({ modalTermsConditions: true }) }>Ver términos y condiciones</Button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className='row mb-5' >
                                    <div className='col'>
                                        <div className='card'>
                                            <div className='card-body'>
                                                <h6>Medios de pago</h6>
                                                <div className='row'>
                                                    <div className='col'>
                                                        <div className='card h-100' >
                                                            <div className='card-body'>
                                                                <div className='float-left' >
                                                                    <Form.Check required type='radio' checked={(this.state.typePay === 1)} name='typePay' value={1} onChange={() => this.setState({ typePay: 1 })} />
                                                                </div>
                                                                <small className='text-muted' >amiPASS</small>
                                                                <div className='text-center py-4' >
                                                                    <Image src={logoAmipass} width={150} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className='col'>
                                                        <div className='card h-100' >
                                                            <div className='card-body'>
                                                                <div className='float-left' >
                                                                    <Form.Check required type='radio' name='typePay' checked={(this.state.typePay === 2)} value={2} onChange={() => this.setState({ typePay: 2 })} />
                                                                </div>
                                                                <small className='text-muted' >Tarjetas Débito/Crédito</small>
                                                                <div className='text-center py-4' >
                                                                    <Image src={logoWebpay} width={150}  />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className='row py-5' >
                                    <div className='col text-center' >
                                        <div>
                                            <button 
                                                className='btn btn-primary' 
                                                type='submit' 
                                                disabled={this.state.goPay} 
                                                onClick={() => { 
                                                    window.dataLayer.push({
                                                        event: 'goPay',
                                                        action: (this.state.typePay === 1) ? 'amiPASS' : 'Transbank'
                                                    })
                                                }}
                                            >{this.state.goPay ? 'Pagando...' : 'Ir a pagar'}</button>
                                        </div>
                                        <div>
                                            <button className='btn btn-link' type='button' onClick={() => {
                                                window.dataLayer.push({event: 'continueShopping'})
                                                window.history.pushState({},'',`/local/${this.state.ecommerceName}`);
                                            window.history.go()
                                            }} >Seguir comprando</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className='col'>
                                <div className='card'>
                                    <div className='card card-body'>
                                        <div className='text-center border-bottom' >
                                            <h6>Resumen de compra</h6>
                                        </div>
                                        <div className='py-4 border-bottom' >
                                            <CarMenuComponent data={this.props.shoppingCarMenuReducer.items} comment={false} />
                                        </div>
                                        <div className='py-2 border-bottom' >
                                            <small className='text-muted'>Sub total: <CurrencyFormat 
                                                value={this.props.shoppingCarMenuReducer.amount}
                                                displayType={'text'} 
                                                thousandSeparator={true} 
                                                prefix={'$'}
                                                renderText={value => (
                                                <span>{value.replace(/,/g, '.')}</span>
                                                )}
                                            /></small><br/>
                                            {
                                                this.state.delivery.delivery && 
                                                <>
                                                    <small className='text-muted'>Delivery: {
                                                        this.state.delivery.deliveryAmount > 0 ?
                                                        <CurrencyFormat 
                                                            value={this.state.delivery.deliveryAmount}
                                                            displayType={'text'} 
                                                            thousandSeparator={true} 
                                                            prefix={'$'}
                                                            renderText={value => (
                                                            <span>{value.replace(/,/g, '.')}</span>
                                                            )}
                                                        /> :
                                                        <span className='text-success' >Gratis</span>
                                                    }</small><br/>
                                                </>
                                            }
                                            {
                                                this.state.delivery.delivery ? 
                                                <>
                                                    <strong>
                                                        <span>Total a pagar: </span><CurrencyFormat 
                                                            value={this.props.shoppingCarMenuReducer.amount + this.state.delivery.deliveryAmount}
                                                            displayType={'text'} 
                                                            thousandSeparator={true} 
                                                            prefix={'$'}
                                                            renderText={value => (
                                                            <span>{value.replace(/,/g, '.')}</span>
                                                            )}
                                                        />
                                                    </strong>
                                                </> :
                                                <strong>
                                                    <span>Total a pagar: </span><CurrencyFormat 
                                                        value={this.props.shoppingCarMenuReducer.amount}
                                                        displayType={'text'} 
                                                        thousandSeparator={true} 
                                                        prefix={'$'}
                                                        renderText={value => (
                                                        <span>{value.replace(/,/g, '.')}</span>
                                                        )}
                                                    />
                                                </strong>
                                            }
                                        </div>
                                        <div className='py-2 border-bottom' >
                                            {
                                                this.state.delivery.delivery === true && 
                                                <>
                                                    <h6>Despacho a domicilio</h6>
                                                    <small className='text-muted' >{this.state.delivery.address}</small>
                                                </>
                                            }
                                            {
                                                this.state.delivery.delivery === false && 
                                                <>
                                                    <h6>Retiro en tienda</h6>
                                                    <small className='text-muted' >{this.state.commerce.localAddress}</small>
                                                </>
                                            }
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <Modal
                    show={this.state.modalAccountsAmipass}
                    onHide={this.handleCloseModalAccountsAmipass}
                    backdrop="static"
                    keyboard={false}
                    size="md"
                >
                    <Modal.Header closeButton>
                        <Modal.Title>Mis cuentas amiPASS</Modal.Title>
                    </Modal.Header>
                    <Form action='#' method='POST' onSubmit={this.goPayAmipass} >
                    <Modal.Body>
                        {
                            this.state.secureKey ? 
                            <>
                            <div className="card">
                                <div className="card-body">
                                    <div className='pb-3 text-center' >
                                        <h5>Ingresa la clave pago</h5>
                                    </div>
                                    <Form.Group>
                                        <Form.Control 
                                        required 
                                        type="password" 
                                        placeholder="Clave de pago"
                                        maxLength={4}
                                        onChange={ e => this.setState({ key: e.target.value }) }
                                        />
                                    </Form.Group>
                                </div>
                            </div>
                            </> :
                            <>
                                {
                                    this.state.accountsAmipass.map(item => (
                                        <div key={item.idusersAccountAmipass} className="card mb-3">
                                            <div className="card-body">
                                                <div className='float-left' >
                                                    <input required type='radio' value={item.cardCode} onChange={() => this.setAccountAmipass(item)} name='account' />
                                                </div>
                                                <div className='col ml-3'>
                                                    <span><strong>{item.employeeName}</strong></span><br/>
                                                    <span>{item.employerName}</span><br/>
                                                    <span>XXXXX{item.cardCode.slice(6)}</span>
                                                </div>
                                            </div>
                                        </div>
                                    ))
                                }
                            </>
                        }
                    </Modal.Body>
                    <Modal.Footer>
                        {
                            this.state.secureKey ?
                            <Button variant='primary' type='submit' disabled={this.state.payingAmipass} >
                                {this.state.payingAmipass ? 'Pagando...' : 'Pagar'}
                            </Button>
                            :
                            <Button variant='primary' type='button' onClick={() => { this.setState({ secureKey: true }) }}>
                                Siguiente
                            </Button>
                        }
                    </Modal.Footer>
                    </Form>
                </Modal>
                <Modal
                    show={this.state.modalCardsTransbank}
                    onHide={this.handleCloseTransbank}
                    backdrop="static"
                    keyboard={false}
                    centered
                >
                    <Modal.Header closeButton>
                        <Modal.Title>Tarjetas registradas</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        {
                            this.state.cardsTranskbank.map(item => (
                                <Card key={item.id}>
                                    <Card.Body>
                                        <div className='float-left' >
                                            <Form.Check type='radio' name='cardTransbank' value={item.id} onChange={() => this.setCardTransbank(item)} />
                                        </div>
                                        <span>{item.last_four_card_digits}</span>
                                        <span className='float-right'>{item.credit_card_type}</span>
                                    </Card.Body>
                                </Card>
                            ))
                        }
                    </Modal.Body>
                    <Modal.Footer>
                        <div className='float-left' >
                            <Button variant='link' onClick={() => {
                                this.setState({ modalCardsTransbank: false })
                                this.subscribeCardTransbank()
                            }} disabled={this.state.paying} >
                                + Agregar otra tarjeta
                            </Button>
                        </div>
                        <Button variant='primary' onClick={this.goPayTransbank} disabled={this.state.paying} >
                            {this.state.paying ? 'Pagando...' : 'Pagar'}
                        </Button>
                    </Modal.Footer>
                </Modal>

                <ModalComponent
                    show={this.state.modalSusbcribeTransbank}
                    handleClose={this.handleCloseSusbcribeTransbank}
                    title='Pago por Transbank'
                    size={'lg'}
                >
                    <div className="embed-responsive embed-responsive-16by9">
                        <iframe 
                            className="embed-responsive-item" 
                            title='transbank' 
                            src={this.state.urlSusbcribeTransbank}
                        ></iframe>
                    </div>
                </ModalComponent>

                <ModalComponent
                    show={this.state.modalLoginAmipass}
                    handleClose={this.handleCloseLoginAmipass}
                    title='Vas a pagar con tu cuenta amiPASS'
                    size={'lg'}
                >
                    <div className="embed-responsive embed-responsive-16by9">
                        <iframe 
                            className="embed-responsive-item" 
                            title='transbank' 
                            src={`/local/${this.state.ecommerceName}/pay/account/amipass/login`}
                        ></iframe>
                    </div>
                </ModalComponent>
                <ModalComponent
                    show={this.state.modalTermsConditions} 
                    handleClose={() => this.setState({ modalTermsConditions: false })}
                    title='Terminos y Condiciones'
                    size={'lg'}
                >
                    <TermsConditions />
                </ModalComponent>
                <ModalComponent
                    show={this.state.msgModal} 
                    handleClose={() => this.setState({ msgModal: false })}
                    title={this.state.msgTitle}
                >
                    <p>{this.state.msgText}</p>
                </ModalComponent>
                <ModalComponent
                    show={this.state.msgCloseCommerce} 
                    handleAccept={() => {
                        this.props.history.push(`/home`)
                    }}
                    title={'Comercio cerrado'}
                >
                    <p>Este comercio se encuentra cerrado</p>
                </ModalComponent>
                <ModalComponent
                    show={this.state.msgNonFood} 
                    handleClose={() => {
                        this.setState({ msgNonFood: false })
                    }}
                    title={'Revisa tu pedido'}
                >
                    <p>Existen productos dentro del carro que no pueden ser pagados con amiPASS</p>
                    <p>Selecciona otro medio de pago o elimina los productos que no tienen la etiqueta "Paga con amiPASS" dentro del resumen de la compra</p>
                </ModalComponent>
                <ModalComponent
                    show={this.state.showModalProductsWithoutStock} 
                    handleAccept={() => {
                        this.setState({showModalProductsWithoutStock: false})
                    }}
                    textAccept='Continuar'
                    acceptDisabled={this.state.modalProductStockAcceptDisable}
                    title={'Cantidad no disponible'}
                >
                    {
                        this.state.productsWithoutStock.map(item => 
                            <>
                            <p>Los siguientes productos superan la cantidad disponible para efectuar tu compra. Cambie la cantidad para continuar.</p>
                            <div>
                                <ProductFromListComponent 
                                    controls={true}
                                    key={item.id} 
                                    item={item} 
                                    addProduct={() => this.addProduct(item)}
                                    removeProduct={() => this.removeProduct(item)}
                                />
                            </div>
                            </>
                        )
                    }
                    {
                        this.state.productsSoldOut.length > 0 &&
                        <>
                            <p>Estos productos se encuentran agotados</p>
                            <div>
                                {
                                    this.state.productsSoldOut.map(item => {
                                        return <ProductFromListComponent 
                                            controls={true}
                                            key={item.id} 
                                            item={item} 
                                            addProduct={() => this.addProduct(item)}
                                            removeProduct={() => this.removeProduct(item)}
                                        />
                                    })
                                }
                            </div>
                        </>
                    }
                </ModalComponent>
            </PageComponent>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        shoppingCarMenuReducer: state.shoppingCarMenuReducer,
        commerceInfoReducer: state.commerceInfoReducer
    }
}

const mapDispatchToProps = {
    commerceInfoReducerAction,
    countItemCarReducerAction,
    itemsCarReducerAction,
}

export default connect(mapStateToProps,mapDispatchToProps)(FinishPayPage) 