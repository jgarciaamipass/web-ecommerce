import React, { Component } from 'react'
import { connect } from 'react-redux';
import menuAuthReducerAction from '../../redux/actions/menuAuthReducerAction'
import commerceInfoReducerAction from '../../redux/actions/commerceInfoReducerAction';
import { Button, Card, Form } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import PageComponent from '../../component/PageComponent'
import ModalComponent from '../../component/system/ModalComponent';
import { SERVER_API } from '../../utils/utils';
import { database } from '../../utils/firebase'

class LoginPayPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
          email: "",
          password: "",
          remember: false,
          isLoading: false,
          msgModal: false,
          msgText: '',
          msgTitle: '',
          commerce: {}
        };
    }

    handleCloseModal = () => {
        this.setState({ showModal: false })
    }

    componentWillMount() {
        const { history } = this.props;
        let { commerce } = this.props.match.params;

        if(localStorage.getItem('jwt') !== null) {
            history.push(`/local/${commerce}/pay/delivery`)
        }

        let statusResponse = 0
        fetch(`${SERVER_API}/commerce/show/name/${commerce}`, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
        }).then(async response => {
            statusResponse = response.status
            return await response.json()
        }).then(async response => {
            let { commerce, commerceConfig } = response;
            let { ecommerceConfig } = commerceConfig;
            this.setState({ 
                commerce: {
                    ...commerce,
                    ecommerceName: ecommerceConfig.ecommerceName
                }, 
            });

            this.props.commerceInfoReducerAction({ 
                ...commerce, 
                config: commerceConfig,
                ecommerceName: ecommerceConfig.ecommerceName
            })
        }).catch(error => {
            console.log(error)
            this.setState({
                msgModal: true,
                msgTitle: "Error",
                msgText: `Hubo un error al intentar de cargar los datos del comercio. Por favor intente más tarde. COD (${statusResponse})`
            })
        })
    }

    login = (e) => {
        const { email, password } = this.state;
        let statusResponse = 0;
        this.setState({ isLoading: true })
        fetch(`${SERVER_API}/login`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                email,
                password
            })
        }).then(async response => {
            statusResponse = response.status;
            return await response.json();
        }).then(async response => {
            switch (statusResponse) {
                case 200:
                    const { jwt, iduser, fullname } = response
                    if(typeof jwt == 'undefined') return

                    localStorage.setItem('jwt', jwt)
                    localStorage.setItem('user', JSON.stringify({ 
                        codeUser: iduser,
                        username: iduser, 
                        email: this.state.email,
                        fullname
                    }))
                    this.props.menuAuthReducerAction({ isAuth: true })

                    if(localStorage.getItem('userTemp')) {
                        const userTemp = await database.collection(`${localStorage.getItem('userTemp')}`)
                        const carts = await userTemp.get();
                        if(carts.docs.length > 0) {
                            if(window.confirm('¿Quieres seguir con tu compra?')) {
                                carts.forEach(async item => {
                                    let data = item.data();
                                    let cart = await database
                                    .collection(`${iduser}`)
                                    .doc(`cart-${data.commerce}${data.store}`)
                                    
                                    cart.set(data)
    
                                    userTemp
                                    .doc(item.id)
                                    .delete()
                                })
                            } else {
                                carts.forEach(async item => {
                                    userTemp
                                    .doc(item.id)
                                    .delete()
                                })
                            }
                        }
                    } else {
                        let itemsCar = JSON.parse(localStorage.getItem('car'));

                        const cart = await database
                        .collection(`${iduser}`)
                        .doc(`cart-${this.state.commerce.commerceID}${this.state.commerce.storeID}`)
                        
                        cart.set({
                            commerce: this.state.commerce.commerceID,
                            store: this.state.commerce.storeID,
                            commerceAlias: this.state.commerce.ecommerceName,
                            items: itemsCar
                        })
                    }

                    localStorage.removeItem('userTemp')

                    this.props.history.push(`/local/${this.state.commerce.ecommerceName}/pay/delivery`)
                    break;
                case 401:
                    this.setState({
                        msgModal: true,
                        msgTitle: "Error",
                        msgText: "Usuario o clave inválidos"
                    })
                    break;
                case 403:
                    this.setState({
                        msgModal: true,
                        msgTitle: "¡Atención!",
                        msgText: "El usuario se encuentra bloqueado. Debe activar su cuenta para iniciar sesión"
                    })
                    break;
                case 404:
                    this.setState({
                        msgModal: true,
                        msgTitle: "Error ingresando",
                        msgText: `El usuario ${this.state.email} no existe, por favor registrese o ingrese un email registrado`
                    })
                    break;
                default:
                    this.setState({
                        msgModal: true,
                        msgTitle: "Error inesperado",
                        msgText: `Hubo un error inesperado iniciando sesión. COD (${statusResponse})`
                    })
                    break;
            }
            this.setState({ isLoading: false })
        }).catch(error => {
            console.log(error)
            this.setState({
                msgModal: true,
                msgTitle: "Error",
                msgText: `Hubo un error intentando de iniciando sesión. COD (${statusResponse})`
            })
        })
        e.preventDefault();
    }

    render() {
        return (
            <PageComponent>
                <div className="d-flex align-items-center h-100" >
                    <div className='d-flex flex-column align-items-center w-100' >
                    <Card className='col-lg-4 col-md-8 body-form-ami' >
                            <Card.Body>
                                <div className='text-center' >
                                    <h3>Ingresa tus datos</h3>
                                    <p>
                                        Donde estés, estamos para ti, siempre podrás comprar con tu amiPASS
                                    </p>
                                </div>
                                <Form onSubmit={this.login} action={"#"} method={'POST'} >
                                    <Form.Group>
                                        <Form.Control required type="email" placeholder="Correo electrónico" onChange={ e => this.setState({ email: e.target.value }) } />
                                    </Form.Group>
                                    <Form.Group>
                                        <Form.Control required type="password" placeholder="Contraseña" onChange={ e => this.setState({ password: e.target.value })} />
                                    </Form.Group>
                                    <Form.Group controlId="formBasicCheckbox">
                                        <Form.Check value={this.state.remember} onChange={() => this.setState({ remember: !this.state.remember })} type="checkbox" label="Recordar usuario y contraseña" />
                                    </Form.Group>
                                    <Button variant="primary" type="submit" block disabled={this.state.isLoading}>
                                        {this.state.isLoading ? 'Iniciando sesión...' : 'Ingresar'}
                                    </Button>
                                    <div className="mt-3 text-center " >
                                        <Link href="#" to={'/forget_password'} >¿Olvidó su clave?</Link>
                                    </div>
                                </Form>
                            </Card.Body>
                        </Card>
                    </div>
                </div>
                <ModalComponent
                    show={this.state.msgModal} 
                    handleClose={() => this.setState({ msgModal:false })}
                    title={this.state.msgTitle}
                >
                    <p>{this.state.msgText}</p>
                </ModalComponent>
            </PageComponent>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        menuAuthReducer: state.menuAuthReducer
    }
}

const mapDispatchToProps = {
    menuAuthReducerAction,
    commerceInfoReducerAction
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginPayPage)