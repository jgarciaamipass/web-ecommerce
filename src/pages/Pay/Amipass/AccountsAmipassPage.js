import React, { Component } from 'react'
import { Button, Form } from 'react-bootstrap';
import CurrencyFormat from 'react-currency-format';
import ModalComponent from '../../../component/system/ModalComponent';
import { SERVER_API } from '../../../utils/utils';

export default class AccountsAmipassPage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            msgShow: false,
            msgText: "",
            msgTitle: "",
            commerce: {},
            ecommerceName: '',
            accounts: [],
            account: null,
            shopping: {},
            paying: false,
            goPay: false,
            key: ""
        }
    }

    componentDidMount() {
        let { commerce } = this.props.match.params;
        let accounts = JSON.parse(localStorage.getItem('accountsAmipass'));
        let shopping = JSON.parse(localStorage.getItem('shopping'));
        console.log(shopping)
        this.setState({ 
            accounts, 
            shopping,
            ecommerceName: commerce
        })
        let statusResponse = 0
        fetch(`${SERVER_API}/commerce/show/name/${commerce}`, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('jwt')}`,
                'RememberToken': `${localStorage.getItem('tkey')}`
            },
        }).then(async response => {
            statusResponse = response.status
            return await response.json();
        }).then(response => {
            switch (statusResponse) {
                case 200:
                    let { commerce } = response
                    this.setState({ commerce });
                    break;
                default:
                    this.setState({
                        msgModal: true,
                        msgTitle: "Error inesperado",
                        msgText: `Hubo un error inesperado . COD (${statusResponse})`
                    })
                    break;
            }
        }).catch(error => { 
            console.log(error)
            this.setState({
                msgModal: true,
                msgTitle: "Error",
                msgText: `Hubo un error. COD (${statusResponse})`
            })
        })
    }

    setAccountAmipass = (item) => {
        this.setState({ account: item })
    }

    goPayAmipass = (e) => {
        e.preventDefault();
        this.setState({ paying: true })
        let statusResponse = 0
        fetch(`${SERVER_API}/pay/amipass`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('jwt')}`,
                'RememberToken': `${localStorage.getItem('tkey')}`
            },
            body: JSON.stringify({
                "sEmpleado": this.state.account.username,
                "sEmpresa": this.state.account.employer,
                "sClave": this.state.key,
                "sEstablecimiento": this.state.commerce.commerceID,
                "sLocal": this.state.commerce.storeID,
                "nMonto": (this.state.shopping.delivery) ? this.state.shopping.totalAmount + this.state.shopping.deliveryAmount : this.state.shopping.totalAmount,
                "buyOrder": this.state.shopping.id
            })
        }).then(async response => {
            statusResponse = response.status
            return await response.json();
        }).then(response => {
            console.log(response)
            switch (statusResponse) {
                case 200:
                    let { cDatosTX, sMensaje } = response
                    let statusResponseStore = 0
                    if(cDatosTX.sCodAutorizador === "1") {
                        fetch(`${SERVER_API}/shopping/order/store/${this.state.shopping.id}`, {
                            method: 'POST',
                            headers: {
                                'Accept': 'application/json',
                                'Content-Type': 'application/json',
                                'Authorization': `Bearer ${localStorage.getItem('jwt')}`,
                                'RememberToken': `${localStorage.getItem('tkey')}`
                            }
                        }).then(async response => {
                            statusResponseStore = response.status
                            return await response.json()
                        }).then(response => {
                            switch (statusResponseStore) {
                                case 200:
                                    let iframe = window.frameElement.ownerDocument.defaultView;
                                    iframe.history.pushState(
                                        {},
                                        '',
                                        `/local/${this.state.ecommerceName}/pay/successfully/${response.id}`
                                    );
                                    iframe.history.go()
                                    break;
                                default:
                                    this.setState({ paying: false })
                                    this.setState({
                                        msgModal: true,
                                        msgTitle: "Error inesperado",
                                        msgText: `Hubo un error inesperado . COD (${statusResponseStore})`
                                    })
                                    break;
                            }
                        }).catch(error => {
                            console.log(error);
                            this.setState({ paying: false })
                            this.setState({
                                msgModal: true,
                                msgTitle: "Error",
                                msgText: `Hubo un error. COD (${statusResponseStore})`
                            })
                        })
                    } else {
                        this.setState({ paying: false })
                        this.setState({
                            msgModal: true,
                            msgTitle: "¡Atención!",
                            msgText: sMensaje
                        })
                    }
                    break;
                default:
                    this.setState({ paying: false })
                    this.setState({
                        msgModal: true,
                        msgTitle: "Error inesperado",
                        msgText: `Hubo un error inesperado . COD (${statusResponse})`
                    })
                    break;
            }
        }).catch(error => { 
            console.log(error) 
            this.setState({ paying: false })
            this.setState({
                msgModal: true,
                msgTitle: "Error",
                msgText: `Hubo un error. COD (${statusResponse})`
            })
        })
    }

    render() {
        return (
            <div className='container my-3' >
                <div className="d-flex align-items-center h-100">
                    <div className='d-flex flex-column align-items-center w-100' >
                        <Form onSubmit={this.goPayAmipass} action={"#"} method={'POST'} autoComplete='false' >
                            {
                                !this.state.goPay ? 
                                <>
                                {
                                    this.state.accounts.map(item => (
                                        <div key={item.numberCard} className="card mb-3">
                                            <div className="card-body">
                                                <div className='float-left' >
                                                    <input type='radio' value={item.numberCard} onChange={() => this.setAccountAmipass(item)} name='account' />
                                                </div>
                                                <div className='col ml-3'>
                                                    <span><strong>{item.firstName} {item.lastName} {item.lastName2}</strong></span><br/>
                                                    <span>{item.employerName} ({item.commercialName})</span><br/>
                                                    <span>XXXXX{item.numberCard.slice(6)}</span><br/>
                                                    <CurrencyFormat 
                                                        value={item.balance}
                                                        displayType={'text'} 
                                                        thousandSeparator={true} 
                                                        prefix={'$'}
                                                        renderText={value => (
                                                        <span><strong>{value.replace(/,/g, '.')}</strong></span>
                                                        )}
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                    ))
                                }
                                <div className='text-center mt-4' >
                                    <Button variant='primary' type="button"
                                        onClick={() => {
                                            if(this.state.account == null) {
                                                this.setState({ paying: false })
                                                this.setState({
                                                    msgModal: true,
                                                    msgTitle: "¡Atención!",
                                                    msgText: `Debe seleccionar una cuenta para continuar`
                                                })
                                                return false
                                            }
                                            this.setState({ goPay: true })
                                        }}
                                    >
                                        Siguiente
                                    </Button>
                                </div>
                                </> :
                                <>
                                <div className="card">
                                    <div className="card-body">
                                        <div className='pb-3 text-center' >
                                            <h5>Ingresa la clave pago</h5>
                                        </div>
                                        <Form.Group>
                                            <Form.Control 
                                            required 
                                            type="password" 
                                            placeholder="Clave de pago"
                                            maxLength={4}
                                            onChange={ e => this.setState({ key: e.target.value }) }
                                            />
                                        </Form.Group>
                                        <div className='text-center' >
                                            <Button variant='primary' type="submit" disabled={this.state.paying}>
                                                {this.state.paying ? 'Pagando...' : 'Pagar'}
                                            </Button>
                                        </div>
                                    </div>
                                </div>
                                </>
                            }
                        </Form>
                    </div>
                </div>
                <ModalComponent
                    show={this.state.msgModal} 
                    handleClose={() => this.setState({ msgModal: false })}
                    title={this.state.msgTitle}
                >
                    <p>{this.state.msgText}</p>
                </ModalComponent>
            </div>
        )
    }
}
