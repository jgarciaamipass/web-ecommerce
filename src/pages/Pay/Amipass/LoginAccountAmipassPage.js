import React, { Component } from 'react'
import { Button, Form } from 'react-bootstrap'
import ModalComponent from '../../../component/system/ModalComponent';
import { SERVER_API } from '../../../utils/utils';

export default class LoginAccountAmipassPage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            msgShow: false,
            msgText: "",
            msgTitle: "",
            username: "",
            password: "",
            remember: "",
            isLoading: false,
            commerce: "",
            numDocument: ""
        }
    }

    componentDidMount() {
        let user = JSON.parse(localStorage.getItem('user'));
        let { commerce } = this.props.match.params;
        this.setState({ commerce })
        let statusResponse = 0
        fetch(`${SERVER_API}/profile/show/info_basic/${user.codeUser}`, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('jwt')}`,
                'RememberToken': `${localStorage.getItem('tkey')}`
            },
        }).then(async response => {
            statusResponse = response.status
            return await response.json();
        }).then(response => {
            switch (statusResponse) {
                case 200:
                    let { numDocument } = response
                    this.setState({
                        numDocument
                    });
                    break;
                default:
                    this.setState({
                        msgModal: true,
                        msgTitle: "Error inesperado",
                        msgText: `Hubo un error inesperado . COD (${statusResponse})`
                    })
                    break;
            }
        }).catch(error => {
            console.log(error)
            this.setState({
                msgModal: true,
                msgTitle: "Error",
                msgText: `Hubo un error. COD (${statusResponse})`
            })
        })
    }

    login = (e) => {
        const { username, password, numDocument } = this.state;
        let statusResponseLogin = 0
        let statusResponseStore = 0
        if(username.startsWith('NN', 0) || (username !== numDocument)) {
            alert("Ud podrá cancelar con esta cuenta, pero no podra enrolarla para guardar y usar en futuros pagos")
            this.setState({ remember: false })
        }
        this.setState({ isLoading: true })
        fetch(`${SERVER_API}/profile/account/amipass`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('jwt')}`,
                'RememberToken': `${localStorage.getItem('tkey')}`
            },
            body: JSON.stringify({
                username,
                password
            })
        }).then(async response => {
            statusResponseLogin = response.status
            return await response.json();
        }).then(response => {
            switch (statusResponseLogin) {
                case 200:
                    localStorage.setItem('accountsAmipass', JSON.stringify(response))
                    if(this.state.remember) {
                        let user = JSON.parse(localStorage.getItem('user'));
                        fetch(`${SERVER_API}/profile/account/amipass/store/${user.codeUser}`, {
                            method: 'POST',
                            headers: {
                                'Accept': 'application/json',
                                'Content-Type': 'application/json',
                                'Authorization': `Bearer ${localStorage.getItem('jwt')}`,
                                'RememberToken': `${localStorage.getItem('tkey')}`
                            },
                            body: JSON.stringify(response)
                        }).then(async response => {
                            statusResponseStore = response.status
                            return await response.json();
                        }).then(response => {
                            switch (statusResponseStore) {
                                case 200:
                                    window.history.pushState({}, 'Cuenta amipass',
                                    `/local/${this.state.commerce}/pay/account/amipass`)
                                    window.history.go()
                                    break;
                                default:
                                    this.setState({
                                        msgModal: true,
                                        msgTitle: "Error inesperado",
                                        msgText: `Hubo un error inesperado . COD (${statusResponseStore})`
                                    })
                                    break;
                            }
                        })
                        .catch(error => {
                            console.log(error)
                            this.setState({
                                msgModal: true,
                                msgTitle: "Error",
                                msgText: `Hubo un error. COD (${statusResponseStore})`
                            })
                        })
                    } else {
                        window.history.pushState({}, 'Cuenta amipass',
                        `/local/${this.state.commerce}/pay/account/amipass`)
                        window.history.go()
                    }
                    break;
                
                case 500:
                    this.setState({
                        msgModal: true,
                        msgTitle: "¡Atención!",
                        msgText: `Usuario o clave de internet de amipass inválida. COD (${statusResponseLogin})`
                    })
                    break;
                default:
                    this.setState({
                        msgModal: true,
                        msgTitle: "Error inesperado",
                        msgText: `Hubo un error inesperado . COD (${statusResponseLogin})`
                    })
                    break;
            }
            this.setState({ isLoading: false })
        }).catch(error => {
            console.log(error)
            this.setState({
                isLoading: false
            })
            this.setState({
                msgModal: true,
                msgTitle: "Error",
                msgText: `Hubo un error. COD (${statusResponseLogin}:${statusResponseStore})`
            })
        })
        e.preventDefault();
    }
    render() {
        return (
            <section className='container' style={{ flex: 1, marginTop: 50 }} >
                <div className="d-flex flex-column align-items-center h-100">
                    <div className='d-flex flex-column align-items-center w-100' >
                        <div className="card col-lg-4 col-md-8 body-form-ami">
                            <div className="card-body">
                                <div className='pb-3 text-center' >
                                    <h3>Ingresa tus datos</h3>
                                </div>
                                <p className='text-center' >
                                    Son los mismos que usas para entrar a la APP de amiPASS.
                                </p>
                                <Form onSubmit={this.login} action={"#"} method={'POST'}>
                                    <Form.Group>
                                        <input 
                                        required
                                        type="text" 
                                        placeholder="Usuario RUT o NN"
                                        value={this.state.username}
                                        autoComplete="new-password"
                                        className='form-control'
                                        onChange={ e => {
                                            let value = e.target.value;
                                            this.setState({ username: value.toUpperCase().replace(/-/, '') })
                                        } } />
                                    </Form.Group>
                                    <Form.Group>
                                        <input
                                        required 
                                        type="password"
                                        autoComplete="new-password"
                                        className='form-control'
                                        placeholder="Clave de internet" 
                                        onChange={ e => this.setState({ password: e.target.value }) }
                                        />
                                    </Form.Group>
                                    <Form.Group controlId="formBasicCheckbox">
                                        <Form.Check value={this.state.remember} onChange={() => this.setState({ remember: !this.state.remember })} type="checkbox" label="Registrar cuenta automáticamente" />
                                    </Form.Group>
                                    <Button variant="primary" type="submit" block disabled={this.state.isLoading}>
                                        {this.state.isLoading ? 'Iniciando sesión...' : 'Ingresar'}
                                    </Button>
                                </Form>
                            </div>
                        </div>
                    </div>
                    <ModalComponent
                        show={this.state.msgModal} 
                        handleClose={() => this.setState({ msgModal: false })}
                        title={this.state.msgTitle}
                    >
                        <p>{this.state.msgText}</p>
                    </ModalComponent>
                </div>
            </section>
        )
    }
}
