import React, { Component } from 'react'
import { connect } from 'react-redux';
import commerceInfoReducerAction from '../redux/actions/commerceInfoReducerAction';
import countItemCarReducerAction from '../redux/actions/countItemCarReducerAction';
import itemsCarReducerAction from '../redux/actions/itemsCarReducerAction';
import PageComponent from '../component/PageComponent'
import { Image } from 'react-bootstrap';
import ProductFromListComponent from '../component/ProductFromListComponent';
import PreloadComponent from '../component/PreloadComponent';
import ModalComponent from '../component/system/ModalComponent';
import CurrencyFormat from 'react-currency-format';
import logo from './../assets/images/preload-comercio.svg'
import { getImageCover, SERVER_API } from './../utils/utils'
import CoverComponent from '../component/CoverComponent';

import { database } from '../utils/firebase'

class CommercePage extends Component {
    constructor(props){
        super(props)
        this.state =  {
            msgShow: false,
            msgText: "",
            msgTitle: "",
            commerce: {},
            communesDeliveries: [],
            products: [],
            productsFiltered: [],
            productsByCategories: [],
            productsByCategoriesFiltered: [],
            productCategorySelected: '',
            productTextFiltered: '',
            categories: [],
            categoriesFiltered: [],
            brands: [],
            car: [],
            delivery: {},
            withdrawal: {},
            modalZone: false,
            logoError: false,
            msgCloseCommerce: false,
            loadingProducts: true,
            showModalProductsWithoutStock: false,
            modalProductStockAcceptDisable: true,
            productsWithoutStock: [],
            productsSoldOut: []
        }
    }

    componentWillMount() {
        let { commerce } = this.props.match.params;
        let statusResponseCommerce = 0
        fetch(`${SERVER_API}/commerce/show/name/${commerce}`, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
        }).then(async response => {
            statusResponseCommerce = response.status
            return await response.json()
        }).then(async response => {
            switch (statusResponseCommerce) {
                case 200:
                    let { commerce, commerceConfig } = response;
                    let { commerceDayDelivery, ecommerceConfig, communesDeliveries } = commerceConfig;
                    
                    fetch(`${SERVER_API}/commerce/state/${commerce.internalID}`, {
                        method: 'GET',
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json',
                        },
                    }).then(async response => {
                        return await response.json()
                    }).then(response => {
                        let { isOpen } = response
                        if(!isOpen) {
                            this.setState({
                                msgCloseCommerce: true,
                            })
                        }
                    })
                    
                    this.setState({ 
                        commerce: {
                            ...commerce,
                            ecommerceName: ecommerceConfig.ecommerceName
                        }, 
                        communesDeliveries 
                    });
                    this.setState({
                        cover: getImageCover(commerce.storeCategory)
                    })
                    if(commerceDayDelivery !== null) {
                        this.setState({
                            delivery: {
                                days: {
                                    monday: commerceDayDelivery.day1,
                                    tuesday: commerceDayDelivery.day2,
                                    wednesday: commerceDayDelivery.day3,
                                    thursday: commerceDayDelivery.day4,
                                    friday: commerceDayDelivery.day5,
                                    saturday: commerceDayDelivery.day6,
                                    sunday: commerceDayDelivery.day7,
                                },
                                timeStart: ecommerceConfig.deliveryStartTime,
                                timeEnd: ecommerceConfig.deliveryEndTime,
                                price: ecommerceConfig.deliveryPrice
                            },
                            withdrawal: {
                                timeStart: ecommerceConfig.withdrawalStartAt,
                                timeEnd: ecommerceConfig.withdrawalEndAt,
                                pickUpReference: ecommerceConfig.pickUpReference
                            }
                        })
                    }
                    this.props.commerceInfoReducerAction({ 
                        ...commerce, 
                        config: commerceConfig,
                        ecommerceName: ecommerceConfig.ecommerceName
                    })

                    this.getProducts(commerce.internalID)
                    break;
                default:
                    this.setState({
                        msgModal: true,
                        msgTitle: "Error inesperado",
                        msgText: `Hubo un error inesperado al intentar de mostrar los datos del comercio. Por favor intente más tarde. COD (${statusResponseCommerce})`
                    })
                    break;
            }
        }).catch(error => {
            console.log(error)
            this.setState({
                msgModal: true,
                msgTitle: "Error",
                msgText: `Hubo un error al intentar de cargar los datos del comercio. Por favor intente más tarde. COD (${statusResponseCommerce})`
            })
        })
    }

    componentWillUpdate(nextProps, nextState) {
        if(nextProps.shoppingCarMenuReducer.items.length !== this.props.shoppingCarMenuReducer.items.length) {
            let newCar = nextProps.shoppingCarMenuReducer.items
            this.setState({
                car: newCar
            })
            localStorage.setItem('car', JSON.stringify(newCar));
            this.props.itemsCarReducerAction(newCar)


            this.state.products.map(item => {
                let thisProductWasMod = newCar.filter(product => {
                    return product.id === item.id
                })
                if(thisProductWasMod[0]) {
                    return thisProductWasMod[0]
                }
                item.quantity = 0
                return item
            })

            console.log({
                nextProps,
                nextState,
            })
        }
    }

    getProducts = (commerce) => {
        let statusResponseProducts = 0
        fetch(`${SERVER_API}/commerce/product/${commerce}`, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
        }).then(async response => {
            statusResponseProducts = response.status
            return await response.json();
        }).then(async response => {
            switch (statusResponseProducts) {
                case 200:
                    let products = response
                    let car = [];
                    let user = null
                    if(localStorage.getItem('userTemp')) {
                        user = {
                            username: localStorage.getItem('userTemp')
                        }
                    } else {
                        user = JSON.parse(localStorage.getItem('user'));
                    }

                    if(user) {
                        let cart = await database
                        .collection(`${user.username}`)
                        .doc(`cart-${this.state.commerce.commerceID}${this.state.commerce.storeID}`)
                        .get()

                        if(cart.data()) {
                            if(cart.data().items.length > 0) {
                                let count = 0
                                let amount = 0
                                cart.data().items.map(item => {
                                    count += item.quantity
                                    amount += item.saleAmount * item.quantity
                                    return car.push(item)
                                })
                                this.props.countItemCarReducerAction({ 
                                    count: count, 
                                    amount: amount, 
                                    items: car 
                                })
                            } else {
                                await database
                                .collection(`${user.username}`)
                                .doc(`cart-${this.state.commerce.commerceID}${this.state.commerce.storeID}`)
                                .delete()
                            }
                        }
                    }

                    let productsWithoutStock = []
                    let productsSoldOut = []
                    let inCar = []
                    let productsInCar = []

                    let items = products.filter(item => {
                        return item.productPrices.length > 0
                    }).map(element => {
                        let { product, productPrices, commerceStock, productCategories, productBrand } =  element
                        if(productPrices.length === 0) return false;

                        if(car.length > 0) {
                            inCar = car.filter(item => {
                                if(item.id === product.id & item.quantity > commerceStock.stock) {
                                    if(commerceStock.stock === 0) {
                                        productsSoldOut.push(item)
                                    } else {
                                        productsWithoutStock.push(item)
                                    }
                                }
                                if(commerceStock.stock > 0) {
                                    return item.id === product.id
                                }
                                return false
                            })
                            if(inCar.length > 0) {
                                productsInCar.push(inCar[0])
                            }
                        }

                        if(productsWithoutStock.length > 0) {
                            this.setState({ 
                                productsWithoutStock,
                                productsSoldOut,
                                showModalProductsWithoutStock: true, 
                                modalProductStockAcceptDisable: (productsWithoutStock.length > 0 && productsSoldOut.length > 0) || !(productsWithoutStock.length === 0 && productsSoldOut.length > 0)
                            })
                        }

                        let price = productPrices.filter(item => item.priceTypeID === 1)[0]
                        
                        return {
                            id: product.id,
                            productName: product.productName,
                            fullDescription: product.fullDescription,
                            shortDescription: product.shortDescription,
                            category: productCategories.name,
                            brand: productBrand.name,
                            photoURL: product.photoURL,
                            saleAmount: price.saleAmount,
                            stk: commerceStock.stock,
                            stkMax: commerceStock.maximunStock,
                            stkMin: commerceStock.minimunStockForEcommerce,
                            quantity: (inCar.length > 0) ? inCar[0].quantity : 0,
                            food: product.food,
                            comment: ''
                        }
                    });
                    this.setState({ products: items, loadingProducts: false });

                    let productsByCategories = items.reduce((r, a) => {
                        let index = a.category.charAt(0).toUpperCase() + a.category.slice(1)
                        r[index] = [...r[index] || [], a]
                        return r
                    }, {})

                    let categories = Object.keys(productsByCategories).sort();

                    let brandsByProducts = items.reduce((r, a) => {
                        let index = a.brand.charAt(0).toUpperCase() + a.brand.slice(1)
                        r[index] = [...r[index] || [], a]
                        return r
                    }, {})
                    
                    let brands = Object.keys(brandsByProducts).sort();
                    
                    if(productsSoldOut.length > 0) {
                        let newDataCart = []
                        let newCart = [];
                        car.map(item => {
                            let productInStock = productsSoldOut.filter(a => a.id === item.id)
                            if(productInStock.length === 0) {
                                return newDataCart.push(item)
                            }
                            return false
                        })

                        let count = 0
                        let amount = 0
                        newDataCart.map(item => {
                            count += item.quantity
                            amount += item.saleAmount * item.quantity
                            return newCart.push(item)
                        })
                        this.props.countItemCarReducerAction({ 
                            count: count, 
                            amount: amount, 
                            items: newCart 
                        })

                        car = newCart

                        const cart = await database
                        .collection(`${user.username}`)
                        .doc(`cart-${this.state.commerce.commerceID}${this.state.commerce.storeID}`)
                        
                        cart.set({
                            commerce: this.state.commerce.commerceID,
                            store: this.state.commerce.storeID,
                            commerceAlias: this.state.commerce.ecommerceName,
                            items: newCart
                        })
                    }

                    this.setState({
                        productsByCategories,
                        categories,
                        brands,
                        car
                    })

                    localStorage.setItem('car', JSON.stringify(car))
                    
                    break;
                default:
                    this.setState({
                        msgModal: true,
                        msgTitle: "Error inesperado",
                        msgText: `Hubo un error inesperado al intentar de mostrar productos del comercio. Por favor intente más tarde. COD (${statusResponseProducts})`
                    })
                    break;
            }
        }).catch(error => {
            console.log(error)
            this.setState({
                msgModal: true,
                msgTitle: "Error",
                msgText: `Hubo un error al intentar de cargar los productos. Por favor intente más tarde. COD (${statusResponseProducts})`
            })
        })
    }

    addProduct = async (item) => {
        let categories = this.state.categories;
        let data = this.state.productsByCategories;
        let newData = []
        let countItem = this.props.shoppingCarMenuReducer.count
        let oldAmount = this.props.shoppingCarMenuReducer.amount
        let newAmount = 0
        categories.map(cat => {
            return data[cat].map(product => {
                if(product.id === item.id) {
                    if(item.quantity === item.stk) return newData.push(product)
                    let quantityOld = product.quantity
                    product.quantity++
                    countItem = countItem + 1
                    newAmount = (product.quantity === 1) ? (product.saleAmount * product.quantity) : ((product.saleAmount * product.quantity) - (product.saleAmount * quantityOld))
                    return newData.push(product)
                }
                return newData.push(product)
            })
        })

        let productsByCategories = newData.reduce((r, a) => {
            r[a.category] = [...r[a.category] || [], a]
            return r
        }, {})

        let itemsCar = newData.filter(item => item.quantity > 0)

        let productsWithoutStock = []
        this.state.productsWithoutStock.map(itemStock => {
            let newProd = newData.filter(a => a.id === itemStock.id)[0]
            return productsWithoutStock.push(newProd)
        })
        this.setState({ 
            productsWithoutStock,
            modalProductStockAcceptDisable: !(typeof productsWithoutStock.filter(a => a.quantity > a.stk)[0] === 'undefined')
        })
        
        this.props.countItemCarReducerAction({ 
            count: countItem, 
            amount: (oldAmount + newAmount), 
            items: itemsCar 
        })
        this.props.itemsCarReducerAction(itemsCar)
        
        this.setState({ productsByCategories })
        this.setState({ car: itemsCar, productsWithoutStock: itemsCar })
        localStorage.setItem('car', JSON.stringify(itemsCar))

        let user = null
        if(localStorage.getItem('userTemp')) {
            user = {
                username: localStorage.getItem('userTemp')
            }
        } else {
            user = JSON.parse(localStorage.getItem('user'));
        }
        if(user) {
            const cart = await database
            .collection(`${user.username}`)
            .doc(`cart-${this.state.commerce.commerceID}${this.state.commerce.storeID}`)
            
            cart.set({
                commerce: this.state.commerce.commerceID,
                store: this.state.commerce.storeID,
                commerceAlias: this.state.commerce.ecommerceName,
                items: itemsCar
            })
        }
        window.dataLayer.push({'event': 'addProduct'})
        if(itemsCar.length === 1) {
            window.dataLayer.push({'event': 'cartOneProduct'})
        }
    }

    removeProduct = async (item) => {
        let categories = this.state.categories;
        let data = this.state.productsByCategories;
        let newData = []
        let countItem = this.props.shoppingCarMenuReducer.count
        let oldAmount = this.props.shoppingCarMenuReducer.amount
        let newAmount = 0
        categories.map(cat => {
            return data[cat].map(product => {
                if(product.id === item.id) {
                    if(item.quantity === 0) {
                        newAmount -= product.saleAmount
                        return newData.push(product)
                    }
                    let quantityOld = product.quantity
                    product.quantity--
                    countItem = countItem - 1
                    newAmount = (product.saleAmount * quantityOld) - (product.saleAmount * product.quantity)
                    return newData.push(product)
                }
                return newData.push(product)
            })
        })

        let productsByCategories = newData.reduce((r, a) => {
            r[a.category] = [...r[a.category] || [], a]
            return r
        }, {})

        let itemsCar = newData.filter(item => item.quantity > 0)

        let productsWithoutStock = []
        this.state.productsWithoutStock.map(itemStock => {
            let newProd = newData.filter(a => a.id === itemStock.id)[0]
            return productsWithoutStock.push(newProd)
        })
        this.setState({ 
            productsWithoutStock,
            modalProductStockAcceptDisable: !(typeof productsWithoutStock.filter(a => a.quantity > a.stk)[0] === 'undefined')
        })
        
        this.props.countItemCarReducerAction({ 
            count: countItem,
            amount: (oldAmount - newAmount), 
            items: itemsCar 
        })
        this.props.itemsCarReducerAction(itemsCar)
        
        this.setState({ productsByCategories })
        this.setState({ car: itemsCar })
        localStorage.setItem('car', JSON.stringify(itemsCar))

        let user = null
        if(localStorage.getItem('userTemp')) {
            user = {
                username: localStorage.getItem('userTemp')
            }
        } else {
            user = JSON.parse(localStorage.getItem('user'));
        }
        if(user) {
            const cart = await database
            .collection(`${user.username}`)
            .doc(`cart-${this.state.commerce.commerceID}${this.state.commerce.storeID}`)
            
            cart.set({
                commerce: this.state.commerce.commerceID,
                store: this.state.commerce.storeID,
                commerceAlias: this.state.commerce.ecommerceName,
                items: itemsCar
            })

            if(itemsCar.length === 0) {
                await database
                .collection(`${user.username}`)
                .doc(`cart-${this.state.commerce.commerceID}${this.state.commerce.storeID}`)
                .delete()
            }
        }
    }
    titleCase = (string) => {
        let str = string.toLowerCase().split(" ");
        for(let i = 0; i< str.length; i++){
           str[i] = str[i].charAt(0).toUpperCase() + str[i].slice(1) + " ";
        }
        return str
    }

    searchFilterByName = (text) => {
        let products = this.state.products.filter(item => {
            const itemData = `${item.productName}`.toUpperCase()
            const textComparer = text.toUpperCase()
            return itemData.indexOf(textComparer) > -1
        })

        let productsByCategories = products.reduce((r, a) => {
            let index = a.category
            r[index] = [...r[index] || [], a]
            return r
        }, {})

        let categories = Object.keys(productsByCategories).sort();

        this.setState({
            categoriesFiltered: categories,
            productsByCategoriesFiltered: productsByCategories,
            productsFiltered: products
        })
    }

    searchFilterByCategories = (text) => {
        let products = this.state.products.filter(item => {
            const itemData = `${item.category}`.toUpperCase()
            const textComparer = text.toUpperCase()
            return itemData.indexOf(textComparer) > -1
        })

        let productsByCategories = products.reduce((r, a) => {
            let index = a.category
            r[index] = [...r[index] || [], a]
            return r
        }, {})

        let categories = Object.keys(productsByCategories).sort();

        this.setState({
            categoriesFiltered: categories,
            productsByCategoriesFiltered: productsByCategories,
            productsFiltered: products
        })
    }

    searchFilterByBrads = (text) => {
        let products = this.state.products.filter(item => {
            const itemData = `${item.brand}`.toUpperCase()
            const textComparer = text.toUpperCase()
            return itemData.indexOf(textComparer) > -1
        })

        let productsByCategories = products.reduce((r, a) => {
            let index = a.category
            r[index] = [...r[index] || [], a]
            return r
        }, {})

        let categories = Object.keys(productsByCategories).sort();

        this.setState({
            categoriesFiltered: categories,
            productsByCategoriesFiltered: productsByCategories,
            productsFiltered: products
        })
    }

    render() {
        let days = {
            ...this.state.delivery.days
        }
        
        return (
            <PageComponent>
                <CoverComponent
                    cover={`/covers/${this.state.cover}.jpg`}
                    overlay={true}
                >
                    <>
                        {
                            (typeof this.state.commerce.commerceID == 'undefined') ? 
                                <PreloadComponent/>
                            :
                            <div className='d-flex bd-highlight p-relative' style={{ color: '#fff' }} >
                                <div className='row w-100 w-100-m'>
                                    <div className='col-lg-2 col-md-12 img-business' >
                                        <div className='center-m p-b-15'>
                                            <Image 
                                                className='rounded-circle' 
                                                width={100} 
                                                height={'auto'} 
                                                src={(this.state.logoError) ? logo : `https://apired.amipass.cl/commerces/${this.state.commerce.commerceID}/${this.state.commerce.storeID}/logo.png`}
                                                onError={() => {
                                                    this.setState({ logoError: true })
                                                }}
                                            />
                                        </div>
                                    </div>
                                    <div className='align-self-center center-m col' >
                                        <h4 style={{ color: '#fff' }} >{this.titleCase(this.state.commerce.storeName)}</h4>
                                        <div>
                                            {
                                                this.state.commerce.hasDelivery && (<><span className="badge badge-pill badge-warning tag-delivery">Delivery</span>{' '}</>)
                                            }
                                            {
                                                this.state.commerce.hasWithdrawal && (<><span className="badge badge-pill badge-info tag-retiro-local">Retiro Local</span>{' '}</>)
                                            }
                                        </div>
                                        <div>
                                            <small>{this.titleCase(`${this.state.commerce.localAddress}, ${this.state.commerce.commune}, ${this.state.commerce.city}`)}</small> <br/>
                                            <small>{this.state.commerce.storeCategory}</small>{'       ' } 
                                            {
                                                (this.state.delivery.price > 0) &&
                                                <small>
                                                    <strong>Monto mínimo para despacho gratis: </strong>
                                                    {
                                                        this.state.delivery.price === null ? <><span className='text-muted' >Sin establecer</span></>
                                                        : (<CurrencyFormat 
                                                            value={this.state.delivery.price}
                                                            displayType={'text'} 
                                                            thousandSeparator={true} 
                                                            prefix={'$'}
                                                            renderText={value => (
                                                                <span>{value.replace(/,/g, '.')}</span>
                                                            )}
                                                        />)
                                                    }
                                                </small>
                                            }
                                        </div>
                                        <div className='d-flex justify-content-start align-items-center'>
                                            <small><strong>Hora de atención: </strong></small>
                                            { (typeof this.state.delivery.days == 'undefined') ? (<><small>Sin horarios establecidos</small> </>) : 
                                                <>
                                                    <span className={((days) && (days.monday)) ? 'tag-day tag-day-available' : 'tag-day tag-day-disable' } ><small>L</small></span>
                                                    <span className={((days) && (days.tuesday)) ? 'tag-day tag-day-available' : 'tag-day tag-day-disable' } ><small>M</small></span>
                                                    <span className={((days) && (days.wednesday)) ? 'tag-day tag-day-available' : 'tag-day tag-day-disable' } ><small>M</small></span>
                                                    <span className={((days) && (days.thursday)) ? 'tag-day tag-day-available' : 'tag-day tag-day-disable' } ><small>J</small></span>
                                                    <span className={((days) && (days.friday)) ? 'tag-day tag-day-available' : 'tag-day tag-day-disable' } ><small>V</small></span>
                                                    <span className={((days) && (days.saturday)) ? 'tag-day tag-day-available' : 'tag-day tag-day-disable' } ><small>S</small></span>
                                                    <span className={((days) && (days.sunday)) ? 'tag-day tag-day-available' : 'tag-day tag-day-disable' } ><small>D</small></span>
                                                </>
                                            }
                                            {
                                            ((this.state.withdrawal.timeStart === null || this.state.withdrawal.timeStart === "" || typeof this.state.withdrawal.timeStart == 'undefined') | (this.state.withdrawal.timeEnd === null || this.state.withdrawal.timeEnd === "" || typeof this.state.withdrawal.timeEnd == 'undefined')) ? <> <small>24 Horas</small></>
                                                : (<span><small className='timestart'>{ this.state.withdrawal.timeStart }</small><strong> a </strong><small>{ this.state.withdrawal.timeEnd }</small></span>)
                                            }
                                        </div>
                                        <div>
                                            <small> <strong>Horario de Retiro en Local: </strong> {
                                            ((this.state.withdrawal.pickUpReference === null || this.state.withdrawal.pickUpReference === "" || typeof this.state.withdrawal.pickUpReference == 'undefined')) ? <>Sin especificar</>
                                            : (<> {this.state.withdrawal.pickUpReference}</>)
                                            }</small> 
                                        </div>
                                        <div>
                                            {
                                                this.state.commerce.hasDelivery && 
                                                    <button 
                                                        className='btn btn-primary btn-sm btn-zone-delivery' 
                                                        onClick={() => {
                                                            window.dataLayer.push({'event': 'clickDispatchArea'})
                                                            this.setState({ modalZone: true })
                                                        }}
                                                    >Zona de despacho</button>
                                            }
                                        </div>
                                    </div>
                                </div>
                            </div>
                        }
                    </>
                </CoverComponent>
                <div className='container my-5 h-100' >
                    <div className='row' >
                        <div className='col-md-3' >
                            <div className='row' >
                                <div className='col' >
                                    <h6>Buscador</h6>
                                    <input
                                        className='form-control'
                                        placeholder="Ej. Bebida"
                                        onBlur={() => {
                                            window.dataLayer.push({
                                                event: 'filterProductSearch',
                                                labelName: this.state.productTextFiltered,
                                            })
                                        }}
                                        onChange={(e) => {
                                            this.setState({ productTextFiltered: e.target.value })
                                            this.searchFilterByName(e.target.value)
                                        }}
                                    />
                                </div>
                            </div>
                            <div className='row mt-4' >
                                <div className='col' >
                                    <h6>Filtros</h6>
                                    <div className='form-group' >
                                        <select className='form-control' onChange={(e) => {
                                            window.dataLayer.push({
                                                event: 'filterProductCategories',
                                                selectedName: e.target.value,
                                            })
                                            this.searchFilterByCategories(e.target.value)
                                        }} >
                                            <option>Todas las Categorías</option>
                                            {
                                                this.state.categories.map((item, index) => (
                                                    <option key={index} value={item} >{ item }</option>
                                                ))
                                            }
                                        </select>
                                    </div>
                                    <div className='form-group' >
                                        <select className='form-control' onChange={(e) => {
                                            window.dataLayer.push({
                                                event: 'filterProductBrand',
                                                selectedName: e.target.value,
                                            })
                                            this.searchFilterByBrads(e.target.value)
                                        }} >
                                            <option>Todas las Marcas</option>
                                            {
                                                this.state.brands.map((item, index) => (
                                                    <option key={index} value={item} >{ item }</option>
                                                ))
                                            }
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className='col-md-9 p-l-r-30' >
                            {
                                this.state.loadingProducts ? 
                                    <PreloadComponent/>
                                :
                                <>
                                {
                                    (this.state.categories.length === 0) ?
                                    <div className='text-center'>
                                        <h3>No hay productos para mostrar</h3>
                                    </div>
                                    :
                                    (this.state.categoriesFiltered.length > 0) ?
                                    this.state.categoriesFiltered.map((item, index) => (
                                        <div className='row' key={index} >
                                            <div className='col'>
                                                <div className='row'>
                                                    <div className='col border-bottom' >
                                                        <h6>{item}</h6>
                                                    </div>
                                                </div>
                                                <div className="row row-cols-1 row-cols-md-2 py-4">
                                                    {
                                                        this.state.productsByCategoriesFiltered[item].map(item => (
                                                            <ProductFromListComponent 
                                                                controls={true}
                                                                key={item.id}
                                                                item={item} 
                                                                removeProduct={() => this.removeProduct(item)}
                                                                addProduct={() => this.addProduct(item)}
                                                            />
                                                        ))
                                                    }
                                                </div>
                                            </div>
                                        </div>
                                    )) :
                                    this.state.categories.map((item, index) => (
                                        <div className='row' key={index} >
                                            <div className='col'>
                                                <div className='row'>
                                                    <div className='col border-bottom' >
                                                        <h6>{item}</h6>
                                                    </div>
                                                </div>
                                                <div className="row row-cols-1 row-cols-md-2 py-4">
                                                    {
                                                        this.state.productsByCategories[item].map(item => (
                                                            <ProductFromListComponent
                                                                controls={true}
                                                                key={item.id}
                                                                item={item} 
                                                                removeProduct={() => this.removeProduct(item)}
                                                                addProduct={() => this.addProduct(item)}
                                                            />
                                                        ))
                                                    }
                                                </div>
                                            </div>
                                        </div>
                                    ))
                                }
                                </>
                            }
                        </div>
                    </div>
                </div>
                <ModalComponent
                    show={this.state.msgModal} 
                    handleClose={() => this.setState({ msgModal:false })}
                    title={this.state.msgTitle}
                >
                    <p>{this.state.msgText}</p>
                </ModalComponent>
                <ModalComponent
                    show={this.state.modalZone} 
                    handleClose={() => this.setState({ modalZone:false })}
                    title='Cobertura de despacho'
                    size={'xl'}
                >
                    <div className="accordion" id='accordionDeliveryZone' >
                        {
                            this.state.communesDeliveries.map((item,i) => (
                                <div className='card' key={item.id} >
                                    <div className="card-header" id={`heading${item.id}`}>
                                        <div className="btn btn-block" data-toggle="collapse" data-target={`#collapse${item.id}`} aria-expanded={(i===0)?'true':'false'} aria-controls={`collapse-${item.id}`}>
                                            <div className="d-flex bd-highlight">
                                                <div className="p-2 flex-grow-1 bd-highlight text-left">
                                                    <div>
                                                        <h5 className='zone-region-text'>{(item.regionName) ? item.regionName : 'Sin especificar'}</h5>
                                                    </div>
                                                    <div>
                                                        <span className='zone-commune-text'>{(item.communeName) ? item.communeName : 'Sin especificar'}</span>
                                                    </div>
                                                </div>
                                                <div className="p-2 bd-highlight text-center">
                                                    <div>
                                                        <h5 className='zone-delivery-text'>Delivery</h5>
                                                    </div>
                                                    {
                                                        item.communeDeliveryPrice > 0 ?
                                                        <CurrencyFormat 
                                                            value={item.communeDeliveryPrice}
                                                            displayType={'text'} 
                                                            thousandSeparator={true} 
                                                            prefix={'$'}
                                                            renderText={value => (
                                                                <span className='zone-delivery-amount'>{value.replace(/,/g, '.')}</span>
                                                            )}
                                                        /> :
                                                        <span>Gratis</span>
                                                    }
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id={`collapse${item.id}`} className={`collapse ${(i===0)?'show':''}`} aria-labelledby={`heading${item.id}`} data-parent="#accordionDeliveryZone">
                                        <div className="card-body">
                                            <div className="row text-center">
                                                <div className='col-xl-2'>
                                                    <h5 className='zone-title-text'>Región</h5>
                                                    <span className='zone-description-text'>{(item.regionName) ? item.regionName : 'Sin especificar'}</span>
                                                </div>
                                                <div className='col-xl-2'>
                                                    <h5 className='zone-title-text'>Comuna</h5>
                                                    <span className='zone-description-text'>{(item.communeName) ? item.communeName : 'Sin especificar'}</span>
                                                </div>
                                                <div className='col-xl-2'>
                                                    <h5 className='zone-title-text'>Tiempo de entrega</h5>
                                                    <span className='zone-description-text'>{(item.communeDeliveryTime) ? item.communeDeliveryTime : 'Sin especificar'}</span>
                                                </div>
                                                <div className='col-xl-4'>
                                                    <h5 className='zone-title-text'>Referencia</h5>
                                                    <span className='zone-description-text'>{(item.communeReference) ? item.communeReference : 'Sin especificar'}</span>
                                                </div>
                                                <div className='col-xl-2'>
                                                    <h5 className='zone-title-text'>Costo Delivery</h5>
                                                    <div>
                                                        {
                                                            item.communeDeliveryPrice > 0 ?
                                                            <CurrencyFormat 
                                                                value={item.communeDeliveryPrice}
                                                                displayType={'text'} 
                                                                thousandSeparator={true} 
                                                                prefix={'$'}
                                                                renderText={value => (
                                                                    <span>{value.replace(/,/g, '.')}</span>
                                                                )}
                                                            /> :
                                                            <span className='zone-description-text'>Gratis</span>
                                                        }
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            ))
                        }
                    </div>
                </ModalComponent>
                <ModalComponent
                    show={this.state.msgCloseCommerce} 
                    handleAccept={() => {
                        this.setState({ msgCloseCommerce: false })
                    }}
                    title={'Comercio cerrado'}
                >
                    <p>Este comercio se encuentra cerrado. Puede ver los productos disponible pero no podrá comprar.</p>
                </ModalComponent>
                <ModalComponent
                    show={this.state.showModalProductsWithoutStock} 
                    handleAccept={() => {
                        this.setState({
                            showModalProductsWithoutStock: false
                        })
                    }}
                    acceptDisabled={this.state.modalProductStockAcceptDisable}
                    textAccept='Continuar'
                    title={'Cantidad no disponible'}
                >
                    <p>Los siguientes productos superan la cantidad disponible para efectuar tu compra. 
                        Disminuya la cantidad para continuar.</p>
                    <div>
                        {
                            this.state.productsWithoutStock.map(item => {
                                return <ProductFromListComponent 
                                    controls={true}
                                    key={item.id} 
                                    item={item} 
                                    addProduct={() => this.addProduct(item)}
                                    removeProduct={() => this.removeProduct(item)}
                                />
                            })
                        }
                    </div>
                    {
                        this.state.productsSoldOut.length > 0 &&
                        <>
                            <hr/>
                            <p>Estos productos se encuentran agotados</p>
                            <div>
                                {
                                    this.state.productsSoldOut.map(item => {
                                        return <ProductFromListComponent 
                                            controls={true}
                                            key={item.id} 
                                            item={item} 
                                            addProduct={() => this.addProduct(item)}
                                            removeProduct={() => this.removeProduct(item)}
                                        />
                                    })
                                }
                            </div>
                        </>
                    }
                </ModalComponent>
            </PageComponent>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        shoppingCarMenuReducer: state.shoppingCarMenuReducer,
        commerceReducer: state.commerceReducer
    }
}

const mapDispatchToProps = {
    commerceInfoReducerAction,
    countItemCarReducerAction,
    itemsCarReducerAction
}

export default connect(mapStateToProps, mapDispatchToProps)(CommercePage)