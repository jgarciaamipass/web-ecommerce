import React, { Component } from 'react'

export default class TermsConditions extends Component {
    render() {
        return (
            <div className='container-fluid mt-4' >
                <p>
                    Los presentes términos y condiciones (en adelante, los “Términos y Condiciones”),
                    constituyen las normas y directrices que regulan el acceso y uso que los Consumidores de
                    la Plataforma Virtual AmiMARKET, en adelante “AmiMARKET”, realicen o tengan efecto en
                    Chile. Esta Plataforma se encuentra a disposición en el sitio web www.amipass.com,
                    gestionada por FDD INNOVACION Y CRECIMIENTO S.A, en adelante “AMIPASS”, con
                    domicilio legal en Av. Raúl Labbé 12613, Oficina 226, Comuna de Lo Barnechea, Santiago
                    de Chile.
                </p>
                <p>
                    AmiMARKET ofrece principalmente los siguientes servicios:
                    <ol type='i' >
                        <li>
                            Exhibición de diferentes productos y servicios de consumo de forma
                            publicitaria de comercios afiliados a AMIPASS (en adelante, “Comercios”) para
                            que los Consumidores puedan adquirirlos a través del servicio de despacho o
                            retiro en local.
                        </li>
                        <li>
                            Facilitación de un punto de encuentro entre Consumidores y Comercios.
                        </li>
                        <li>
                            Sistema de pago del valor del producto
                        </li>
                        <li>
                            Servicios Publicitarios y de Gestión de Datos de acuerdo a lo señalado más adelante.
                        </li>
                    </ol>
                </p>
                <p>
                    Respecto a los servicios descritos y a los demás que ofrezca, AMIPASS es enfático en
                    señalar que éstos constituyen un canal de encuentro entre los Consumidores y los
                    Comercios, y la gestión de transacciones de pago de aquellos Productos que sean
                    requeridos por los Consumidores. Al respecto, AMIPASS no es dueño de los Productos
                    ofrecidos ni de sus mecanismos de publicidad, sino que ofrece vías de publicidad y
                    comunicación para que los productos de terceros ajenos a AMIPASS puedan ser
                    visualizados por los Consumidores, y cuya gestión de compra y servicio de entrega está a
                    cargo del Vendedor. AmiMARKET permitirá a los Consumidores satisfacer una necesidad
                    de consumo personal y no con el objetivo de una posterior comercialización, reventa u
                    otro tipo de transacción comercial o interés con los productos adquiridos.
                </p>
                <p>
                    A continuación, se expondrán los Términos y Condiciones que regularán el uso de
                    AmiMARKET por parte de los Consumidores, quienes declaran en este acto conocer y
                    aceptar lo prescrito en el presente documento, a saber:
                </p>
                <h5>PRIMERO. Obligación de aceptación de los Términos y Condiciones.</h5>
                <p>
                    1.1 Estos Términos y Condiciones sustituyen expresamente cualquier acuerdo o
                    compromiso previo realizado entre el Consumidor y AMIPASS.
                </p>
                <p>
                    1.2 Estos Términos y Condiciones regulan el acceso y uso, por parte del Consumidor, de la
                    Plataforma para la compra de productos y servicios que ofrecen Comercios en
                    AmiMARKET.
                </p>
                <p>
                    1.3 Todo Consumidor, para poder acceder a AmiMARKET declara haber leído y aceptado
                    los presentes Términos y Condiciones, y haberse registrado con información real y
                    fidedigna en la Plataforma.
                </p>
                <p>
                    1.4 El Registro al que hace referencia el numeral anterior y que se describe más adelante,
                    supone expresamente que el Consumidor conoce y acepta estos Términos y Condiciones.
                    En ese sentido, el uso de la Plataforma como Consumidor registrado supone que usted
                    conoce y acepta la última versión de los Términos y Condiciones para los Consumidores,
                    cuyo documento actualizado y vigente se encontrará a disposición en la misma
                    Plataforma.
                </p>
                <p>
                    1.5 Al aceptar estos los Términos y Condiciones, usted acepta y reconoce que:
                    <ol type='i'>
                        <li>
                            AMIPASS no presta servicios de mensajería, logística ni transporte de ningún tipo.
                        </li>
                        <li>
                            Bajo ninguna circunstancia se considerará que los Servicios ofrecidos en
                            AmiMARKET son prestados por AMIPASS y, en razón de ello, tanto los
                            Comercios como los repartidores nunca podrán ser considerados empleados,
                            trabajadores ni representantes de AMIPASS.
                        </li>
                    </ol>
                </p>
                <p>
                    1.6 AMIPASS se reserva el derecho a poner fin de inmediato al vínculo con cada
                    Consumidor, así como dejar de ofrecer Servicios o denegar el acceso a uno o más
                    consumidores, ya sea en forma total o parcial, en caso de que éstos infrinjan de algún
                    modo los mismos.
                </p>
                <h5>SEGUNDO. El Registro.</h5>
                <p>
                    2.1 Para utilizar a la Plataforma como Consumidor, usted deberá registrarse como
                    Consumidor, completando el Formulario de Registro (el “Formulario”) con sus datos
                    personales tales como nombre, apellido, rut y dirección de correo electrónico. Sin
                    perjuicio de que el consumidor podrá proporcionar datos adicionales para completar su
                    perfil en la Plataforma dentro de la sección Perfil. El Consumidor declara que la
                    información suscrita en el Formulario es exacta, precisa y verdadera, y asume la obligación
                    de actualizar sus datos personales cada vez que ocurra una modificación de ellos.
                </p>
                <p>
                    2.2 Una vez completado el Registro, AMIPASS otorgará al Consumidor una cuenta
                    personal en AmiMARKET (en adelante, “la Cuenta”) con la cual podrá realizar sus pedidos
                    con la contraseña que dicho Consumidor elija. El Consumidor accederá a la Cuenta
                    mediante el ingreso de su correo electrónico y contraseña de seguridad personal elegida.
                    La Cuenta es de uso personal, única e intransferible, y está expresamente prohibido que
                    un mismo consumidor se registre más de una vez o posea más de una Cuenta. Asimismo,
                    queda prohibido para el Consumidor prestar o arrendar su cuenta a terceros para
                    cualquier fin. En caso de que AMIPASS detecte sospeche fundadamente que distintas
                    Cuentas contienen datos o usos que hacen presumir que se trate de una misma persona
                    con más de una cuenta, podrá cancelar, suspender o inhabilitar algunas o todas las
                    Cuentas relacionadas sin previo aviso, y sin que ello implique derecho a reclamo a alguno
                    por parte de los titulares de dichas Cuentas. El consumidor será el único responsable por
                    el cuidado y buen uso de su Cuenta, así como de mantener sus contraseñas de acceso y
                    pago protegidas, siendo de entero conocimiento del Consumidor que sus claves son
                    personales e intransferibles, por lo que de ser usadas de forma incorrecta es de su propia
                    responsabilidad. Todas las compras realizadas con las credenciales del consumidor se
                    considerarán válidas.
                </p>
                <p>
                    2.3 Los Datos Personales introducidos por el consumidor en el Registro deberán ser
                    exactos, actuales y veraces en todo momento. AMIPASS se reserva el derecho a solicitar,
                    en cualquier momento, algún comprobante y/o dato adicional a efectos de corroborar los
                    Datos Personales indicados en el Registro y de suspender, temporal y/o definitivamente, a
                    aquel Consumidor cuyos datos no coincidan con los registrados en la cuenta de dicho
                    Consumidor. No obstante lo anterior, AMIPASS no se responsabiliza por la veracidad de
                    los datos consignados en el Registro. En este sentido el Consumidor garantiza y responde,
                    ante cualquier caso, por la veracidad, exactitud, vigencia y autenticidad de sus Datos
                    Personales. Los Datos Personales que el Consumidor proporcione se integrarán en una
                    base de datos personales de la que es responsable AMIPASS, en caso de que el
                    Consumidor registrado tenga datos existentes en la base de datos AMIPASS estos serán
                    complementados a la información otorgada, teniendo sobre ellos el consumidor en todo
                    momento los derechos consagrados en la ley 19.628 sobre protección de la vida privada y
                    los demás términos del presente documento. Se deja constancia que los datos que sean
                    proporcionados por el consumidor podrán ser entregados a la autoridad correspondiente
                    en virtud de una resolución judicial o por expreso mandato de la entidad fiscalizadora
                    correspondiente.
                </p>
                <p>
                    2.4 AMIPASS, se reserva el derecho de dar de baja el Registro y/o Cuenta de un
                    determinado Consumidor en cualquier momento, de manera unilateral, ante cualquier
                    infracción a los presentes Términos y Condiciones.
                </p>
                <h5>TERCERO. El Servicios Ofrecidos.</h5>
                <p>
                    3.1 Los Servicios ofrecidos en AmiMARKET se encuentran dirigidos o destinados
                    exclusivamente a los consumidores registrados.
                </p>
                <p>
                    3.2 El Consumidor podrá encontrar en AmiMARKET, diversos productos ofrecidos por
                    distintos Comercios a un precio determinado. En caso de que el consumidor quiera
                    adquirir algún producto, podrá realizar la compra y solicitar el despacho del Producto al
                    domicilio que indique o el retiro en tienda dependiendo de las condiciones del Comercio
                    oferente y/o las preferencias del Consumidor.
                </p>
                <p>
                    3.3 El Consumidor podrá comprar más de un pedido por día, sin perjuicio de ello, se
                    podrán establecer en la Plataforma un número máximo de transacciones o compras,
                    dependiendo del medio de pago utilizado o del monto de ellas. AMIPASS podrá, ante
                    sospechas por reventa de Productos, la comisión de un ilícito o alguna infracción a los
                    presentes Términos y Condiciones, rechazar o cancelar algún pedido de los Consumidores
                    reservándose AMIPASS el derecho de reembolsar o no dependiendo del motivo de
                    cancelación.
                </p>
                <p>
                    3.4 A fin de una mejor experiencia de consumo, AmiMARKET permite que el Consumidor
                    indique el día, horario y lugar en donde quiere le sean entregados los Productos.
                </p>
                <p>
                    3.5 Cualquier cambio en el Servicio o Producto será informado al consumidor a través de
                    la Plataforma, al teléfono de contacto y/o al correo electrónico informado en su Registro.
                </p>
                <p>
                    3.6 Cuando por errores de carácter técnico o tecnológico se exhiban precios erróneos de
                    los productos en AmiMARKET de manera tal que estos resulten desproporcionados en
                    comparación con el precio del producto en el mercado o no cumplan con el requisito de la
                    seriedad en la oferta, AMIPASS podrá cancelar su compra a su libre discreción, con plena
                    autonomía y sin que ello genere derecho a reclamo alguno del Consumidor.
                </p>
                <p>
                    3.7 El Comercio cumplirá con el delivery o retiro de la compra entregando la mercadería
                    en la dirección consignada en los Detalles de Entrega o el retiro en el local, siendo el
                    Consumidor el único responsable por la veracidad de los datos allí introducidos. En caso
                    de que los datos sean incorrectos y el Consumidor no cancele el pedido en el período
                    establecido previo a la confirmación por parte del Comercio, el cobro se realizará
                    igualmente y sin derecho a reclamo.
                </p>
                <p>
                    3.8 Se encuentra totalmente prohibido a los Consumidores el uso de AmiMARKET con el
                    fin de transportar mercadería con fines ilícitos o contrarios a lo establecido en los
                    presentes Términos y Condiciones, a la buena fe, la moral y al orden público, sin limitación
                    alguna. AMIPASS se reserva el derecho de denunciar a la autoridad competente a aquellos
                    Consumidores que realicen esta clase de conductas.
                </p>
                <p>
                    3.9 El Consumidor entiende y acepta que AMIPASS no es productor, proveedor,
                    expendedor, agente, distribuidor y en general ningún tipo de comercializador de los
                    Productos que exhibe, ya que opera solo como una plataforma tecnológica que permite el
                    encuentro de Consumidores y Comercios para la gestión del servicio de compra a
                    domicilio o con retiro en local.
                </p>
                <p>
                    3.10 Los productos de uso restringido, tales como tabaco y bebidas alcohólicas, solo
                    podrán ser adquiridas por consumidores que cuenten con mayoría de edad. Para ello,
                    todo Consumidor deberá manifestar expresamente esta calidad al momento de
                    seleccionar el Producto. El Consumidor deberá presentar su cédula de identidad,
                    acreditando su mayoría de edad al momento en que se produzca la entrega del producto.
                    En caso de no cumplir con esta obligación, el despachador o persona que entregue los
                    productos en el local no entregará el producto al consumidor. Respecto a lo anterior, el
                    Consumidor es el único responsable por la veracidad y legalidad de la información que
                    provee a AMIPASS y a las personas encargadas del despacho o entrega de productos, así
                    como por los documentos de identidad que exhiba al momento de la entrega.
                </p>
                <p>
                    3.11 AMIPASS se reserva el derecho de actualizar, modificar y/o descontinuar los
                    productos exhibidos en AmiMARKET.
                </p>
                <h5>TERCERO. El Servicios Ofrecidos.</h5>
                <p>
                    4.1 El acceso a AmiMARKET es gratuito, salvo en lo relativo al costo de la conexión a través
                    de la red de telecomunicaciones suministrada por el proveedor de acceso contratado (ISP)
                    por el Consumidor, que será siempre de su exclusivo cargo.
                </p>
                <p>
                    4.2 El Consumidor es el único y exclusivo responsable de:
                    <ol type='i'>
                        <li>
                            El uso de su Cuenta.
                        </li>
                        <li>
                            La veracidad de los Datos Personales ingresados en el Registro; y;
                        </li>
                        <li>
                            La legalidad de la posesión y uso de los Productos que incluya en su Solicitud, sin
                            perjuicio de las responsabilidades del Comercio al publicarlo.
                        </li>
                    </ol>
                </p>
                <p>
                    4.3 El Consumidor se compromete a hacer un uso adecuado de la Plataforma de
                    conformidad con la legislación vigente, a los presentes Términos y Condiciones, la moral y
                    buenas costumbres.
                </p>
                <p>
                    4.4 El Consumidor reconoce que AMIPASS únicamente pone a su disposición un espacio
                    virtual que le permite ponerse en comunicación con un Comercio para realizar una
                    compra. En este sentido, el consumidor reconoce y acepta que existe un mandato entre el
                    repartidor y el comercio, respecto del despacho de los productos adquiridos mediante y
                    que AMIPASS no interviene en el perfeccionamiento de las operaciones realizadas entre el
                    repartidor y el Comercio, por lo que AMIPASS no será responsable respecto de la calidad,
                    cantidad, estado, integridad o legitimidad de la mercadería y/o productos que sean
                    transportados por el repartidor, así como tampoco será responsable de las condiciones
                    laborales del repartidor ni de la veracidad de los datos personales del Consumidor.
                </p>
                <p>
                    4.5 Durante el uso y solicitud de Productos y Servicios en AmiMARKET, el Consumidor estará obligado a utilizar su Cuenta para su uso personal e individual, sin facultades para ceder, cualquiera sea la forma, su Cuenta a un tercero, y a guardar de forma segura y confidencial la contraseña de su Cuenta y cualquier identificación facilitada para permitirle el acceso y uso de AmiMARKET.
                </p>
                <p>
                    4.6 Por el contrario, durante el uso y solicitud de Productos y Servicios en AmiMARKET, el Consumidor tendrá prohibido:
                    <ul>
                        <li>Autorizar a terceros a usar su Cuenta. De hacerlo, AMIPASS se reserva el derecho de cancelar su Registro y el Consumidor se hará completamente responsable de los actos que cualquier otra persona realice a su nombre y/o a través de su Cuenta, con o sin su consentimiento, liberando de toda responsabilidad al respecto a AMIPASS.</li>
                        <li>Ceder o transferir de cualquier modo su Cuenta a cualquier otra persona o entidad.</li>
                        <li>Solicitar el Servicio con fines ilícitos, contrarios a lo establecido en los presentes Términos y Condiciones, al orden público, la buena fe, la moral y/o las buenas costumbres incluyendo, sin limitación, el transporte de material ilegal o con fines fraudulentos.</li>
                        <li>Acceder y/o usar la Plataforma con un dispositivo incompatible o no autorizado por las autoridades correspondientes y que pueda significar un riesgo de carácter informático y de seguridad para la Plataforma.</li>
                        <li>Acceder, utilizar y/o manipular los datos de AMIPASS, otros Consumidores, Comercios y/o cualquier otro aliado de AMIPASS u otros consumidores.</li>
                        <li>No introducir ni difundir virus informáticos o de otra naturaleza que sean susceptibles de provocar daños en la Plataforma.</li>
                    </ul>
                </p>
                <h5>QUINTO. Condiciones de Pago.</h5>
                <p>5.1 Para efectos de adquirir el producto, AMIPASS abonará su precio al Comercio, más el valor del despacho en caso de haberlo, por cuenta y orden del Consumidor, descontando la comisión más IVA del servicio acordado previamente con el Comercio.</p>
                <p>5.2 El Consumidor podrá dar adicionalmente en efectivo la propina para el Repartidor que considere adecuada de acuerdo con el nivel del Servicio prestado.</p>
                <p>5.3 La tarifa de despacho será establecida por el Comercio en consideración a la oferta y demanda vigente, distancia de la entrega y cualquier otro factor que estime necesario para definirla, la que es aceptada por el Consumidor al momento de solicitar un Producto o Servicio. Asimismo, la tarifa será cobrada directamente por el Comercio, sobre el cual AMIPASS retendrá la comisión establecida previamente con el Comercio más el IVA correspondiente.</p>
                <p>5.4 AMIPASS se reserva el derecho de modificar, cambiar, agregar o eliminar la tarifa para cualquier Servicio según el convenio con cada Comercio, lo cual se verá reflejado automáticamente en la compra realizada en AmiMARKET. Asimismo, el consumidor entiende y acepta, al momento de realizar la compra, que los Comercios podrán rechazar el pedido ante falta de stock o precios mal publicados, sin perjuicio de que una vez confirmado el pedido se harán responsables de entregar el Producto o Servicio o asumir dicho costo.</p>
                <p>5.5 Finalizada la compra, AMIPASS enviará un correo electrónico al consumidor con el resumen de su compra, un detalle de los productos efectivamente cargados, fecha y hora de la compra y el Valor Total pagado, con los impuestos incluidos (en adelante, el “Resumen”).</p>
                <p>5.6 AMIPASS se reserva el derecho de tomar las medidas judiciales y extrajudiciales que estime pertinentes para obtener el pago del valor total que adeude el Consumidor.</p>
                <h5>SEXTA. Cancelaciones, Devoluciones y Penalidades.</h5>
                <p>6.1. Una vez emitida la compra, el Consumidor podrá cancelarla sin penalidad alguna, siempre que realice esta cancelación a través de AmiMARKET y antes de que el Comercio ya haya confirmado la orden.</p>
                <p>6.2 El plazo de devolución del dinero de la compra cancelada bajo las condiciones expuestas en el punto anterior dependerá del medio de pago utilizado, a saber: (i) para pagos realizados con AMIPASS o tarjeta de débito será de 72 horas hábiles, considerando horas hábiles de Lunes a Viernes, sin contar feriados; (ii) para pagos con tarjetas de crédito, el plazo de devolución corresponderá a los establecidos por el banco emisor de la misma tarjeta con la que se realizó la transacción.</p>
                <p>6.3 Si la cancelación se realiza de manera posterior a la confirmación del pedido por parte del Comercio, el consumidor estará obligado al pago del valor total de la compra con despacho incluido si existiese. Sin perjuicio de lo anterior, AMIPASS podrá abrir un plazo de disputa de 10 días hábiles en los que el Consumidor podrá realizar sus descargos, así como también el Comercio, con el fin de permitir a AMIPASS definir como estime conveniente en base a las razones expuestas por las partes.</p>
                <p>6.4 El Consumidor en ningún caso podrá alegar falta de conocimiento de las limitaciones, restricciones y penalidades asociadas a la compra, toda vez que la opción de cancelación es informada al momento de realizar la compra y por lo dispuesto en los presentes Términos y Condiciones.</p>
                <p>6.5 El Comercio podrá cancelar el pedido estando éste en cualquiera de sus estados (Nuevos; Confirmados; En Camino; Entregados; y Cancelados), lo que será notificado al Consumidor a través de la plataforma.</p>
                <p>6.6 En caso de que algún Producto de una compra este fuera de stock al momento de la entrega, no existirá posibilidad de reemplazo, por lo que el Comercio enviará el valor del producto sin stock en efectivo al momento de la entrega o retiro de la compra.</p>
                <p>6.7 Una vez fijada la hora de entrega del pedido en casos de retiro en el local, el Consumidor podrá retirarlo máximo hasta una hora y media después del horario convenido, o hasta el horario de cierre del local, si ello ocurre antes que el vencimiento de dicho plazo.</p>
                <p>6.8 En caso de que el Consumidor no retire el pedido en el plazo establecido anteriormente, no se realizará devolución de dinero de aquellos productos perecibles solicitados, pudiendo retirar, previa coordinación con el comercio, aquellos productos no perecibles que incluya el pedido en un nuevo día y hora a convenir. </p>
                <p>6.9 En caso de que el Comercio no tenga el pedido listo para la entrega en el plazo establecido, el Consumidor podrá solicitar la cancelación de este, con derecho a un reembolso completo en perjuicio del Comercio, a través de la apertura de una disputa por el caso, descrito en la cláusula Octava.</p>
                <p>6.10 Si el Comercio se ve en la necesidad de cancelar un pedido, debido a que la información entregada por el Consumidor no es correcta y/o verídica, el Consumidor no tendrá derecho a devolución alguna. En caso de que los productos comprados no sean perecibles, el Consumidor podrá corregir la información y solicitar un nuevo despacho de los Productos pagando nuevamente el costo de despacho correspondiente.</p>
                <h5>QUINTO. Condiciones de Pago.</h5>
                <p>7.1 AMIPASS no será responsable de ningún siniestro, daño o perjuicio, ya sea directo o indirecto, que sufra el Consumidor, el repartido y/o terceros, con ocasión de la entrega de los productos comprados, bajo el entendido de que el repartidor responde ante el Comercio respectivo y es quien presta el Servicio por cuenta y riesgo propio, liberando tanto a AMIPASS como al Consumidor de cualquier responsabilidad que pudiera surgir durante la prestación del servicio.</p>
                <p>7.2 En virtud de lo anterior, AMIPASS no se hará responsable por incumplimientos del Consumidor o por daños y/o perjuicios, directos ni indirectos, que se ocasionen al Consumidor, al repartidor y/o a terceros producto de la prestación del Servicio o entrega del Producto.</p>
                <p>7.3 El Consumidor asume como propia la responsabilidad que pudiera derivar de sus propios actos u omisiones, eximiendo a AMIPASS de toda responsabilidad ante cualquier reclamo, daño, perjuicio, pérdida, penalización y costo (incluyendo, sin limitación alguna, los honorarios de abogados y costas procesales judiciales) y de cualquier gasto derivado de o relacionado con la Solicitud, la Reserva y/o de la utilización de la Plataforma mediante un actuar doloso o culposo del Consumidor.</p>
                <h5>OCTAVO. Reclamos y Disputas.</h5>
                <p>8.1 AMIPASS pondrá a disposición del consumidor un servicio de reclamos vía email, donde el Consumidor podrá enviar su reclamo a la casilla hola@AMIPASS.com en un plazo máximo de 24 horas corridas posterior a la hora de entrega o retiro de la compra.</p>
                <p>8.2 Todo reclamo deberá contar con los respaldos correspondientes como comprobantes de compra, fotos de los productos y detalle del problema. De lo contrario, se enviará un email al Consumidor solicitando la información faltante, la cual, de no ser enviada en un plazo de 24 horas hábiles, facultará a AMIPASS a dar por cerrado el caso.</p>
                <p>8.3 AMIPASS dará respuesta a todos los reclamos interpuestos por los consumidores en un plazo de 10 días hábiles, el que podrá extenderse por hasta 30 días hábiles adicionales.</p>
                <p>8.4 AMIPASS pondrá a disposición del Consumidor un número de la aplicación WhatsApp para realizar consultas y solicitar asistencia, sin perjuicio de que todo reclamo debe ser enviado de formalmente por email a la casilla hola@AMIPASS.com, pues de lo contrario no será considerado un reclamo válido.</p>
                <h5>NOVENO. Uso y Garantía de la Aplicación.</h5>
                <p>9.1 AMIPASS no garantiza la disponibilidad ni continuidad del funcionamiento o acceso a la Plataforma. En consecuencia, AMIPASS no será en ningún caso responsable por ningún daño ni perjuicio que pueda derivarse, sin ser la siguiente numeración taxativa, de: (i) la falta de disponibilidad o accesibilidad a la Plataforma, ya sea por fallas de AMIPASS o causas ajenas a ella; (ii) la interrupción en el funcionamiento de la Plataforma y/o fallos informáticos, averías telefónicas, desconexiones, retrasos o bloqueos causados por deficiencias o sobrecargas en las líneas telefónicas, centros de datos, en el sistema de Internet o en otros sistemas electrónicos producidos en el curso de su funcionamiento; y (iii) otros daños que puedan ser causados por terceros mediante intromisiones no autorizadas.</p>
                <p>9.2 AMIPASS no garantiza la ausencia de virus ni de otros elementos en la Plataforma introducidos por terceros ajenos a AMIPASS que puedan producir alteraciones en los sistemas electrónicos y/o lógicos del Consumidor y/o en los documentos electrónicos y ficheros almacenados en sus sistemas. En consecuencia, AMIPASS no será en ningún caso responsable de los daños y perjuicios de toda naturaleza que pudieran derivarse de la presencia de virus y/u otros elementos que puedan producir alteraciones en los sistemas físicos y/o lógicos, documentos electrónicos y/o ficheros del Consumidor.</p>
                <p>9.3 Sin perjuicio del numeral anterior, AMIPASS adopta diversas medidas de protección para proteger la Plataforma y los contenidos contra ataques informáticos de terceros. No obstante, AMIPASS no garantiza que terceros no autorizados no puedan conocer las condiciones, características y circunstancias en las cuales el Consumidor accede a la Plataforma. En definitiva, AMIPASS no será en ningún caso responsable de los daños y perjuicios que pudieran derivarse de algún acceso no autorizado, sin perjuicio de tomar acciones preventivas para que ello no suceda.</p>
                <h5>DÉCIMO. Derechos de Propiedad Intelectual e Industrial.</h5>
                <p>10.1 El Consumidor reconoce y acepta que todos los derechos de propiedad intelectual e industrial sobre los contenidos, o cualesquiera otros elementos insertados en la Plataforma (incluyendo, sin limitación, marcas, logotipos, nombres comerciales, textos, imágenes, gráficos, diseños, sonidos, bases de datos, software, diagramas de flujo, presentación, audio y vídeo), pertenecen y son propiedad exclusiva de AMIPASS, sin implicar como cesión de derecho alguno a favor del Consumidor, su acceso a la Plataforma o la aceptación de los presentes Términos y Condiciones.</p>
                <p>10.2 AMIPASS autoriza al Consumidor a utilizar, visualizar, imprimir, descargar y almacenar los contenidos y/o los elementos insertados en la Plataforma exclusivamente para su uso personal, privado y no lucrativo.</p>
                <p>10.3 El Consumidor se obliga a no realizar sobre la Plataforma acto alguno de descompilación, ingeniería inversa, modificación, divulgación o suministro.</p>
                <p>10.4 Cualquier otro uso, cesión y/o explotación de contenidos insertados en la Plataforma distinto de los aquí previstos, estará sujeto a la autorización previa y por escrito de AMIPASS.</p>
                <h5>NOVENO. Uso y Garantía de la Aplicación.</h5>
                <p>11.1 Los Datos Personales que el Consumidor proporcione en su Registro se integrarán en una base de datos personales de la cual es responsable AMIPASS, y que se administrará de acuerdo a las normas establecidas en la Ley Nº 19.628 sobre Protección de la Vida Privada.</p>
                <p>11.2 El Consumidor autoriza expresamente a AMIPASS para que almacene, guarde y procese sus Datos Personales o los transfiera a otros servidores de conformidad a la ley Nº 19.628 y a la Política de Privacidad de Datos de AMIPASS.</p>
                <p>11.3 El Consumidor podrá en cualquier momento solicitar la eliminación de su Registro y de sus Datos Personales de conformidad a la Ley Nº 19.628 de Protección de la Vida Privada, mediante una comunicación al correo electrónico a hola@AMIPASS.com o enviando una notificación formal a la dirección Av. Raul Labbé 12613, Oficina 226 Comuna de Las Condes.</p>
                <h5>DECIMO SEGUNDO. Notificaciones.</h5>
                <p>12.1 AMIPASS podrá realizar notificaciones al Consumidor a través de una notificación general en la Plataforma, mediante un mensaje de texto, notificaciones “push”, o a la dirección de correo electrónico indicada por el Consumidor en su Cuenta.</p>
                <p>12.2 El Consumidor podrá notificar a AMIPASS mediante el envío de un correo electrónico a la dirección hola@AMIPASS.com.</p>
                <h5>DÉCIMO TERCERO. Cesión.</h5>
                <p>13.1 El Consumidor no podrá ceder sus derechos ni las obligaciones establecidas en estos Términos y Condiciones sin el previo consentimiento escrito de AMIPASS.</p>
                <p>13.2 AMIPASS podrá ceder, sin necesidad de consentimiento previo del Consumidor, los derechos y obligaciones emanados de estos Términos y Condiciones a cualquier entidad comprendida dentro de su grupo de sociedades, su matriz y personas relacionadas en todo el mundo, así como a cualquier entidad que le suceda en el ejercicio de su negocio.</p>
                <h5>DÉCIMO CUARTO. Ley Aplicable y Competencia.</h5>
                <p>14.1 Los presentes Términos y Condiciones, así como la relación entre AMIPASS y el Consumidor, se regirán e interpretarán con arreglo a la legislación vigente en la República de Chile.</p>
                <p>14.2 Para todos los efectos legales, el Consumidor y AMIPASS fijan su domicilio en la comuna y ciudad de Santiago de Chile, y prorrogan competencia a los Tribunales Ordinarios de Justicia.</p>
                <h5>DÉCIMO QUINTO. Política de Privacidad</h5>
                <p>15.1 Amipass declara que ha tomado las medidas necesarias en sus instalaciones y sistemas que garantizan la confidencialidad de los datos ingresados por el Consumidor en su plataforma amiMARKET. El Consumidor podrá modificar o rectificar los datos ingresados por él en la Plataforma a través de la sección Perfil. El Consumidor se obliga a proporcionar información auténtica, cierta, verdadera, fidedigna, completa y actualizada cuando así sea requerido en o a través de la Plataforma amiMARKET. AMIPASS tratará los datos de carácter personal, que sean ingresados por el usuario o aquellos que obran en poder de AMIPASS por cualquier causa, con la debida confidencialidad y de acuerdo con las normas vigentes.</p>
                <p>15.2 Para los efectos de la ley 19.628 Sobre Protección de Datos de Carácter Personal, el Consumidor autoriza a amiMARKET a divulgar a las autoridades competentes o compartir exclusivamente con terceros con quienes suscriba alianzas o acuerdos comerciales, ciertos datos personales como el número de Rut, domicilio, actividad, correo electrónico, fecha de nacimiento, numero de celular o cualquier información relacionada su cuenta amiMARKETAMIPASS, cuando ello se realice con el propósito de facilitar el desarrollo, materialización y/o implementación de acciones comerciales, sean de carácter general o personal, cuando ello fuere necesario o conveniente a fin de agregar nuevos atributos o beneficios a sus consumidores o mejorar la calidad de prestación de servicios y/o productos de AMIPASS. El Consumidor declara conocer y aceptar que parte de la información contenida en la aplicación está amparada en la ley 19.628, de tal manera, que ésta es entregada a solicitud del Consumidor y bajo su propia responsabilidad, bastando como prueba de ello los antecedentes informáticos correspondientes</p>
                <p>15.3 La información obtenida por o a través de la web y/o aplicación cualesquiera de los Servicios que por este medio se ofrece u otorga es para el uso personal del Consumidor, obligándose éste a mantener la debida confidencialidad; le queda prohibida al Consumidor la cesión, reproducción, recirculación, retransmisión, comercialización y/o distribución, en todo o parte, y cualquier forma de los contenidos la plataforma amiMARKET.</p>
                <p>15.4 El Consumidor declara mediante la aceptación del presente documento entender que a través del uso de los Servicios da su consentimiento a las diferentes formas de tratamiento de sus datos personales y de los archivos de Amipass de conformidad con los límites establecidos en la presente cláusula.</p>
            </div>
        )
    }
}
