import React, { Component } from 'react'
import HeaderComponent from './HeaderComponent'
import FooterComponent from './FooterComponent'

export default class PageComponent extends Component {
    
    render() {
        return (
            <>
                <HeaderComponent/>
                <section style={{ flex: 1, marginTop: 50 }} >
                    <div className="d-flex flex-column h-100" >
                        {this.props.children}
                    </div>
                </section>
                <FooterComponent/>
            </>
        )
    }
}
