import React, { Component } from 'react'
import { connect } from 'react-redux';
import { useHistory } from 'react-router-dom'
import { Image, Container, } from 'react-bootstrap'
import menuAuthReducerAction from './../redux/actions/menuAuthReducerAction'
import countItemCarReducerAction from '../redux/actions/countItemCarReducerAction';
import itemsCarReducerAction from './../redux/actions/itemsCarReducerAction';
import logo from '../assets/images/amipass.svg'
import iconCar from '../assets/icons/car.svg'
import CarMenuComponent from './CarMenuComponent';
import CurrencyFormat from 'react-currency-format';

class HeaderComponent extends Component {
  constructor(props) {
    super(props)
    this.state = {
      user: null,
      fullname: ""
    }
  }

  componentDidMount() {
    window.callbellSettings = {
      token: "hAaoBzBNaSXiu2oQbA1af5a2"
    };
    let products = JSON.parse(localStorage.getItem('car'));
    if(products !== null) {
      this.props.itemsCarReducerAction(products)
      let amount = 0
      let count = 0
      products.map(item => {
        count += item.quantity
        amount += item.saleAmount * item.quantity
        return true
      })
      this.props.countItemCarReducerAction({
        count: count,
        items: products,
        amount
      })
    }
    let user = JSON.parse(localStorage.getItem('user'))
    this.setState({
      user,
      fullname: (user === null | typeof user === 'undefined') ? '' : user.fullname
    })
    window.scrollTo(0,0)
  }

  onLogin = () => {
    useHistory().pushState(null, 'Login', '/login')
  }

  onRegister = () => {
    const { history } = this.props;
    history.push('/register')
  }

  render() {
    let fullName = (typeof this.state.fullname == 'undefined' ? '' : this.state.fullname)
      return (
        <header>
          <nav className="navbar navbar-expand-sm fixed-top navbar-light bg-light border-bottom">
            <Container>
              <div className='menu-resposive'>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                  <span className="navbar-toggler-icon"></span>
                </button>
              </div>

              {
                localStorage.getItem('address') ?
                <a className="navbar-brand position-absolute mt-4" href="/home">
                  <Image className='logo-top' src={logo} width={150} />
                </a>
                :
                <a className="navbar-brand position-absolute mt-4" href="/">
                  <Image className='logo-top' src={logo} width={150} />
                </a>
              }              

              <div className="d-movil car-movil">
                <div className="nav-link dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown">
                  <span className="badge badge-pill badge-primary quantity-car">{this.props.shoppingCarMenuReducer.count}</span>
                  <span className='quantity-amount'> <CurrencyFormat 
                          value={this.props.shoppingCarMenuReducer.amount}
                          displayType={'text'} 
                          thousandSeparator={true} 
                          prefix={'$'}
                          renderText={value => (
                          <span className='quantity-amount'>{value.replace(/,/g, '.')}</span>
                          )}
                      /></span>
                  <Image width={30} src={iconCar} style={{ color: '#000' }} />
                </div>
                <div className="dropdown-menu dropdown-menu-sm-right center-div-m" aria-labelledby="dropdownMenuButton">
                  <div className='overflow-auto mx-1'>
                    <div className='col' style={{ width:300, maxHeight: 300 }}>
                      {
                        !(localStorage.getItem('car') === null | typeof localStorage.getItem('car') === 'undefined' ) && 
                        <CarMenuComponent data={this.props.shoppingCarMenuReducer.items} />
                      }
                    </div>
                  </div>
                  <div className="dropdown-divider"></div>
                  <span className='dropdown-item-text d-flex justify-content-end car-sub' ><small>Sub total: <strong>
                      <CurrencyFormat 
                          value={this.props.shoppingCarMenuReducer.amount}
                          displayType={'text'} 
                          thousandSeparator={true} 
                          prefix={'$'}
                          renderText={value => (
                          <span>{value.replace(/,/g, '.')}</span>
                          )}
                      /></strong> </small></span>
                  <div className="dropdown-item-text">
                    {
                      this.state.user ? 
                      <a className='btn btn-primary btn-block btn-sm button-login' href={`/local/${this.props.commerceReducer.ecommerceName}/pay/delivery`} >Finaliza tu compra</a>
                      :
                      <a className='btn btn-primary btn-block btn-sm button-login' href={`/local/${this.props.commerceReducer.ecommerceName}/pay/login`} >Inicia sesión</a>
                    }
                  </div>
                </div>
              </div>
            <div className="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
              <ul className="navbar-nav">
                <li className="nav-item">
                  <a className="nav-link p-r-30" href="/recommender">Pide tu local</a>
                </li>
                <li className="nav-item">
                  <a className="nav-link p-r-30" 
                    href="https://api.whatsapp.com/send?phone=56975196382"
                    onClick={() => { window.dataLayer.push({'event': 'iNeedHelp'}) }}
                  >Necesito ayuda</a>
                </li>
                {
                  this.props.shoppingCarMenuReducer.count > 0 &&
                  <li className="nav-item dropdown d-desktop">
                    <div className="nav-link dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown">
                      <span className="badge badge-pill badge-primary quantity-car">{this.props.shoppingCarMenuReducer.count}</span>
                      <span> <CurrencyFormat 
                              value={this.props.shoppingCarMenuReducer.amount}
                              displayType={'text'} 
                              thousandSeparator={true} 
                              prefix={'$'}
                              renderText={value => (
                              <span className='quantity-amount'>{value.replace(/,/g, '.')}</span>
                              )}
                          /></span>
                      <Image width={30} src={iconCar} style={{ color: '#000' }} />
                    </div>
                    <div className="dropdown-menu dropdown-menu-sm-right" aria-labelledby="dropdownMenuButton">
                      <div className='overflow-auto mx-1'>
                        <div className='col' style={{ width:300, maxHeight: 300 }}>
                          {
                            !(localStorage.getItem('car') === null | typeof localStorage.getItem('car') === 'undefined' ) && 
                            <CarMenuComponent data={this.props.shoppingCarMenuReducer.items} />
                          }
                        </div>
                      </div>
                      <div className="dropdown-divider"></div>
                      <span className='dropdown-item-text d-flex justify-content-end car-sub' ><small>Sub total: <strong>
                          <CurrencyFormat 
                              value={this.props.shoppingCarMenuReducer.amount}
                              displayType={'text'} 
                              thousandSeparator={true} 
                              prefix={'$'}
                              renderText={value => (
                              <span className='amount-sub-car'>{value.replace(/,/g, '.')}</span>
                              )}
                          /></strong> </small></span>
                      <div className="dropdown-item-text">
                        {
                          this.state.user ? 
                          <a className='btn btn-primary btn-block btn-sm button-login' 
                            href={`/local/${this.props.commerceReducer.ecommerceName}/pay/delivery`} 
                            onClick={() => { window.dataLayer.push({event: 'finishShopping'}) }}
                          >Finaliza tu compra</a>
                          :
                          <a className='btn btn-primary btn-block btn-sm button-login' 
                            href={`/local/${this.props.commerceReducer.ecommerceName}/pay/login`}
                            onClick={() => { window.dataLayer.push({'event': 'logInCart'}) }}
                          >Inicia sesión</a>
                        }
                      </div>
                    </div>
                  </li>
                }
                {
                  !(localStorage.getItem('jwt') === null | typeof localStorage.getItem('jwt') === 'undefined' ) && 
                  <li className="nav-item dropdown">
                    <div className='nav-link dropdown-toggle border-button-ami' id='navbarDropdownProfile' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false' >
                      { fullName }
                    </div>
                    <div className='dropdown-menu' aria-labelledby='navbarDropdownProfile' >
                      <a className='dropdown-item' href="/profile">Mis datos</a>
                      <a className='dropdown-item' href="/profile/history">Histórico de compras</a>
                      {/* <a className='dropdown-item' href="/profile/inbox">Inbox</a> */}
                      {/* <a className='dropdown-item' href="/help/questions">Preguntas frecuentes</a> */}
                      {/* <a className='dropdown-item' href="/help/claims">Reclamos</a> */}
                      <div className='dropdown-divider' ></div>
                      <a className='dropdown-item' 
                        href="/logout"
                        onClick={() => { window.dataLayer.push({'event': 'logOut'}) }}
                      >Cerrar Sesión</a>
                    </div>
                  </li>
                }
                {
                  (localStorage.getItem('jwt') === null || typeof localStorage.getItem('jwt') === 'undefined' ) &&
                  <>
                    <div className='nav-item' >
                      <a className='nav-link border-button-ami m-r-20' href="/register"
                        onClick={() => { window.dataLayer.push({'event': 'signingUp'}) }}
                      >Registrate</a>
                    </div>
                    <div className='nav-item' >
                      <a className='nav-link m-r-30 button-ami-red' href="/login"
                        onClick={() => { window.dataLayer.push({'event': 'logIn'}) }}
                      >Ingresa</a>
                    </div>
                  </>
                }
              </ul>
            </div>
            </Container>
          </nav>
        </header>
      )
  }
}

const mapStateToProps = (state) => {
  return {
    shoppingCarMenuReducer: state.shoppingCarMenuReducer,
    commerceReducer: state.commerceReducer,
    menuAuthReducer: state.menuAuthReducer
  }
}

const mapDispatchToProps = {
  menuAuthReducerAction,
  itemsCarReducerAction,
  countItemCarReducerAction
}

export default connect(mapStateToProps, mapDispatchToProps)(HeaderComponent)