import React, { Component } from 'react'
import { Button, Image} from 'react-bootstrap'
import { connect } from 'react-redux'
import countItemCarReducerAction from '../redux/actions/countItemCarReducerAction';
import itemsCarReducerAction from './../redux/actions/itemsCarReducerAction';
import CurrencyFormat from 'react-currency-format';
import IconCheck from '../assets/images/check.svg';

import { database } from '../utils/firebase'
import ModalComponent from './system/ModalComponent';

class CarMenuComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            modalAddComment: false,
            comment: "",
            product: {}
        }
    }

    remove = async (item) => {
        let newItems = this.props.data.filter(el => el.id !== item.id)
        this.props.itemsCarReducerAction(newItems)
        let amount = 0
        let count = 0
        newItems.map(item => {
            count += item.quantity
            amount += item.saleAmount * item.quantity
            return true
        })
        this.props.countItemCarReducerAction({ 
            count,
            amount,
            items: newItems 
        })
        localStorage.setItem('car', JSON.stringify(newItems));
        let commerce = this.props.commerceReducer
        let user = null
        if(localStorage.getItem('userTemp')) {
            user = {
                username: localStorage.getItem('userTemp')
            }
        } else {
            user = JSON.parse(localStorage.getItem('user'));
        }
        if(user) {
            const cart = await database
            .collection(`${user.username}`)
            .doc(`cart-${commerce.commerceID}${commerce.storeID}`)
            
            cart.set({
                commerce: commerce.commerceID,
                store: commerce.storeID,
                commerceAlias: commerce.ecommerceName,
                items: newItems
            })

            if(newItems.length === 0) {
                await database
                .collection(`${user.username}`)
                .doc(`cart-${this.state.commerce.commerceID}${this.state.commerce.storeID}`)
                .delete()
            }
        }
    }

    comment = async () => {
        let product = this.state.product;
        product.comment = this.state.comment;
        let commerce = this.props.commerceReducer
        let user = JSON.parse(localStorage.getItem('user'));
        
        let newData = this.props.data.map(item => {
            if(item.id === product.id) {
                item.comment = this.state.comment
            }
            return item
        })

        this.props.countItemCarReducerAction({ 
            ...this.props.shoppingCarMenuReducer,
            items: newData 
        })
        localStorage.setItem('car', JSON.stringify(newData));

        const cart = await database
            .collection(`${user.username}`)
            .doc(`cart-${commerce.commerceID}${commerce.storeID}`)
            
            cart.set({
                commerce: commerce.commerceID,
                store: commerce.storeID,
                commerceAlias: commerce.ecommerceName,
                items: newData
            })
    }
    render() {
        return (
            <>
                {
                    this.props.data.map(item => (
                        <div key={item.id} className="row py-2 border-bottom">
                            <div className="col-4">
                                <Image 
                                    width={80}
                                    src={item.photoURL} 
                                    alt={item.shortDescription}
                                    fluid={true}
                                />
                            </div>
                            <div className="col">
                                <div className='row' >
                                    <h6 className='title-car'>
                                        {item.productName}<br/>
                                    </h6>
                                </div>
                                <div className="row">
                                    <small className="text-muted">{item.shortDescription}</small>
                                </div>
                                <div className="row">
                                    {
                                        (item.food === true) ? 
                                        <>
                                            <small className="text-muted t-pay-amipass"><Image width={10} src={IconCheck} /> Paga con amiPASS</small>
                                        </> :
                                        <>
                                            {/* <span className='badge badge-pill badge-success' >No Food</span> */}
                                        </>
                                    }
                                    { (this.props.comment) && <button className='btn btn-link' type='button' onClick={() => {
                                        this.setState({ 
                                            modalAddComment: true,
                                            product: item,
                                            comment: item.comment
                                        })
                                    }} >Agregar comentario</button> }
                                </div>
                                <div className='row justify-content-end' >
                                    <span className='pr-3' >
                                        <CurrencyFormat 
                                            value={item.saleAmount}
                                            displayType={'text'} 
                                            thousandSeparator={true} 
                                            prefix={'$'}
                                            renderText={value => (
                                            <span className='amount-car'>{value.replace(/,/g, '.')}</span>
                                            )}
                                        /> <small className='text-muted' >x {item.quantity}</small>
                                    </span>
                                </div>
                            </div>
                            <span className='float-right'>
                                <Button className='button-remove'
                                    onClick={async () => this.remove(item)}
                                    size='sm'
                                    variant='primary' >X</Button>
                            </span>
                        </div>
                    ))
                }
                <ModalComponent
                    show={this.state.modalAddComment} 
                    handleAccept={() => {
                        this.setState({modalAddComment: false})
                        this.comment()
                    }}
                    handleClose={() => {
                        this.setState({modalAddComment: false})
                    }}
                    textAccept='Agregar'
                    textClose='Cerrar'
                    title={'Comentario'}
                >
                    <textarea 
                        rows="5" 
                        className='form-control' 
                        placeholder='Ingrese su comentario aquí'
                        value={this.state.comment}
                        onChange={(e) => this.setState({ comment: e.target.value })}
                    ></textarea>
                </ModalComponent>
            </>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        shoppingCarMenuReducer: state.shoppingCarMenuReducer,
        commerceReducer: state.commerceReducer
    }
}

const mapDispatchToProps = {
    itemsCarReducerAction,
    countItemCarReducerAction
  }

export default connect(mapStateToProps, mapDispatchToProps)(CarMenuComponent)