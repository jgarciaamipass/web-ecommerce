import React, { Component } from 'react'
import CardComponent from './system/CardComponent'
// import logo from './../assets/images/preload-comercio.svg'
import CurrencyFormat from 'react-currency-format'
import { Button } from 'react-bootstrap'

export default class ShoppingCard extends Component {
    constructor(props) {
        super(props)
        this.state = {

        }
    }

    componentDidMount = () => {
        this.setState({
            ...this.props.data
        })
    }

    render() {
        return (
            <CardComponent>
                <div className='row' >
                    <div className='col-3 d-flex flex-column align-items-center h-100' >
                        <img 
                            className='rounded-circle' 
                            width={80} 
                            height={'auto'} 
                            src={`https://apired.amipass.cl/commerces/${this.state.commerceID}/${this.state.storeID}/logo.png`}
                            alt={`${this.state.commerceID}${this.state.storeID}`}
                        />
                    </div>
                    <div className='col-9' >
                        <div className='row' >
                            <div className='col'>Compra <strong># {this.state.id}</strong></div>
                            <div className='col'>
                                {
                                    (this.state.statusID === -1 || this.state.statusID === -2) && 
                                    <strong className='text-danger' >CANCELADO</strong>
                                }
                            </div>
                        </div>
                        <div className='row' >
                            <div className='col'><strong>{this.state.commerce}</strong></div>
                            <div className='col'>Fecha de compra <div>{this.state.creationDate}</div></div>
                        </div>
                        <div className='row' >
                            <div className='col'>
                                <strong>Estado</strong> <br />
                                {
                                    this.state.statusID === -1 && <span>Cancelado por el comercio</span>
                                }
                                {
                                    this.state.statusID === -2 && <span>Cancelado</span>
                                }
                                {
                                    this.state.statusID === 1 && <span>Creado</span>
                                }
                                {
                                    this.state.statusID === 2 && <span>Confirmado</span>
                                }
                                {
                                    this.state.statusID === 3 && <span>En camino</span>
                                }
                                {
                                    this.state.statusID === 4 && <span>Entregado</span>
                                }
                            </div>
                            <div className='col'>
                                <strong>Tipo de entrega</strong> <br />
                                {
                                    this.state.orderTypeID === 1 ? <span>Retiro en local</span> :
                                    <span>Delivery</span>
                                }
                            </div>
                            <div className='col' >

                            </div>
                        </div>
                        <div className="row">
                            <div className="col">
                                <strong>Medio de pago</strong> <br />
                                {
                                    this.state.typePayment === 1 ? <span>Amipass</span>
                                    : <span>Transbank</span>
                                }
                            </div>
                            <div className="col">
                                <strong>Cantidad</strong> <br />
                                { this.state.totalItems}
                            </div>
                            <div className="col">
                                <strong>Total</strong> <br />
                                <CurrencyFormat
                                    value={this.state.finalAmount}
                                    displayType={'text'} 
                                    thousandSeparator={true} 
                                    prefix={'$'}
                                    renderText={value => (
                                    <span>{value.replace(/,/g, '.')}</span>
                                    )}
                                />
                            </div>
                        </div>
                        <div className="row">
                            <Button 
                                size='sm' 
                                className='mr-3' 
                                variant='primary'
                                onClick={this.props.handleShow}>
                                Ver Resumen
                            </Button>
                            {/* <Button 
                                size='sm' 
                                className='mr-3' 
                                variant="outline-secondary"
                                onClick={this.props.handleRepeat}>
                                Repetir
                            </Button> */}
                            {
                                this.state.statusID === 1 && 
                                <span className='mx-2' >
                                    <Button 
                                        size='sm' 
                                        variant="outline-secondary"
                                        onClick={this.props.handleCancel}>
                                        Cancelar orden
                                    </Button>
                                </span>
                            }
                        </div>
                    </div>
                </div>
            </CardComponent>
        )
    }
}
