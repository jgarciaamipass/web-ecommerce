import React, { Component } from 'react'

export default class CoverComponent extends Component {
    render() {
        return (
            <div className='jumbotron'
                style={{ 
                    backgroundImage: (this.props.overlay) ? 
                        `linear-gradient(rgba(0, 0, 0, 0.13) 0%, rgba(0, 0, 0, 0.6) 100%), url(${this.props.cover})` : 
                        `url(${this.props.cover})`, 
                    backgroundAttachment: 'inherit',
                    backgroundPosition: 'center', 
                    backgroundRepeat: 'no-repeat',
                    backgroundSize: 'cover',
                }}
            >
                <div className='container' >
                    <div className='align-self-center' >
                        {this.props.children}
                    </div>
                </div>
            </div>
        )
    }
}
