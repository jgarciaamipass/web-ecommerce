import React, { Component } from 'react'
import logo from '../assets/images/amipass.svg'
import facebook from '../assets/images/facebook.svg'
import instagram from '../assets/images/instagram.svg'
// import youtube from '../assets/images/youtube.svg'
import { Image } from 'react-bootstrap'

export default class FooterComponent extends Component {
    render() {
        return (
            <footer style={{ height: 150, backgroundColor: '#ff1e2f' }} >
                    <div className='container d-flex footer-container' >
                        <div className='col flex-column d-flex justify-content-center text-center align-items-center' >
                            <a href='/' >
                                <Image src={logo} width={100} className='pb-2' />
                            </a>
                            <span>amiPASS 2020. Todos los derechos reservados.</span>
                        </div>
                        <div className='col flex-column d-flex justify-content-center col. footer-ask'>
                            <ul className='list-unstyled' >
                                {/* <li className='li-footer'>Preguntas frecuentes</li> */}
                                <li className='li-footer'><a href='https://www.amipass.com/lanzamientos/amimarket' onClick={() => {window.dataLayer.push({'event': 'commerceSigningUp'})}}  target='_blank' rel="noopener noreferrer" >Sumate a amiMarket</a></li>
                                <li className='li-footer'><a href='/recommender' onClick={() => {window.dataLayer.push({'event': 'requestYourLocal'})}} >Pide tu local</a> </li>
                                <li className='li-footer'><a href='/terms_conditions' onClick={() => {window.dataLayer.push({'event': 'termsAndConditions'})}} >Términos y condiciones</a></li>
                            </ul>
                        </div>
                        <div className='col flex-column d-flex justify-content-center '>
                            <div className='footer-container-social'>
                                <strong className='pb-2'>Síguenos:</strong>
                                <div className='d-flex icon-social'>
                                    <a href='https://www.facebook.com/amipasschile' onClick={() => {window.dataLayer.push({'event': 'buttonFacebook'})}} target='_blank' rel="noopener noreferrer" ><Image className='m-l-1' src={facebook} width={50} /></a>
                                    <a href='https://www.instagram.com/amipass.chile/?hl=es-la' onClick={() => {window.dataLayer.push({'event': 'buttonInstagram'})}} target='_blank' rel="noopener noreferrer" ><Image className='m-l-1' src={instagram} width={50} /></a>
                                    {/* <Image className='m-l-1' src={youtube} width={50} /> */}
                                </div>
                            </div>
                        </div>
                    </div>
            </footer>

            
        )
    }
}
