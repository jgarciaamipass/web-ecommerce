import React, { Component } from 'react'
import { Button, Modal } from 'react-bootstrap'

export default class ModalComponent extends Component {
    render() {
        return (
            <Modal 
                show={this.props.show}
                backdrop="static" 
                keyboard={false} 
                centered 
                onHide={this.props.handleClose} 
                size={this.props.size} 
                scrollable={true}
            >
                <Modal.Header>
                    <Modal.Title>{this.props.title}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    {this.props.children}
                </Modal.Body>
                <Modal.Footer>
                    {
                        this.props.handleClose && <Button disabled={this.props.cancelDisabled} variant='secondary' onClick={this.props.handleClose} >{ this.props.textClose ? this.props.textClose : 'Cerrar' }</Button>
                    }
                    {
                        this.props.handleAccept && <Button disabled={this.props.acceptDisabled} variant='primary' onClick={this.props.handleAccept} >{ this.props.textAccept ? this.props.textAccept : 'Aceptar' }</Button>
                    }
                </Modal.Footer>
            </Modal>
        )
    }
}
