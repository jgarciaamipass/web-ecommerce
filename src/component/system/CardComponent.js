import React, { Component } from 'react'

export default class CardComponent extends Component {
    render() {
        return (
            <div className='col' >
                <div className='card' >
                    <div className="card-body">
                        {this.props.children}
                    </div>
                </div>
            </div>
        )
    }
}
