import React, { Component } from 'react'
import { Image, Button } from 'react-bootstrap'
import CurrencyFormat from 'react-currency-format'
import IconCheck from '../assets/images/check.svg';

export default class ProductFromListComponent extends Component {
    titleCase = (string) => {
        let str = string.toLowerCase().split(" ");
        for(let i = 0; i< str.length; i++){
           str[i] = str[i].charAt(0).toUpperCase() + str[i].slice(1) + " ";
        }
        return str
    }
    render() {
        return (
            <div key={this.props.item.id} className={(this.props.item.stk > 0) ? 'col mb-4 box-market-list' : 'col mb-4 box-market-list unavailable-product'} >
                <div className="row content-product">
                    <div className="col-md-4">
                        <Image className='img-list-product'
                            src={this.props.item.photoURL} 
                            alt={this.props.item.shortDescription}
                            fluid={true}
                        />
                    </div>
                    <div className="col-md-8">
                        <div className='row' >
                            <div className='col-md-12'>
                                <h6>
                                    {this.titleCase(this.props.item.productName)}<br/>
                                </h6>
                                <small className="text-muted">{this.props.item.shortDescription}</small><br></br>
                                    {
                                    (this.props.item.food === true) ? 
                                    <>
                                    <small className="text-muted t-pay-amipass"><Image width={15} src={IconCheck} /> Paga con amiPASS</small>
                                    </> :
                                    <>
                                    {/* <span className='badge badge-pill badge-success' >No Food</span> */}
                                    </>
                                }
                            </div>
                            <div className='col-md-6 amount-box' >
                                <CurrencyFormat 
                                    value={this.props.item.saleAmount}
                                    displayType={'text'} 
                                    thousandSeparator={true} 
                                    prefix={'$'}
                                    renderText={value => (
                                    <span className='font-semibold'>{value.replace(/,/g, '.')}</span>
                                    )}
                                />
                            </div>
                            <div className='col-md-6 col-6 offset-3 offset-md-0' >
                                <div className='row'>
                                    {
                                        this.props.item.stk > 0 ?
                                        <>
                                            {
                                                this.props.item.quantity > 0 && 
                                                <div className='col-md-12'>
                                                    <div className='row'>
                                                        <div className='col-4'>
                                                            { (this.props.controls === true) ?
                                                                <Button className='btn-control-add-remove btn-remove'
                                                                    onClick={this.props.removeProduct} 
                                                                    size='sm'
                                                                    variant='primary' >-</Button>
                                                                : <></>
                                                            }
                                                        </div>
                                                        <div className='col-4'>
                                                            <p className='text-center font-bold'>{this.props.item.quantity}</p>
                                                        </div>
                                                        <div className='col-4'>
                                                            { (this.props.controls === true) ?
                                                                <Button className='btn-control-add-remove btn-add'
                                                                    onClick={this.props.addProduct} 
                                                                    size='sm'
                                                                    variant='primary' >+</Button>
                                                                : <></>
                                                            }
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            }
                                            {
                                                this.props.item.quantity === 0 && 
                                                <div className='col btn-cart-div text-center'>
                                                    <button 
                                                        className='btn btn-primary btn-sm btn-block btn-add-cart' type="button" 
                                                        onClick={this.props.addProduct} 
                                                        >Agregar
                                                    </button>
                                                </div>
                                            }
                                        </>
                                        :
                                        <>
                                            <span className='font-semibold bt-unavailable'>Agotado</span>
                                        </>
                                    }
                                </div>
                                {
                                    (this.props.item.quantity > this.props.item.stk && this.props.item.stk > 0) && 
                                    <div className='row text-center' >
                                        <small className='text-danger' >Cantidad excedida</small>
                                    </div>
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
