import React, { Component } from 'react'
import preload from './../assets/images/preload-amipass.gif'

export default class PreloadComponent extends Component {
    render() {
        return (
            <div className='text-center pt-5 border border-secondary' style={{ 
                top: 0, 
                left: 0, 
                height: '200px', 
                width: '100%' ,
                zIndex: 50,
                background: 'rgba(255,255,255,0.7)',
                borderRadius: 3,
            }} >
                <img src={preload} alt='' width={80} className='rounded-circle' />
            </div>
        )
    }
}
