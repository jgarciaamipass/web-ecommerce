import React, { Component } from 'react'
import { Image } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import logo from './../assets/images/preload-comercio.svg'

export default class CommerceFromListComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            logoError: false
        }
    }

    titleCase = (string) => {
        let str = string.toLowerCase().split(" ");
        for(let i = 0; i< str.length; i++){
           str[i] = str[i].charAt(0).toUpperCase() + str[i].slice(1) + " ";
        }
        return str
    }
    render() {
        return (
            <Link style={{ textDecoration: 'none' }} to={`/local/${this.props.data.ecommerceName}`}
                onClick={() => {
                    window.dataLayer.push({
                        event: 'selectedCommerceList',
                        labelName: this.titleCase(this.props.data.storeName),
                        communeName: this.titleCase(this.props.data.commune),
                        commerceId: this.props.data.internalID
                    })
                }}
            >
                <div className='d-flex bd-highlight border-bottom py-1 box-market-list' >
                    <div className='mr-3' >
                        <Image 
                            className='rounded-circle' 
                            width={80} 
                            height={'auto'} 
                            src={(this.state.logoError) ? logo : 
                                `https://apired.amipass.cl/commerces/${this.props.data.commerceID}/${this.props.data.storeID}/logo.png`
                            }
                            onError={() => {
                                this.setState({ logoError: true })
                            }}
                        />
                    </div>
                    <div className='align-self-center' >
                        <div className='title-list-commerce'>
                            <span className='title-list-a font-semibold' >{this.titleCase(this.props.data.storeName)}</span>
                        </div>
                        <div>
                            {
                                this.props.data.hasDelivery && (<><span className="badge badge-pill badge-warning tag-delivery">Delivery</span>{' '}</>)
                            }
                            {
                                this.props.data.hasWithdrawal && (<><span className="badge badge-pill badge-info tag-retiro-local">Retiro Local</span>{' '}</>)
                            }
                        </div>
                        <div>
                            <small className='text-muted' >
                                {this.titleCase(`${this.props.data.localAddress}, ${this.props.data.commune}, ${this.props.data.city}`)} <br/> {this.props.data.storeCategory}
                            </small>
                        </div>
                    </div>
                </div>
            </Link>
        )
    }
}
