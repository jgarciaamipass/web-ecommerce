import React, { Component, Suspense } from 'react'
import { withRouter } from 'react-router-dom'

class App extends Component {

  componentWillMount() {
    this.unlisten = this.props.history.listen((location, action) => {
      switch (location.pathname) {
        case "/":
          localStorage.removeItem('car')
          break;
        case "/home":
          localStorage.removeItem('car')
          break;
        default:
          break;
      }
      console.log(`The current URL is ${location.pathname}${location.search}${location.hash}`)
      console.log(`The last navigation action was ${action}`)    
    })
  }

  componentWillUnmount() {
    this.unlisten()
  }

  render() {
    return (
      <Suspense fallback={<h1>Cargando...</h1>} >
        {this.props.children}
      </Suspense>
    )
  }
}

export default withRouter(App)