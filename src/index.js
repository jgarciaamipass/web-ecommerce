import React, { Suspense } from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
// import * as serviceWorker from './serviceWorker';
import { Provider } from 'react-redux';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import TagManager from 'react-gtm-module'

import store from './redux/store';

import InitPage from './pages/InitPage'
import HomePage from './pages/HomePage'
import RecommenderStorePage from './pages/RecommenderStorePage'
import CommercePage from './pages/CommercePage'

//Auth
import LoginPage from './pages/Auth/LoginPage'
import LogoutPage from './pages/Auth/LogoutPage'
import ForgetPasswordPage from './pages/Auth/ForgetPasswordPage'
import ChangePasswordPage from './pages/Auth/ChangePasswordPage'
import RegisterPage from './pages/Auth/RegisterPage'
import ActivateAccountPage from './pages/Auth/ActivateAccountPage';

//Profile
// import AddAmipassAccountPage from './pages/Profile/AddAmipassAccountPage'
import AddTransbankAccountPage from './pages/Profile/AddTransbankAccountPage'
import InboxPage from './pages/Profile/InboxPage'
import ProfilePage from './pages/Profile/ProfilePage'
import RegisterAccountAmipassPage from './pages/Profile/RegisterAccountAmipassPage'
import ShoppingHistoryPage from './pages/Profile/Shopping/ShoppingHistoryPage'
import ShoppingShowPage from './pages/Profile/Shopping/ShoppingShowPage'

// Pay
import DeliveryPage from './pages/Pay/DeliveryPage'
import FinishPayPage from './pages/Pay/FinishPayPage'
import LoginPayPage from './pages/Pay/LoginPayPage'
import PayStatusPage from './pages/Pay/PayStatusPage'
import PaySuccessfullyPage from './pages/Pay/PaySuccessfullyPage'
import PayDeclinedPage from './pages/Pay/PayDeclinedPage'
import AccountsAmipassPage from './pages/Pay/Amipass/AccountsAmipassPage'
import LoginAccountAmipassPage from './pages/Pay/Amipass/LoginAccountAmipassPage'
import SecurityAmipassPage from './pages/Pay/Amipass/SecurityAmipassPage'
import TransbankPage from './pages/Pay/Transbank/TransbankPage'
import TermsConditionsPage from './pages/Pay/TermsConditionsPage';

// help
import HelpPage from './pages/Help/HelpPage'
import QuestionsPage from './pages/Help/QuestionsPage'
import Error404Page from './pages/Exception/Error404Page';
import ClaimsPage from './pages/Profile/ClaimsPage';
 
const tagManagerArgs = {
    gtmId: process.env.REACT_APP_GTM_PROD
}

// ReactGA.initialize('UA-000000-01', {
//   debug: true
// });

TagManager.initialize(tagManagerArgs)


ReactDOM.render(
  <Provider store={store} >
    <React.StrictMode>
      <Suspense fallback={'Cargando...'} >
        <BrowserRouter>
          <App>
            <Switch>
              <Route path={"/"} exact component={InitPage} />
              {/* <Route path={"/"} exact component={HomePage} /> */}
              <Route path={"/home"} exact component={HomePage} />
              <Route path={"/recommender"} exact component={RecommenderStorePage} />
              <Route path={"/login"} exact component={LoginPage} />
              <Route path={"/logout"} exact component={LogoutPage} />
              <Route path={"/register"} exact component={RegisterPage} />
              <Route path={"/activate_account/:token"} exact component={ActivateAccountPage} />
              <Route path={"/forget_password"} exact component={ForgetPasswordPage} />
              <Route path={"/forget_password/:token"} exact component={ForgetPasswordPage} />
              <Route path={"/change_password/:token"} exact component={ChangePasswordPage} />
              {/* profile */}
              <Route path={"/profile/accounts/amipass"} exact component={RegisterAccountAmipassPage} />
              <Route path={"/profile/accounts/transbank"} exact component={AddTransbankAccountPage} />
              <Route path={"/profile/inbox"} exact component={InboxPage} />
              <Route path={"/profile"} exact component={ProfilePage} />
              <Route path={"/profile/history"} exact component={ShoppingHistoryPage} />
              <Route path={"/profile/history/show/:number"} exact component={ShoppingShowPage} />
              {/* Commerce */}
              <Route path={"/local/:commerce"} exact component={CommercePage} />
              <Route path={"/local/:commerce/pay/delivery"} exact component={DeliveryPage} />
              <Route path={"/local/:commerce/pay/end"} exact component={FinishPayPage} />
              <Route path={"/local/:commerce/pay/status"} exact component={PayStatusPage} />
              <Route path={"/local/:commerce/pay/successfully/:shoppingCode"} exact component={PaySuccessfullyPage} />
              <Route path={"/local/:commerce/pay/declined"} exact component={PayDeclinedPage} />
              <Route path={"/local/:commerce/pay/login"} exact component={LoginPayPage} />
              <Route path={"/local/:commerce/pay/account/amipass"} exact component={AccountsAmipassPage} />
              <Route path={"/local/:commerce/pay/account/amipass/login"} exact component={LoginAccountAmipassPage} />
              <Route path={"/local/:commerce/pay/account/amipass/security"} exact component={SecurityAmipassPage} />
              <Route path={"/local/:commerce/pay/transbank"} exact component={TransbankPage} />
              <Route path={"/terms_conditions"} exact component={TermsConditionsPage} />
              {/* help */}
              <Route path={"/help"} exact component={HelpPage} />
              <Route path={"/help/questions"} exact component={QuestionsPage} />
              <Route path={"/help/claims"} exact component={ClaimsPage} />
              <Route>
                <Error404Page/>
              </Route>
            </Switch>
          </App>
        </BrowserRouter>
      </Suspense>
    </React.StrictMode>
  </Provider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
// serviceWorker.register();
