import { SERVER_API } from '../utils/utils';

export {}

export async function refreshToken () {
    let statusResponseToken = 0;
    return fetch(`${SERVER_API}/login/refresh_token`, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('jwt')}`,
            'isRefreshToken': 'true'
        }
    }).then(async response => {
        statusResponseToken = response.status
        return await response.json();
    }).then(response => {
        switch (statusResponseToken) {
            case 200:
                const { jwt } = response
                localStorage.setItem('jwt', jwt)
                break;
            default:
                break;
        }
    }).catch(error => {
        console.log(error)
    })
}