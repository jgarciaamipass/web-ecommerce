import { createStore, combineReducers } from 'redux'

import shoppingCarMenuReducer from './reducers/shoppingCarMenuReducer';
import commerceReducer from './reducers/commerceReducer';
import menuAuthReducer from './reducers/menuAuthReducer';

const reducer = combineReducers({
    shoppingCarMenuReducer,
    commerceReducer,
    menuAuthReducer
})

const store = createStore(reducer)

export default store;