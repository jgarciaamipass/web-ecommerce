export const type = 'menuAuthReducerAction'

const menuAuthReducerAction = item => {
    return {
        type,
        payload: item
    }
}

export default menuAuthReducerAction;