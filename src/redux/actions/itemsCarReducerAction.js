export const type = 'itemsCarReducerAction'

const itemsCarReducerAction = item => {
    return {
        type,
        payload: item
    }
}

export default itemsCarReducerAction;