export const type = 'countItemCarReducerAction'

const countItemCarReducerAction = count => {
    return {
        type,
        payload: count
    }
}

export default countItemCarReducerAction;