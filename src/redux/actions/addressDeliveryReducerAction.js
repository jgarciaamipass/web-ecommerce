export const type = 'addressDeliveryReducerAction'

const addressDeliveryReducerAction = address => {
    return {
        type,
        payload: address
    }
}

export default addressDeliveryReducerAction;