export const type = 'commerceInfoReducerAction'

const commerceInfoReducerAction = count => {
    return {
        type,
        payload: count
    }
}

export default commerceInfoReducerAction;