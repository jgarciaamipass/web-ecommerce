const defaultState = {}

function reducer(state = defaultState, { type, payload }) {
    switch (type) {
        case 'commerceInfoReducerAction':{
            return payload
        }
        default:
            return state;
    }
}

export default reducer;