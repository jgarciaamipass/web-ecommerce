const defaultState = {
    count: 0,
    amount: 0,
    items: []
}

function reducer(state = defaultState, { type, payload }) {
    switch (type) {
        case 'countItemCarReducerAction': {
            return payload
        }
        default:
            return state;
    }
}

export default reducer;