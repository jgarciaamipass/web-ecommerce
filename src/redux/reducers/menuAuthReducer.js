const defaultState = {
    isAuth: false
}

function reducer(state = defaultState, { type, payload }) {
    switch (type) {
        case 'menuAuthReducerAction':{
            return payload
        }
        default:
            return state;
    }
}

export default reducer;